package com.citybiz.wxweb.utils;

import com.citybiz.common.exception.ServiceException;
import com.citybiz.service.utils.WxUserAuthUtil;
import com.citybiz.system.domain.WxUser;
import com.citybiz.system.domain.WxUserRole;
import com.citybiz.wxcommon.enums.WxUserRoleEnums;

/**
 * Create by Ykg on 2023/12/14 00:22
 * effect: 微信用户工具类
 */
public class WxUserUtils {

    public static void checkEmployRole(){
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        WxUserRole wxUserRole = wxUser.getWxUserRole();
        if(!WxUserRoleEnums.EMPLOYER_ROLE.getCode().equals(wxUserRole.getRole())){
            throw new ServiceException("请使用雇主角色操作");
        }

    }

    public static void checkWorkerRole(){
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        WxUserRole wxUserRole = wxUser.getWxUserRole();
        if(!WxUserRoleEnums.WORKER_ROLE.getCode().equals(wxUserRole.getRole())){
            throw new ServiceException("请使用威客角色操作");
        }
    }

}
