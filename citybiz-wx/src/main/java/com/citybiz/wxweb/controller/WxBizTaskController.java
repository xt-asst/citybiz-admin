package com.citybiz.wxweb.controller;

import com.citybiz.common.core.controller.BaseController;
import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.core.page.BizTableDataInfo;
import com.citybiz.common.core.page.TableDataInfo;
import com.citybiz.common.enums.biz.BizTaskStatusEnum;
import com.citybiz.common.exception.ServiceException;
import com.citybiz.common.utils.AssertUtils;
import com.citybiz.common.utils.DateUtils;
import com.citybiz.service.utils.WxUserAuthUtil;
import com.citybiz.system.domain.WxUser;
import com.citybiz.system.domain.biz.BizTaskManuscript;
import com.citybiz.system.domain.dto.BizTaskConsignDTO;
import com.citybiz.system.domain.dto.BizTaskDto;
import com.citybiz.system.domain.dto.BizTaskExtendInfoDTO;
import com.citybiz.system.domain.dto.BizTaskManuscriptDto;
import com.citybiz.system.domain.request.BizTaskRequest;
import com.citybiz.system.domain.vo.BizTaskConsignVO;
import com.citybiz.system.domain.vo.BizTaskExtendInfoVO;
import com.citybiz.system.domain.vo.BizTaskVo;
import com.citybiz.system.service.IWxUserService;
import com.citybiz.system.service.biz.IBizTaskManuscriptService;
import com.citybiz.system.service.biz.IBizTaskService;
import com.citybiz.wxweb.utils.WxUserUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Create by Ykg on 2023/10/19 23:46
 * effect: 任务控制器
 */
@RestController
@RequestMapping("/wx/task")
@Slf4j
@AllArgsConstructor
public class WxBizTaskController extends BaseController {

    private IBizTaskService bizTaskService;

    private IBizTaskManuscriptService bizTaskManuscriptService;

    private IWxUserService wxUserService;

    @PostMapping("/index/task")
    public AjaxResult indexTask(@RequestBody BizTaskRequest request ){
        BizTableDataInfo<BizTaskDto> bizTaskDtoBizTableDataInfo = bizTaskService.indexTask(request);
        BizTableDataInfo<BizTaskVo> bizTaskVoBizTableDataInfo = new BizTableDataInfo<>();
        BeanUtils.copyProperties(bizTaskDtoBizTableDataInfo, bizTaskVoBizTableDataInfo);
        List<BizTaskDto> rows = bizTaskDtoBizTableDataInfo.getRows();
        List<BizTaskVo> collect = rows.stream()
                .map(this::convert)
                .collect(Collectors.toList());
        bizTaskVoBizTableDataInfo.setRows(collect);
        return AjaxResult.success(bizTaskDtoBizTableDataInfo);
    }

    /**
     * 发布招标/悬赏任务
     * @return 发布结果
     */
    @PostMapping("/addTask")
    public AjaxResult addTask(@RequestBody BizTaskRequest request){
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        request.setEmployId(wxUser.getId());
        WxUserUtils.checkEmployRole();
        if( request.getTaskStartTime() == null){
            throw new ServiceException("任务开始时间不可为空");
        }

        if( request.getTaskEndTime() == null){
            throw new ServiceException("任务结束时间不可为空");
        }

        if(request.getTaskPrice() == null){
            throw new ServiceException("任务预算不可为空");
        }
        if(request.getTaskTypeId() == null){
            throw new ServiceException("所属行业不可为空");
        }
        AssertUtils.isNotEmpty(request.getTaskName(), "任务名称不可为空");
        AssertUtils.isNotEmpty(request.getTaskDetail(), "任务详情不可为空");
        AssertUtils.isNotEmpty(request.getTaskSort(), "请选择任务为招标/悬赏类型");
        bizTaskService.insertBizTask(request);
        return AjaxResult.success("任务发布成功");
    }


    /**
     * 任务详情信息
     * @param taskId
     * @return
     */
    @GetMapping("/getTaskInfo")
    public AjaxResult getTaskInfo(Long taskId){
        BizTaskDto bizTaskDto = bizTaskService.selectBizTaskById(taskId);
        return AjaxResult.success(convert(bizTaskDto));
    }

    /**
     * 威客的任务列表
     * @return 任务列表
     */
    @GetMapping("/myWorkerTasks")
    public BizTableDataInfo<BizTaskVo> myWorkerTasks(String taskSort){
        AssertUtils.isNotEmpty(taskSort, "任务类型不可为空");
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        WxUserUtils.checkWorkerRole();
        BizTableDataInfo<BizTaskDto> bizTaskDtoBizTableDataInfo = bizTaskService.selectBizTaskListByWorker(wxUser.getId(), taskSort);
        BizTableDataInfo<BizTaskVo> bizTaskVoBizTableDataInfo = new BizTableDataInfo<>();
        BeanUtils.copyProperties(bizTaskDtoBizTableDataInfo, bizTaskVoBizTableDataInfo);
        List<BizTaskDto> rows = bizTaskDtoBizTableDataInfo.getRows();
        List<BizTaskVo> collect = rows.stream()
                .map(this::convert)
                .collect(Collectors.toList());
        bizTaskVoBizTableDataInfo.setRows(collect);
        return bizTaskVoBizTableDataInfo;
    }

    /**
     * 雇主的任务列表
     * @return 任务列表
     */
    @GetMapping("/myEmployerTasks")
    public TableDataInfo myEmployerTasks(String taskSort){
        AssertUtils.isNotEmpty(taskSort, "任务类型不可为空");
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        WxUserUtils.checkEmployRole();
        BizTaskRequest query = new BizTaskRequest();
        query.setEmployId(wxUser.getId());
        startPage();
        query.setTaskSort(taskSort);
        List<BizTaskDto> bizTaskDtos = bizTaskService.selectBizTaskList(query);
        return getDataTable(bizTaskDtos);
    }

    // 任务评价接口
    @GetMapping("/evaluateTask")
    public AjaxResult evaluateTask(Long taskId, String evaluateMsg,Integer evaluateStar){
        return null;
    }


    /* -------------------------------- 招标任务控制器 --------------------------------*/
    /**
     * 招标任务投稿
     * @return 任务投稿结果
     */
    @PostMapping("/addTaskManuscript")
    public AjaxResult addTaskManuscript(@RequestBody BizTaskManuscript bizTaskManuscript){
        WxUserUtils.checkWorkerRole();
        AssertUtils.isNotEmpty(bizTaskManuscript.getManuscriptInfo(), "招标任务稿件说明不可为空");
        AssertUtils.isNotNull(bizTaskManuscript.getTaskId(), "任务id不可为空");
        if(bizTaskManuscript.getQuote() == null){
            throw new ServiceException("招标任务报价不可为空");
        }
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        bizTaskManuscript.setWorkerId(wxUser.getId());
        bizTaskManuscriptService.insertBizTaskManuscript(bizTaskManuscript);
        return AjaxResult.success("任务投稿成功");
    }

    /**
     * 获取招标任务下的招标信息列表
     * @param taskId 任务id
     * @return 查询结果
     */
    @GetMapping("/getTaskManuscripts")
    public AjaxResult getTaskManuscripts(Long taskId){
        AssertUtils.isNotNull(taskId, "任务id不可为空");
        BizTaskManuscript query = new BizTaskManuscript();
        query.setTaskId(taskId);
        List<BizTaskManuscriptDto> bizTaskManuscripts =
                bizTaskManuscriptService.selectBizTaskManuscriptList(query);
        return AjaxResult.success(bizTaskManuscripts);
    }

    /**
     * 选择招标任务下的招标信息作为中标信息
     * @param taskManuscriptId 标书id
     * @return 选中结果
     */
    @GetMapping("/selectedTaskManuscript")
    public AjaxResult selectedTaskManuscript(Long taskManuscriptId){
        AssertUtils.isNotNull(taskManuscriptId, "任务招标id不可为空");
        bizTaskManuscriptService.selectedTaskManuscript(taskManuscriptId);
        return AjaxResult.success("选中投稿信息成功");
    }

    public BizTaskVo convert(BizTaskDto bizTaskDto){
        BizTaskVo vo = new BizTaskVo();
        BeanUtils.copyProperties(bizTaskDto, vo);
        vo.setBizTaskManuscriptVoList(bizTaskManuscriptService.convertVO(bizTaskDto.getBizTaskManuscriptDtoList()));
        BizTaskExtendInfoDTO bizTaskExtendInfoDTO = bizTaskDto.getBizTaskExtendInfoDTO();
        BizTaskExtendInfoVO bizTaskExtendInfoVO = new BizTaskExtendInfoVO();
        BeanUtils.copyProperties(bizTaskExtendInfoDTO, bizTaskExtendInfoVO);
        vo.setBizTaskExtendInfoVO(bizTaskExtendInfoVO);
        vo.setTaskStatusName(BizTaskStatusEnum.getEnum(vo.getTaskStatus()).getMsg());

        Date rightTime = new Date();
        Date taskEndTime = bizTaskDto.getTaskEndTime();
        if(rightTime.getTime() > taskEndTime.getTime()){
            String timeLeftStr = DateUtils.getTimeLeftStr(taskEndTime, rightTime);
            if(!bizTaskDto.getTaskStatus().equals(BizTaskStatusEnum.EVALUATING.getCode())
                    && !(bizTaskDto.getTaskStatus().equals(BizTaskStatusEnum.TRADE_END.getCode()))){
                vo.setTimeLeftStr(timeLeftStr);
            }
        }

        BizTaskConsignDTO bizTaskConsignDTO = bizTaskDto.getBizTaskConsignDTO();
        if(bizTaskConsignDTO != null) {
            BizTaskConsignVO bizTaskConsignVO = new BizTaskConsignVO();
            BeanUtils.copyProperties(bizTaskConsignDTO, bizTaskConsignVO);
            // 交付人id
            Long consignId = bizTaskConsignDTO.getConsignId();
            if(consignId != null) {
                WxUser consign = wxUserService.selectWxUserById(consignId);
                bizTaskConsignVO.setConsignAvatar(consign.getAvatar());
                bizTaskConsignVO.setConsignName(consign.getUsername());
                vo.setBizTaskConsignVO(bizTaskConsignVO);
            }
        }
        return vo;
    }


}
