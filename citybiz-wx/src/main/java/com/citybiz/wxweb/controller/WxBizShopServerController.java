package com.citybiz.wxweb.controller;

import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.enums.biz.ServerTypeEnum;
import com.citybiz.common.exception.ServiceException;
import com.citybiz.common.utils.AssertUtils;
import com.citybiz.service.utils.WxUserAuthUtil;
import com.citybiz.system.domain.WxUser;
import com.citybiz.system.domain.WxUserRole;
import com.citybiz.system.domain.biz.BizShop;
import com.citybiz.system.domain.biz.BizShopCase;
import com.citybiz.system.domain.biz.BizShopServer;
import com.citybiz.system.service.biz.IBizShopCaseService;
import com.citybiz.system.service.biz.IBizShopServerService;
import com.citybiz.system.service.biz.IBizShopService;
import com.citybiz.wxcommon.enums.WxUserRoleEnums;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Create by Ykg on 2023/10/18 22:37
 * effect: 店铺作品控制器
 */
@RestController
@RequestMapping("/wx/shopServer")
@Slf4j
@AllArgsConstructor
public class WxBizShopServerController {

    private IBizShopServerService bizShopServerService;

    private IBizShopService bizShopService;

    private IBizShopCaseService bizShopCaseService;

    /**
     * 店铺作品增加
     * @param bizShopServer
     * @return
     */
    @PostMapping("/addShopServer")
    public AjaxResult addShopServer(@RequestBody BizShopServer bizShopServer){
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        WxUserRole wxUserRole = wxUser.getWxUserRole();
        if(!WxUserRoleEnums.WORKER_ROLE.getCode().equals(wxUserRole.getRole())){
            throw new ServiceException("请使用威客角色操作");
        }
        AssertUtils.isNotEmpty(bizShopServer.getServerName(), "服务名称不可为空");
        AssertUtils.isNotEmpty(bizShopServer.getServerType(), "服务类型不可为空");
        if(!ServerTypeEnum.contains(bizShopServer.getServerType())){
            throw new ServiceException("服务类型不在范围内，请确认");
        }
        AssertUtils.isNotNull(bizShopServer.getBizShopId(), "店铺id不可为空");
        if(bizShopServer.getPrice() == null){
            throw new ServiceException("服务类型价格不可为空");
        }
        bizShopServerService.insertBizShopServer(bizShopServer);
        return AjaxResult.success("新增成功");
    }

    /**
     * 店铺作品增加
     * @param shopServerId 店铺作品/服务id
     * @return
     */
    @PostMapping("/getShopServer")
    public AjaxResult getShopServer(Long shopServerId){
        AssertUtils.isNotNull(shopServerId, "服务/作品ID不可为空");
        BizShopServer bizShopServer = bizShopServerService.selectBizShopServerById(shopServerId);
        return AjaxResult.success(bizShopServer);
    }

    /**
     * 店铺作品查询
     * @param shopId 店铺id
     * @param sererType 作品类型 work:作品 server:服务
     * @return 查询列表
     */
    @GetMapping("/queryShopServer")
    public AjaxResult queryShopServer(Long shopId, String sererType){
        AssertUtils.isNotNull(shopId, "店铺id不可为空");
        if(!ServerTypeEnum.contains(sererType)){
            throw new ServiceException("服务类型不在范围内，请确认");
        }

        List<BizShopServer> bizShopServers = bizShopServerService.queryShopServer(shopId, sererType);
        return AjaxResult.success(bizShopServers);
    }

    @GetMapping("/deleteShopServer")
    public AjaxResult queryShopServer(Long shopServerId){
        AssertUtils.isNotNull(shopServerId, "店铺服务id不可为空");
        bizShopServerService.deleteBizShopServerById(shopServerId);
        return AjaxResult.success("删除成功");
    }


    /**
     * 店铺作品修改
     * @param bizShopServer
     * @return
     */
    @PostMapping("/updateShopServer")
    public AjaxResult updateShopServer(@RequestBody BizShopServer bizShopServer){
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        WxUserRole wxUserRole = wxUser.getWxUserRole();
        if(!WxUserRoleEnums.WORKER_ROLE.getCode().equals(wxUserRole.getRole())){
            throw new ServiceException("请使用威客角色操作");
        }
        AssertUtils.isNotNull(bizShopServer.getId(), "服务id不可为空");
        AssertUtils.isNotEmpty(bizShopServer.getServerName(), "服务名称不可为空");
        AssertUtils.isNotEmpty(bizShopServer.getServerType(), "服务类型不可为空");
        if(!ServerTypeEnum.contains(bizShopServer.getServerType())){
            throw new ServiceException("服务类型不在范围内，请确认");
        }
        AssertUtils.isNotNull(bizShopServer.getBizShopId(), "店铺id不可为空");
        if(bizShopServer.getPrice() == null){
            throw new ServiceException("服务类型价格不可为空");
        }
        bizShopServerService.updateBizShopServer(bizShopServer);
        return AjaxResult.success("新增成功");
    }


    /**
     * 服务购买
     * @param shopServerId 服务id
     * @param num 购买数量
     * @return 购买结果
     */
    @GetMapping("/byShopServer")
    public AjaxResult byShopServer(Long shopServerId, Long num){
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        WxUserRole wxUserRole = wxUser.getWxUserRole();
        if(!WxUserRoleEnums.EMPLOYER_ROLE.getCode().equals(wxUserRole.getRole())){
            throw new ServiceException("请使用雇主角色操作");
        }
        bizShopServerService.byShopServer(wxUser.getId(), shopServerId,num);
        return AjaxResult.success("创建订单成功");
    }

    @GetMapping("/shop/{id}")
    public AjaxResult getShopById(@PathVariable Long id){
        BizShop bizShop = bizShopService.selectBizShopById(id);
        if(bizShop == null){
            throw new ServiceException("商店不存在，请确认");
        }
        BizShopCase select = new BizShopCase();
        select.setShopId(bizShop.getId());
        select.setDelFlag(0L);
        List<BizShopCase> bizShopCases = bizShopCaseService.selectBizShopCaseList(select);
        bizShop.setBizShopCases(bizShopCases);
        return AjaxResult.success(bizShop);
    }

    @PostMapping("/addShopCase")
    public AjaxResult addShopCase(@RequestBody BizShopCase bizShopCase){
        AssertUtils.isNotEmpty(bizShopCase.getCaseName(), "案例名称不可为空");
        AssertUtils.isNotNull(bizShopCase.getShopId(), "案例商店id不可为空");
        bizShopCaseService.insertBizShopCase(bizShopCase);
        return AjaxResult.success();
    }

    @PostMapping("/updateShopCase")
    public AjaxResult updateShopCase(@RequestBody BizShopCase bizShopCase){
        AssertUtils.isNotEmpty(bizShopCase.getCaseName(), "案例名称不可为空");
        AssertUtils.isNotNull(bizShopCase.getShopId(), "案例商店id不可为空");
        AssertUtils.isNotNull(bizShopCase.getId(), "案例id不可为空");
        bizShopCaseService.updateBizShopCase(bizShopCase);
        return AjaxResult.success();
    }

    @PostMapping("/delShopCase/{id}")
    public AjaxResult delShopCase(@PathVariable Long id){
        bizShopCaseService.deleteBizShopCaseById(id);
        return AjaxResult.success();
    }

    @GetMapping("/getShopCaseByShopId")
    public AjaxResult getShopCases(Long shopId){
        BizShop bizShop = bizShopService.selectBizShopById(shopId);
        if(bizShop == null){
            throw new ServiceException("商店不存在，请确认");
        }
        BizShopCase select = new BizShopCase();
        select.setShopId(bizShop.getId());
        select.setDelFlag(0L);
        List<BizShopCase> bizShopCases = bizShopCaseService.selectBizShopCaseList(select);
        bizShop.setBizShopCases(bizShopCases);
        return AjaxResult.success(bizShopCases);
    }

    @GetMapping("/getShopCase")
    public AjaxResult getShopCase(Long shopCaseId){
        BizShopCase bizShopCase = bizShopCaseService.selectBizShopCaseById(shopCaseId);
        return AjaxResult.success(bizShopCase);
    }

}
