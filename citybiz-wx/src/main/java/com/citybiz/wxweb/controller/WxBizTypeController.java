package com.citybiz.wxweb.controller;

import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.system.domain.biz.BizTaskType;
import com.citybiz.system.service.biz.IBizTaskTypeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Create by Ykg on 2023/10/24 00:39
 * effect: 业务类型控制器
 */
@RestController
@RequestMapping("/wxIndex/bizType")
@Slf4j
public class WxBizTypeController {

    @Autowired
    private IBizTaskTypeService bizTaskTypeService;


    @GetMapping("/getTypes")
    public AjaxResult getParentTypes(){
        List<BizTaskType> parentTypes = bizTaskTypeService.getParentTypes();
        return AjaxResult.success(parentTypes);
    }

//    @GetMapping("/getChildTypes")
    public AjaxResult getChildTypes(Long parentId){
        List<BizTaskType> childTypes = bizTaskTypeService.getChildTypes(parentId);
        return AjaxResult.success(childTypes);
    }




}
