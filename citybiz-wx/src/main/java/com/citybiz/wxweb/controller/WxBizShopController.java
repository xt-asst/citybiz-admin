package com.citybiz.wxweb.controller;

import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.enums.biz.ServerTypeEnum;
import com.citybiz.common.exception.ServiceException;
import com.citybiz.common.utils.AssertUtils;
import com.citybiz.service.utils.WxUserAuthUtil;
import com.citybiz.system.domain.WxUser;
import com.citybiz.system.domain.WxUserRole;
import com.citybiz.system.domain.biz.*;
import com.citybiz.system.service.biz.*;
import com.citybiz.wxcommon.enums.WxUserRoleEnums;
import com.citybiz.wxweb.utils.WxUserUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Create by Ykg on 2023/10/4 22:17
 * effect: 店铺管理控制类
 */
@RestController
@RequestMapping("/wx/shop")
@Slf4j
public class WxBizShopController {

    @Autowired
    private IBizShopService bizShopService;

    @Autowired
    private IBizShopCaseService bizShopCaseService;

    @Autowired
    private IBizWxPrivateApproveInfoService bizWxPrivateApproveInfoService;

    @PostMapping("/addShop")
    public AjaxResult addShop(@RequestBody BizShop bizShop){
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        WxUserUtils.checkWorkerRole();
        AssertUtils.isNotEmpty(bizShop.getName(), "请输入店铺名称");
        AssertUtils.isNotEmpty(bizShop.getAvatar(), "请上传店铺头像");
        AssertUtils.isNotNull(bizShop.getAddressId(), "请输入地址id");
        AssertUtils.isNotNull(bizShop.getBizTaskId(), "行业类型不可为空");
        bizShop.setWorkerId(wxUser.getId());
        checkApprove(wxUser.getId());
        bizShopService.insertBizShop(bizShop);
        return AjaxResult.success("新增成功");
    }


    /**
     * 店铺案例修改
     * @param bizShop
     * @return
     */
    @PostMapping("/editShop")
    public AjaxResult editShop(@RequestBody BizShop bizShop){
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        WxUserUtils.checkWorkerRole();
        AssertUtils.isNotEmpty(bizShop.getName(), "请输入店铺名称");
        AssertUtils.isNotEmpty(bizShop.getAvatar(), "请上传店铺头像");
        AssertUtils.isNotNull(bizShop.getAddressId(), "请输入地址id");
        AssertUtils.isNotNull(bizShop.getBizTaskId(), "行业类型不可为空");
        bizShop.setWorkerId(wxUser.getId());
        checkApprove(wxUser.getId());
        bizShopService.updateBizShop(bizShop);
        return AjaxResult.success("修改成功");
    }

    @GetMapping("/getMyShop")
    public AjaxResult getMyShop(){
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        WxUserUtils.checkWorkerRole();
        checkApprove(wxUser.getId());
        BizShop bizShopForUserId = bizShopService.getBizShopForUserId(wxUser.getId());
        return AjaxResult.success(bizShopForUserId);

    }

    /**
     * 店铺案例增加
     * @param bizShopCase
     * @return
     */
    @PostMapping("/addShopCase")
    public AjaxResult addShopCase(@RequestBody BizShopCase bizShopCase){
        WxUserUtils.checkWorkerRole();
        AssertUtils.isNotEmpty(bizShopCase.getCaseName(), "案例名称不可为空");
        AssertUtils.isNotEmpty(bizShopCase.getCaseDetail(), "案例描述不可为空");
        AssertUtils.isNotEmpty(bizShopCase.getCaseImg(), "案例图片不可为空");
        if(bizShopCase.getPrice() == null ){
            throw new ServiceException("案例价格不可为空");
        }
        bizShopCaseService.insertBizShopCase(bizShopCase);
        return AjaxResult.success("新增成功");
    }

    private void checkApprove(Long userId){
        BizWxPrivateApproveInfo bizWxPrivateApproveInfo = bizWxPrivateApproveInfoService.queryApproveForUserId(userId);
        if(bizWxPrivateApproveInfo == null || bizWxPrivateApproveInfo.getPassFlag() != 1L){
            throw new ServiceException("请先进行实名认证，如已提交，请联系管理员审核");
        }
    }



}
