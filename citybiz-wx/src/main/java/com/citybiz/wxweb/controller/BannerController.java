package com.citybiz.wxweb.controller;

import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.system.service.biz.IBizBannerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Create by Ykg on 2023/9/29 17:34
 * effect: 首页轮播图控制器
 */
@RestController
@RequestMapping("/wxIndex/banner")
@Slf4j
public class BannerController {

    @Autowired
    private IBizBannerService iBizBannerService;

    @GetMapping("/{count}")
    public AjaxResult getHomeBannerList(@PathVariable("count") int count){
        System.out.println("321321");
        return AjaxResult.success(iBizBannerService.getHomeBannerList(count));
    }

}
