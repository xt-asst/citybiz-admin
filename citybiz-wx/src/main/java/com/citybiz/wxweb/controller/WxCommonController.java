package com.citybiz.wxweb.controller;


import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.utils.file.FileUploadUtils;
import com.citybiz.system.service.impl.TencentFileUploadServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * Create by Ykg on 2023/10/27 22:59
 * effect:
 */

@RestController
@RequestMapping("/wx/common")
public class WxCommonController {


    @Autowired
    private TencentFileUploadServiceImpl tencentFileUploadService;


    /**
     * cos通用上传请求（单个）
     */
    @PostMapping("/cosUpload")
    public AjaxResult cosUploadFile(MultipartFile file) throws Exception
    {
        AjaxResult ajax = AjaxResult.success();
        String suffix = "." + FileUploadUtils.getExtension(file);
        String fileKey = tencentFileUploadService.upload(FileUploadUtils.transferToFile(file), suffix);
        ajax.put("url", fileKey);
        return ajax;
    }

}
