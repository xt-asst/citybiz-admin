package com.citybiz.wxweb.controller;

import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.exception.ServiceException;
import com.citybiz.common.utils.AssertUtils;
import com.citybiz.service.utils.WxUserAuthUtil;
import com.citybiz.system.domain.WxUser;
import com.citybiz.system.domain.WxUserRole;
import com.citybiz.system.domain.biz.BizShop;
import com.citybiz.system.domain.biz.WxUserOrder;
import com.citybiz.system.service.biz.IBizShopService;
import com.citybiz.system.service.biz.IWxUserOrderService;
import com.citybiz.wxcommon.enums.WxUserRoleEnums;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.awt.print.PrinterException;
import java.util.List;

/**
 * Create by Ykg on 2023/10/18 23:29
 * effect: 微信用户订单控制器
 */
@RestController
@RequestMapping("/wx/wxUserOrder")
@Slf4j
public class WxUserOrderController {

    @Autowired
    private IWxUserOrderService wxUserOrderService;

    @Autowired
    private IBizShopService bizShopService;

    /**
     * 雇主查询购买的订单
     * @param wxUserOrder 订单查询
     * @return 查询结果
     */
    @PostMapping("/query")
    public AjaxResult query(@RequestBody WxUserOrder wxUserOrder){
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        WxUserRole wxUserRole = wxUser.getWxUserRole();
        if(!WxUserRoleEnums.EMPLOYER_ROLE.getCode().equals(wxUserRole.getRole())){
            throw new ServiceException("请使用雇主角色操作");
        }
        AssertUtils.isNotEmpty(wxUserOrder.getServerType(), "订单服务类型不可为空");
        wxUserOrder.setWxUserId(wxUserRole.getWxUserId());
        List<WxUserOrder> wxUserOrders = wxUserOrderService.queryList(wxUserOrder);
        return AjaxResult.success(wxUserOrders);
    }


    /**
     * 威客查询我的订单
     * @param wxUserOrder 订单查询
     * @return 查询结果
     */
    @PostMapping("/workerQuery")
    public AjaxResult workerQuery(@RequestBody WxUserOrder wxUserOrder){
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        WxUserRole wxUserRole = wxUser.getWxUserRole();
        if(!WxUserRoleEnums.WORKER_ROLE.getCode().equals(wxUserRole.getRole())){
            throw new ServiceException("请使用威客角色操作");
        }

        Long wxUserId = wxUserRole.getWxUserId();
        BizShop bizShopForUserId = bizShopService.getBizShopForUserId(wxUserId);
        if(bizShopForUserId == null){
            throw new ServiceException("当前店铺不存在，请确认");
        }

        AssertUtils.isNotEmpty(wxUserOrder.getServerType(), "订单服务类型不可为空");
        wxUserOrder.setShopId(bizShopForUserId.getId());
        List<WxUserOrder> wxUserOrders = wxUserOrderService.queryList(wxUserOrder);
        return AjaxResult.success(wxUserOrders);
    }

}
