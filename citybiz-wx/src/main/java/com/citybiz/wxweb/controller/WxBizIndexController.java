package com.citybiz.wxweb.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.system.domain.biz.BizShop;
import com.citybiz.system.service.biz.IBizShopService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Create by Ykg on 2023/10/24 21:31
 * effect: 业务首页控制器
 */
@RestController
@RequestMapping("/wxIndex")
@Slf4j
public class WxBizIndexController {

    @Autowired
    private IBizShopService bizShopService;


    /**
     * 店铺列表查询
     * @param bizShop 店铺对象
     * @return 查询结果
     */
    @PostMapping("/queryShop")
    public AjaxResult queryShop(@RequestBody BizShop bizShop, Long pageNo, Long pageSize){
        if(pageNo == null){
            pageNo = 1L;
        }
        if(pageSize == null){
            pageSize = 10L;
        }
        BizShop query = new BizShop();
        query.setName(bizShop.getName());
        query.setId(bizShop.getId());
        query.setId(bizShop.getBizTaskId());
        Page<BizShop> page = bizShopService.selectBizShopListPage(query, true, pageNo, pageSize);
        return AjaxResult.success(page);
    }

    @GetMapping("/getRecommendShop")
    public AjaxResult getNewShop(){
        List<BizShop> newShop = bizShopService.getNewShop();
        return AjaxResult.success(newShop);
    }
}
