package com.citybiz.wxweb.controller;

import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.utils.AssertUtils;
import com.citybiz.system.domain.biz.BizAddressTable;
import com.citybiz.system.service.biz.IBizAddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Create by Ykg on 2023/10/19 15:39
 * effect:
 */
@RestController
@RequestMapping("/wx/address")
@Slf4j
public class BizAddressController {

    @Autowired
    private IBizAddressService bizAddressService;

    @GetMapping("/query")
    public AjaxResult query(Long parentId, String type){
        AssertUtils.isNotEmpty(type, "查询类型不可为空");
        List<BizAddressTable> bizAddress = bizAddressService.getBizAddress(parentId, type);
        return AjaxResult.success(bizAddress);
    }

}