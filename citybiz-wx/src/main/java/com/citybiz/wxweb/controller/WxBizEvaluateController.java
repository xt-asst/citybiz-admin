package com.citybiz.wxweb.controller;

import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.enums.biz.EvaluateLevelEnum;
import com.citybiz.common.enums.biz.EvaluateTypeEnum;
import com.citybiz.common.exception.ServiceException;
import com.citybiz.common.utils.AssertUtils;
import com.citybiz.system.domain.biz.BizEvaluate;
import com.citybiz.system.service.biz.IBizEvaluateListService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Create by Ykg on 2023/10/18 23:46
 * effect: 评价控制器
 */
@RestController
@RequestMapping("/wx/evaluate")
@Slf4j
public class WxBizEvaluateController {

    @Autowired
    private IBizEvaluateListService bizEvaluateListService;

    @GetMapping("/query")
    public AjaxResult query(Long bizId, String evaluateType){
        if(!EvaluateTypeEnum.contains(evaluateType)){
            throw new ServiceException("评价类型有误，请确认");
        }

        BizEvaluate query = new BizEvaluate();
        query.setBizId(bizId);
        query.setEvaluateType(evaluateType);
        List<BizEvaluate> bizEvaluates = bizEvaluateListService.selectBizEvaluateListList(query);
        return AjaxResult.success(bizEvaluates);
    }

    /**
     * 店铺评价
     * @return 评价结果
     */
    @GetMapping("/add")
    public AjaxResult add(@RequestBody BizEvaluate bizEvaluate){
        AssertUtils.isNotNull(bizEvaluate.getBizId(), "业务id不可为空");
        if(!EvaluateTypeEnum.contains(bizEvaluate.getEvaluateType())){
            throw new ServiceException("评价类型有误，请确认");
        }
        if(bizEvaluate.getEvaluateFraction() == null){
            throw new ServiceException("评价分数不可为空");
        }

        if(!EvaluateLevelEnum.contains(bizEvaluate.getEvaluateLevel())){
            throw new ServiceException("评价等级有误，请确认");
        }

        bizEvaluateListService.insertBizEvaluateList(bizEvaluate);
        return AjaxResult.success("评价成功");
    }

}
