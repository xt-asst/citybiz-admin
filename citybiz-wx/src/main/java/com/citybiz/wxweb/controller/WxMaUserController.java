package com.citybiz.wxweb.controller;

import cn.binarywang.wx.miniapp.api.WxMaService;
import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.exception.WxBizException;
import com.citybiz.common.utils.AssertUtils;
import com.citybiz.service.utils.WxUserAuthUtil;
import com.citybiz.service.web.request.WxLoginRequest;
import com.citybiz.service.web.service.WxLoginService;
import com.citybiz.system.domain.WxUser;
import com.citybiz.system.domain.WxUserRole;
import com.citybiz.system.domain.biz.BizWxCompanyApproveInfo;
import com.citybiz.system.domain.biz.BizWxPrivateApproveInfo;
import com.citybiz.system.service.IWxUserService;
import com.citybiz.system.service.biz.IBizWxCompanyApproveInfoService;
import com.citybiz.system.service.biz.IBizWxPrivateApproveInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.config.web.servlet.oauth2.client.OAuth2ClientSecurityMarker;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Create by Ykg on 2023/9/28 23:48
 * effect: 微信相关登录接口
 */
@RestController
@RequestMapping("/wx/user")
@Slf4j
public class WxMaUserController {

    private final WxMaService wxMaService;

    @Autowired
    private WxLoginService wxLoginService;

    @Autowired
    private IWxUserService wxUserService;

    @Autowired
    private IBizWxPrivateApproveInfoService bizWxPrivateApproveInfoService;

    @Autowired
    private IBizWxCompanyApproveInfoService bizWxCompanyApproveInfoService;

    // 令牌标识
    @Value("${token.header}")
    private String header;

    public WxMaUserController(WxMaService wxMaService){
        this.wxMaService = wxMaService;
    }

    /**
     * 登陆接口
     */
    @PostMapping("/login")
    public AjaxResult login(@RequestBody  WxLoginRequest request) {

        AssertUtils.isNotEmpty(request.getCode(), "code不可为空");
        if(request.getRole() == null
                || (!request.getRole().equals(0) && !request.getRole().equals(1)) ){
            request.setRole(0);
        }

        String token = wxLoginService.login(request);
        AjaxResult success = AjaxResult.success("登录成功");
        success.put(header, token);
        return success;
    }

    /**
     * 升级用户信息接口
     * @param wxUser 用户基础信息
     * @return 更新结果
     */
    @PostMapping("/update")
    public AjaxResult updateUserInfo(@RequestBody WxUser wxUser){
        WxUser oldWxUser = WxUserAuthUtil.getWxUser();
        wxUser.setId(oldWxUser.getId());
        wxUserService.updateWxUser(wxUser);
        return AjaxResult.success("信息更新成功");
    }

    @GetMapping("/getWxUser")
    public AjaxResult getWxUser(){
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        wxUser.setPayPassword(null);
        wxUser.setLoginPassword(null);
        wxUser.setOpenId(null);
        wxUser.setParams(null);
        return AjaxResult.success(wxUser);
    }

    @GetMapping("/changeRole/{role}")
    public AjaxResult changeRole(@PathVariable("role")Integer role, HttpServletRequest request){
        if(role != 0 && role != 1){
            throw new WxBizException("角色切换错误，请确认");
        }

        WxUser wxUser = WxUserAuthUtil.getWxUser();
        WxUserRole wxUserRole = wxUser.getWxUserRole();
        if(wxUserRole.getRole().equals(role)){
            return AjaxResult.success("当前无需切换");
        }

        //删除登录信息
        String token = wxLoginService.getToken(request);
        wxLoginService.delWxUser(token);

        //生成新的登录信息
        String newToken = wxLoginService.changeRole(wxUser, role);
        AjaxResult success = AjaxResult.success("登录成功");
        success.put(header, newToken);
        return success;
    }

    /**
     * 私人认证接口
     * @return 认证结果
     */
    @PostMapping("/privateAuthApprove")
    public AjaxResult privateAuthApprove(@RequestBody BizWxPrivateApproveInfo bizWxPrivateApproveInfo){
        AssertUtils.isNotEmpty(bizWxPrivateApproveInfo.getRealName(), "真实姓名不可为空");
        AssertUtils.isNotEmpty(bizWxPrivateApproveInfo.getIdentityCard(), "身份证号不可为空");
        AssertUtils.isNotEmpty(bizWxPrivateApproveInfo.getIdentityCardFrontImg(), "身份证国徽面不可为空");
        AssertUtils.isNotEmpty(bizWxPrivateApproveInfo.getIdentityCardBackImg(), "身份证人像面不可为空");
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        bizWxPrivateApproveInfo.setWxUserId(wxUser.getId());
        BizWxPrivateApproveInfo oldInfo = bizWxPrivateApproveInfoService.selectByUserId(wxUser.getId());
        // 已存在则更新
        if(oldInfo != null){
            bizWxPrivateApproveInfo.setId(oldInfo.getId());
            bizWxPrivateApproveInfoService.updateBizWxPrivateApproveInfo(bizWxPrivateApproveInfo);
        }else {
            bizWxPrivateApproveInfoService.insertBizWxPrivateApproveInfo(bizWxPrivateApproveInfo);
        }
        return AjaxResult.success();
    }

    @PostMapping("/companyAuthApprove")
    public AjaxResult companyAuthApprove(@RequestBody BizWxCompanyApproveInfo bizWxCompanyApproveInfo){
        AssertUtils.isNotEmpty(bizWxCompanyApproveInfo.getCompanyName(), "公司名称不可为空");
        AssertUtils.isNotNull(bizWxCompanyApproveInfo.getBizTypeId(), "所属行业id不可为空");
        AssertUtils.isNotNull(bizWxCompanyApproveInfo.getAddressId(), "公司地址id不可为空");
        AssertUtils.isNotEmpty(bizWxCompanyApproveInfo.getUnifiedSocialCreditCode(), "统一社会信用码不可为空");
        AssertUtils.isNotEmpty(bizWxCompanyApproveInfo.getAddressDetail(), "公司详细地址不可为空");
        AssertUtils.isNotEmpty(bizWxCompanyApproveInfo.getProveImages(), "公司资质图片不可为空");
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        bizWxCompanyApproveInfo.setWxUserId(wxUser.getId());
        BizWxCompanyApproveInfo oldInfo = bizWxCompanyApproveInfoService.selectByUserId(wxUser.getId());
        if(oldInfo != null){
            bizWxCompanyApproveInfo.setId(oldInfo.getId());
            bizWxCompanyApproveInfoService.updateBizWxCompanyApproveInfo(bizWxCompanyApproveInfo);
        }else {
            bizWxCompanyApproveInfoService.insertBizWxCompanyApproveInfo(bizWxCompanyApproveInfo);
        }

        return AjaxResult.success("提交企业认证信息成功");

    }

    @GetMapping("/getCompanyAuthInfo")
    public AjaxResult getCompanyAuthInfo(){
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        BizWxCompanyApproveInfo bizWxCompanyApproveInfo = bizWxCompanyApproveInfoService.selectByUserId(wxUser.getId());
        return AjaxResult.success(bizWxCompanyApproveInfo);
    }

    @GetMapping("/getPrivateAuthInfo")
    public AjaxResult getPrivateAuthInfo(){
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        BizWxPrivateApproveInfo bizWxPrivateApproveInfo = bizWxPrivateApproveInfoService.selectByUserId(wxUser.getId());
        return AjaxResult.success(bizWxPrivateApproveInfo);
    }


}
