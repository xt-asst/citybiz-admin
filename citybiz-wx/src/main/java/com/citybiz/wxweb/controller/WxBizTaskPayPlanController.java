package com.citybiz.wxweb.controller;

import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.exception.ServiceException;
import com.citybiz.common.utils.AssertUtils;
import com.citybiz.service.utils.WxUserAuthUtil;
import com.citybiz.system.domain.WxUser;
import com.citybiz.system.domain.dto.BizPayPlanDTO;
import com.citybiz.system.domain.dto.BizTaskConsignDTO;
import com.citybiz.system.service.biz.IBizTaskPayPlanService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * Create by Ykg on 2024/2/11 17:42
 * effect: 微信任务支付方案控制器
 */
@RestController
@RequestMapping("/wx/payPlan")
@Slf4j
@AllArgsConstructor
public class WxBizTaskPayPlanController {

    private IBizTaskPayPlanService bizTaskPayPlanService;

    // 查看付款方案列表
    @GetMapping("/getPayPlanList")
    public AjaxResult getPayPlanList(Long taskId){
        AssertUtils.isNotNull(taskId, "任务id不可为空");
        List<BizPayPlanDTO> payPlanListForTaskId = bizTaskPayPlanService.getPayPlanListForTaskId(taskId);
        return AjaxResult.success(payPlanListForTaskId);
    }

    /* -------- 雇主控制器 --------*/
    // 添加付款方案
    @PostMapping("/addPayPlan")
    public AjaxResult addPayPlan(@RequestBody BizPayPlanDTO bizPayPlan){
        AssertUtils.isNotEmpty(bizPayPlan.getRemark(), "阶段备注不可为空");
        AssertUtils.isNotEmpty(bizPayPlan.getPlanStage(), "阶段不可为空");
        AssertUtils.isNotNull(bizPayPlan.getPayProp(), "付款比例不可为空");
        AssertUtils.isNotNull(bizPayPlan.getTaskId(), "任务id不可为空");
        if(bizPayPlan.getPayAmount() == null){
            throw new ServiceException("付款金额不可为空");
        }
        bizTaskPayPlanService.addPayPlan(bizPayPlan);
        return AjaxResult.success("添加付款方案成功");
    }

    // 修改付款方案
    @PostMapping("/updatePayPlan")
    public AjaxResult updatePayPlan(@RequestBody BizPayPlanDTO bizPayPlan){
        AssertUtils.isNotEmpty(bizPayPlan.getRemark(), "阶段备注不可为空");
        AssertUtils.isNotEmpty(bizPayPlan.getPlanStage(), "阶段不可为空");
        AssertUtils.isNotNull(bizPayPlan.getPayProp(), "付款比例不可为空");
        if(bizPayPlan.getPayAmount() == null){
            throw new ServiceException("付款金额不可为空");
        }
        bizTaskPayPlanService.updatePayPlan(bizPayPlan);
        return AjaxResult.success("修改付款方案成功");
    }

    // 提交付款方案
    @GetMapping("/submitPayPlan")
    public AjaxResult submitPayPlan(Long taskId){
        AssertUtils.isNotNull(taskId, "任务id不可为空");
        bizTaskPayPlanService.submitPayPlan(taskId);
        return AjaxResult.success("提交付款方案成功");
    }

    /**
     * 托管付款阶段金额
     * @param taskPayPlanId 任务阶段id
     * @param payAmount 托管金额
     * @return
     */
    @GetMapping("/hostingPayPlanAmount")
    public AjaxResult hostingPayPlanAmount(Long taskPayPlanId, BigDecimal payAmount){
        // 托管金额操作，正常来说需要到微信下单然后返回订单支付，这里直接进行支付成功
        AssertUtils.isNotNull(taskPayPlanId, "付款方案id不可为空");
        bizTaskPayPlanService.hostingPayPlanAmount(taskPayPlanId,payAmount);
        return AjaxResult.success("托管金额成功");
    }

    // 支付金额
    @GetMapping("/payPlanAmount")
    public AjaxResult payPlanAmount(Long taskPayPlanId, BigDecimal payAmount){
        // 托管金额操作，正常来说需要到微信下单然后返回订单支付，这里直接进行支付成功
        AssertUtils.isNotNull(taskPayPlanId, "付款方案id不可为空");
        bizTaskPayPlanService.payPlanAmount(taskPayPlanId,payAmount);
        return AjaxResult.success("支付金额成功");
    }

    // 确认交付
    @GetMapping("/confirmConsign")
    public AjaxResult confirmConsign(Long taskPayPlanId){
        AssertUtils.isNotNull(taskPayPlanId, "付款方案id不可为空");
        bizTaskPayPlanService.confirmConsign(taskPayPlanId);
        return AjaxResult.success("确认交付成功");
    }
    // 交易维权


    /* -------- 威客控制器 --------*/
    /**
     * 威客确认付款方案
     * @param taskId 任务id
     * @param remark 备注信息
     * @param confirmFlag 是否同意 0: 不同意 1: 同意
     * @return 确认结果
     */
    @GetMapping("/confirmPayPlan")
    public AjaxResult confirmPayPlan(Long taskId, String remark, Integer confirmFlag){
        AssertUtils.isNotNull(taskId, "任务id不可为空");
        bizTaskPayPlanService.confirmPayPlan(taskId,remark,confirmFlag);
        return AjaxResult.success("确认付款方案成功");
    }

    // 交付作品
    @PostMapping("/consignWorker")
    public AjaxResult consignWorker(@RequestBody BizTaskConsignDTO bizTaskConsignDTO){
        AssertUtils.isNotEmpty(bizTaskConsignDTO.getRemark(), "交付备注不可为空");
        AssertUtils.isNotEmpty(bizTaskConsignDTO.getFileUrls(), "交付文件不可为空");
        AssertUtils.isNotNull(bizTaskConsignDTO.getTaskId(), "所属任务id不可为空");
        AssertUtils.isNotNull(bizTaskConsignDTO.getTaskPayPlanId(), "所属交付阶段id不可为空");
        WxUser wxUser = WxUserAuthUtil.getWxUser();
        bizTaskConsignDTO.setConsignId(wxUser.getId());
        bizTaskPayPlanService.consignWorker(bizTaskConsignDTO);
        return AjaxResult.success("交付作品成功");
    }

    // 交易维权

}
