package com.citybiz.system.domain.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * 任务投标信息对象 biz_task_manuscript
 *
 * @author citybiz
 * @date 2023-10-18
 */
@Data
public class BizTaskManuscriptDto {
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 稿件说明 */
    private String manuscriptInfo;

    /** 创建时间 */
    private Date createTime;

    /** 稿件地址 */
    private String manuscriptUrls;

    /** 任务id */
    private Long taskId;

    /** 工作者id */
    private Long workerId;

    /** 报价金额 */
    private BigDecimal quote;

    private String feature;

    private Map<String,String> featureContext;
}
