package com.citybiz.system.domain.process.req;

import lombok.Data;

/**
 * Create by Ykg on 2023/10/25 11:49
 * effect: 主线任务请求体
 */
@Data
public class MasterHandlerRequest {

    private Long taskId;

    private Long taskManuscriptId;

}
