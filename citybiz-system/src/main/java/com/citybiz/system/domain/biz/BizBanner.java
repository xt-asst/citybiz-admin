package com.citybiz.system.domain.biz;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.citybiz.common.core.domain.BasePlusEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.citybiz.common.annotation.Excel;
import com.citybiz.common.core.domain.BaseEntity;

/**
 * banner轮播图列对象 biz_banner_list
 * 
 * @author ykg
 * @date 2023-09-29
 */
@TableName("biz_banner_list")
@Data
public class BizBanner extends BasePlusEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /** 图片地址 */
    @Excel(name = "图片地址")
    private String bannerUrl;

    /** 轮播图名称 */
    @Excel(name = "轮播图名称")
    private String bannerName;

    /** 创建人 */
    @Excel(name = "创建人")
    private String creator;

    /** 修改人 */
    @Excel(name = "修改人")
    private String modifier;

    /** 修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date modifyTime;

    /** 修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createTime;

    /** 逻辑删除， 0存在，1删除 */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long delFlag;

    /** 排名 */
    @Excel(name = "排名")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long rank;

}
