package com.citybiz.system.domain.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.citybiz.system.domain.biz.BizTaskExtendInfo;
import com.citybiz.system.domain.biz.BizTaskType;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 任务表格对象 biz_task
 *
 * @author citybiz
 * @date 2023-10-01
 */
@Data
public class BizTaskDto {
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 任务创建时间 */
    private Date createTime;

    /** 任务修改时间 */
    private Date updateTime;

    /** 任务开始时间 */
    private Date taskStartTime;

    /** 任务结束时间 */
    private Date taskEndTime;

    /** 任务名称 */
    private String taskName;

    /** 任务详情 */
    private String taskDetail;

    /** 招标任务工作流状态 */
    private String tenderTaskStatus;

    /** 招标任务开始时间 */
    private Date tenderTaskStartTime;

    /** 招标任务完成时间 */
    private Date tenderTaskFinishTime;

    /** 雇主id */
    private Long employId;

    /** 雇主姓名 */
    private String employName;

    /** 工作者id */
    private Long workerId;

    /** 工作者名称 */
    private String workName;

    /** 任务价格 */
    private BigDecimal taskPrice;

    /** 任务类型 */
    private Long taskTypeId;

    private String taskTypeStr;

    /** 任务状态 */
    private String taskStatus;

    /** 删除标记 0存在 1删除 */
    private Long delFlag;

    /** 投稿id  */
    private Long manuscriptId;

    // 任务分类 tender：招标任务 bounty：悬赏任务
    private String taskSort;

    /** 任务投稿列表 */
    private List<BizTaskManuscriptDto> bizTaskManuscriptDtoList;

    private BizTaskType bizTaskType;

    private BizTaskExtendInfoDTO bizTaskExtendInfoDTO;

    private String taskPhone;

    private BizTaskConsignDTO bizTaskConsignDTO;

}
