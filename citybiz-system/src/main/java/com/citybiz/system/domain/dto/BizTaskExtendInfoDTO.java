package com.citybiz.system.domain.dto;

import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.citybiz.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Create by Ykg on 2024/2/10 18:11
 * effect: 商店扩展信息DTO
 */
@Data
public class BizTaskExtendInfoDTO {

    /** 主键 */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /** 行业id */
    private Long industryId;

    /** 雇主联系方式 */
    private String employerPhone;

    /** 地址id */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long addressId;

    /** 是否选用交付工具 0未选用，1选用 */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long deliveryToolFlag;

    /** 任务附件地址，限制三个 */
    private String attachmentUrls;

    /** 预算价格 */
    private BigDecimal budget;

    /** 增值服务 */
    private String extraServices;

    /** 任务id */
    private Long taskId;

    // 任务扩展特征信息
    private String feature;

    // 投稿数量
    private Integer manuscriptCount;

    private Map<String,String> featureContext;

    public String getContext(String contextKey){
        if(featureContext == null){
            return null;
        }
        return featureContext.get(contextKey);
    }

    public void setContext(String contextKey, String value) {
        if(featureContext == null){
            featureContext = new HashMap<>();
        }

        featureContext.put(contextKey, value);
        feature = JSON.toJSONString(featureContext);
    }

}
