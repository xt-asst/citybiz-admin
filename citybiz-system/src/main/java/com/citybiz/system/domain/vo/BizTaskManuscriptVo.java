package com.citybiz.system.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.citybiz.common.annotation.Excel;
import com.citybiz.common.core.domain.BasePlusEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 任务投标信息对象 biz_task_manuscript
 *
 * @author citybiz
 * @date 2023-10-18
 */
@Data
public class BizTaskManuscriptVo
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @Excel(name = "主键")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /** 稿件说明 */
    @Excel(name = "稿件说明")
    private String manuscriptInfo;

    /** 稿件地址 */
    @Excel(name = "稿件地址")
    private String manuscriptUrls;

    /** 任务id */
    @Excel(name = "任务id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long taskId;

    /** 工作者id */
    @Excel(name = "工作者id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long workerId;

    /** 工作者姓名 */
    private String workerName;

    /** 工作者头像地址 */
    private String avatar;

    /** 报价金额 */
    @Excel(name = "报价金额")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal quote;

}
