package com.citybiz.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Create by Ykg on 2024/2/13 17:14
 * effect: 任务扩展信息VO
 */
@Data
public class BizTaskExtendInfoVO {

    /** 主键 */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /** 行业id */
    private Long industryId;

    /** 雇主联系方式 */
    private String employerPhone;

    /** 地址id */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long addressId;

    /** 是否选用交付工具 0未选用，1选用 */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long deliveryToolFlag;

    /** 任务附件地址，限制三个 */
    private String attachmentUrls;

    /** 预算价格 */
    private BigDecimal budget;

    /** 增值服务 */
    private String extraServices;

    /** 任务id */
    private Long taskId;

    // 任务扩展特征信息
    private Map<String,String> featureContext;

}
