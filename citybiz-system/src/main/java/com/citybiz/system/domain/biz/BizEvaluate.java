package com.citybiz.system.domain.biz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.citybiz.common.annotation.Excel;
import com.citybiz.common.core.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * 评级列对象 biz_evaluate_list
 * 
 * @author citybiz
 * @date 2023-10-16
 */
@Data
public class BizEvaluate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 逐渐 */
    @TableId(type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /** 评价类型: 作品评价，任务评价，服务评价，店铺评价 */
    @Excel(name = "评价类型: 作品评价，任务评价，服务评价，店铺评价")
    private String evaluateType;

    /**
     * 指出所属的作品/人物/服务/店铺
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long bizId;

    /** 评价人id */
    @Excel(name = "评价人id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long shopEvaluaterId;

    /** 评价分数(0 - 5) */
    @Excel(name = "评价分数(0 - 5)")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal evaluateFraction;

    /** 评价等级: 好评good_evaluate,中评neutral_evaluate,差评 bad_evaluate */
    @Excel(name = "评价等级: 好评good_evaluate,中评neutral_evaluate,差评 bad_evaluate")
    private String evaluateLevel;

    /** 删除标记 0:未删除 1:删除 */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long delFlag;
}
