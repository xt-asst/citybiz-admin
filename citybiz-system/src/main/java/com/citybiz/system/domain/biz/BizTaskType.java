package com.citybiz.system.domain.biz;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.citybiz.common.annotation.Excel;
import com.citybiz.common.core.domain.TreeEntity;

/**
 * 任务类型管理对象 biz_task_type
 * 
 * @author citybiz
 * @date 2023-10-01
 */

public class BizTaskType extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /** 类型名称 */
    @Excel(name = "类型名称")
    private String name;

    /** 修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date modifyTime;

    /** 创建人 */
    @Excel(name = "创建人")
    private String creator;

    /** 修改人 */
    @Excel(name = "修改人")
    private String modifier;

    /** 删除标记 0存在 1删除 */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long delFlag;

    /** 图片信息 */
    @Excel(name = "图片信息")
    private String iconMsg;

    @TableField(exist = false)
    private List<BizTaskType> childTypes;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setModifyTime(Date modifyTime) 
    {
        this.modifyTime = modifyTime;
    }

    public Date getModifyTime() 
    {
        return modifyTime;
    }
    public void setCreator(String creator) 
    {
        this.creator = creator;
    }

    public String getCreator() 
    {
        return creator;
    }
    public void setModifier(String modifier) 
    {
        this.modifier = modifier;
    }

    public String getModifier() 
    {
        return modifier;
    }
    public void setDelFlag(Long delFlag) 
    {
        this.delFlag = delFlag;
    }

    public Long getDelFlag() 
    {
        return delFlag;
    }
    public void setIconMsg(String iconMsg) 
    {
        this.iconMsg = iconMsg;
    }

    public String getIconMsg() 
    {
        return iconMsg;
    }

    public List<BizTaskType> getChildTypes() {
        return childTypes;
    }

    public void setChildTypes(List<BizTaskType> childTypes) {
        this.childTypes = childTypes;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("parentId", getParentId())
            .append("createTime", getCreateTime())
            .append("modifyTime", getModifyTime())
            .append("creator", getCreator())
            .append("modifier", getModifier())
            .append("delFlag", getDelFlag())
            .append("iconMsg", getIconMsg())
            .toString();
    }
}
