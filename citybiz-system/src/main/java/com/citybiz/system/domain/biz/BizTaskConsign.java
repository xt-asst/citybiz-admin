package com.citybiz.system.domain.biz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

/**
 * Create by Ykg on 2024/2/12 01:26
 * effect: 任务交付信息
 */
@Data
public class BizTaskConsign {

    // 主键id
    @TableId(type = IdType.AUTO)
    private Long id;

    // 所属任务id
    private Long taskId;

    // 所属交付阶段id
    private Long taskPayPlanId;

    // 交付人id
    private Long consignId;

    // 备注
    private String remark;

    // 文件地址，使用逗号隔开
    private String fileUrls;

    // 交付时间
    private Date createTime;

}
