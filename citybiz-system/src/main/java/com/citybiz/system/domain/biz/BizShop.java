package com.citybiz.system.domain.biz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.citybiz.common.core.domain.BasePlusEntity;
import com.citybiz.system.domain.WxUser;
import com.citybiz.system.mapper.biz.BizShopExtendPlusMapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.citybiz.common.annotation.Excel;
import com.citybiz.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 店铺格对象 biz_shop
 *
 * @author citybiz
 * @date 2023-10-02
 */
@Data
@TableName("biz_shop")
public class BizShop extends BasePlusEntity
{
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    /** 主键 */
    private Long id;

    /** 店铺名称 */
    @Excel(name = "店铺名称")
    private String name;

    /** 头像地址 */
    @Excel(name = "头像地址")
    private String avatar;

    /** 店铺详情 */
    @Excel(name = "店铺详情")
    private String detail;

    /** 地址id */
    @Excel(name = "地址id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long addressId;

//    @TableField(exist = false)
    private String addressStr;

    @TableField(exist = false)
    private BizAddressTable addressTable;

    /** 工作者id */
    @Excel(name = "工作者id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long workerId;

    /** 综合评分 */
    @Excel(name = "综合评分")
    private BigDecimal overallRating;

    /** 服务次数 */
    @Excel(name = "服务次数")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long serviceTimes;

    /** 删除标记 0:存在  1:删除 */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long delFlag;

    // 创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 审核标记
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long passFlag;

    // 扩展信息
    @TableField(exist = false)
    private BizShopExtendInfo extend;

    // 工作者姓名
    @TableField(exist = false)
    private String workerWxUserName;

    // 行业类型id
    private Long bizTaskId;

    // 行业类型名称
    @TableField(exist = false)
    private String bizTaskStr;

    @TableField(exist = false)
    private List<BizShopCase> bizShopCases;

    // 好评排序  1:倒序，2:正序 0/null:不排序
    @TableField(exist = false)
    private Integer goodFreqDesc;
}
