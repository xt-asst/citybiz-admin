package com.citybiz.system.domain.biz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Create by Ykg on 2023/10/18 23:11
 * effect: 微信用户订单
 */
@Data
@TableName("wx_user_order")
public class WxUserOrder {

    /**
     * 订单id
     */
    @TableId(type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /**
     * 订单创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 微信用户id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long wxUserId;

    /**
     * 服务类型
     */
    private String serverType;

    /**
     * 店铺id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long shopId;

    /**
     * 店铺服务id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long shopBizServerId;

    /**
     * 订单状态
     */
    private String orderStatus;

    /**
     * 订单总价
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal price;

    /**
     * 订单单价，类型为作品时才有
     */
    private BigDecimal unitPrice;

    /**
     * 订单购买数量
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long num;

    /**
     * 删除标记 0:存在  1:删除
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long delFlag;


}
