package com.citybiz.system.domain.biz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.citybiz.common.annotation.Excel;
import com.citybiz.common.core.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * 店铺案例对象 biz_shop_case
 *
 * @author citybiz
 * @date 2023-10-18
 */
@Data
public class BizShopCase extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    // 商店id
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long shopId;

    /** 案例名称 */
    @Excel(name = "案例名称")
    private String caseName;

    /** 案例描述 */
    @Excel(name = "案例描述")
    private String caseDetail;

    /** 案例图片 */
    @Excel(name = "案例图片")
    private String caseImg;

    /** 案例内容链接 */
    @Excel(name = "案例内容链接")
    private String caseContentUrls;

    /** 案例价格 */
    @Excel(name = "案例价格")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal price;

    /** 删除标记 0:未删除 1:删除 */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long delFlag;
}
