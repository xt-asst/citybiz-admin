package com.citybiz.system.domain.biz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Create by Ykg on 2023/10/25 09:35
 * effect: 店铺扩展信息表
 */
@TableName("biz_shop_extend_info")
@Data
public class BizShopExtendInfo {

    // 主键id
    @TableId(type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    // 商店id
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long shopId;

    // 综合评分
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal overallRating;

    // 服务次数
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long serviceFreq;

    // 评分次数
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long ratingNum;

    // 好评率
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal goodFreqProp;

}
