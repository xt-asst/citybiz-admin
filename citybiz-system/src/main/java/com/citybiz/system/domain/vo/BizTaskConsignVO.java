package com.citybiz.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * Create by Ykg on 2024/2/13 23:40
 * effect:
 */
@Data
public class BizTaskConsignVO {

    // 主键id
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    // 所属任务id
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long taskId;

    // 所属交付阶段id
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long taskPayPlanId;

    // 交付人id
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long consignId;

    // 交付人姓名
    private String consignName;

    // 交付人头像
    private String consignAvatar;

    // 备注
    private String remark;

    // 文件地址，使用逗号隔开
    private String fileUrls;

    // 交付时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
