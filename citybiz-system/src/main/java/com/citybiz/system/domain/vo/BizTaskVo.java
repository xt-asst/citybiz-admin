package com.citybiz.system.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.citybiz.common.annotation.Excel;
import com.citybiz.common.core.domain.BasePlusEntity;
import com.citybiz.system.domain.biz.BizTaskConsign;
import com.citybiz.system.domain.biz.BizTaskManuscript;
import com.citybiz.system.domain.biz.BizTaskType;
import com.citybiz.system.domain.dto.BizTaskExtendInfoDTO;
import com.citybiz.system.domain.dto.BizTaskManuscriptDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 任务表格对象 biz_task
 *
 * @author citybiz
 * @date 2023-10-01
 */
@TableName("biz_task")
@Data
public class BizTaskVo extends BasePlusEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @Excel(name = "主键")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /** 任务创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "任务创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createTime;

    /** 任务修改时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "任务修改时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updateTime;

    /** 任务开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "任务开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date taskStartTime;

    /** 任务结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "任务结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date taskEndTime;

    /** 任务名称 */
    @Excel(name = "任务名称")
    private String taskName;

    /** 任务详情 */
    private String taskDetail;

    /** 招标任务工作流状态 */
    @Excel(name = "招标任务工作流状态")
    private String tenderTaskStatus;

    /** 招标任务开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "招标任务开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date tenderTaskStartTime;

    /** 招标任务完成时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "招标任务完成时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date tenderTaskFinishTime;

    /** 雇主id */
    @Excel(name = "雇主id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long employId;

    /** 雇主姓名 */
    @Excel(name = "雇主姓名")
    private String employName;

    /** 工作者id */
    @Excel(name = "工作者id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long workerId;

    /** 工作者名称 */
    @Excel(name = "工作者名称")
    private String workName;

    /** 任务价格 */
    @Excel(name = "任务价格")
    private BigDecimal taskPrice;

    /** 任务类型 */
    @Excel(name = "任务类型")
    private Long taskTypeId;

    private String taskTypeStr;

    /** 任务状态 */
    @Excel(name = "任务状态")
    private String taskStatus;

    private String taskStatusName;

    /** 删除标记 0存在 1删除 */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long delFlag;

    /** 投稿id  */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long manuscriptId;

    // 任务分类 tender：招标任务 bounty：悬赏任务
    private String taskSort;

    // 招标任务列表
    private List<BizTaskManuscriptVo> bizTaskManuscriptVoList;

    // 所属行业信息
    private BizTaskType bizTaskType;

    // 扩展信息
    private BizTaskExtendInfoVO bizTaskExtendInfoVO;

    // 联系方式
    private String taskPhone;

    // 交付信息
    private BizTaskConsignVO bizTaskConsignVO;

    // 剩余时间字符串
    private String timeLeftStr;
}
