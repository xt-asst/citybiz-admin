package com.citybiz.system.domain.biz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.citybiz.common.core.domain.BasePlusEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.citybiz.common.annotation.Excel;
import com.citybiz.common.core.domain.BaseEntity;

/**
 * 微信用户私人认证信息对象 biz_wx_private_approve_info
 * 
 * @author citybiz
 * @date 2023-10-04
 */
@Data
@TableName("biz_wx_private_approve_info")
public class BizWxPrivateApproveInfo extends BasePlusEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId(type =  IdType.AUTO)
    private Long id;

    /** 微信用户id */
    @Excel(name = "微信用户id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long wxUserId;

    /** 真实姓名 */
    @Excel(name = "真实姓名")
    private String realName;

    /** 身份证 */
    @Excel(name = "身份证")
    private String identityCard;

    /** 身份证人面照 */
    @Excel(name = "身份证人面照")
    private String identityCardFrontImg;

    /** 身份证背面照片 */
    @Excel(name = "身份证背面照片")
    private String identityCardBackImg;

    /** 是否删除 0: 存在 1: 删除 */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long delFlag;

    /** 是否通过 0:未通过 1:通过 */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @Excel(name = "是否通过 0:未通过 1:通过")
    private Long passFlag;
}
