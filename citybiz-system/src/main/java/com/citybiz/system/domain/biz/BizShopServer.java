package com.citybiz.system.domain.biz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.citybiz.common.core.domain.BasePlusEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.citybiz.common.annotation.Excel;
import com.citybiz.common.core.domain.BaseEntity;

import java.math.BigDecimal;

/**
 * 店铺服务列对象 biz_shop_server
 *
 * @author citybiz
 * @date 2023-10-18
 */
@Data
public class BizShopServer extends BasePlusEntity
{
    private static final long serialVersionUID = 1L;

    /** 逐渐 */
    @TableId(type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /** 服务名称 */
    @Excel(name = "服务名称")
    private String serverName;

    // 备注
    private String remark;

    /** 服务类型: 服务: server，作品:work */
    @Excel(name = "服务类型: 服务: server，作品:work")
    private String serverType;

    /** 服务链接 */
    @Excel(name = "服务链接")
    private String serverUrls;

    /** 店铺id */
    @Excel(name = "店铺id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long bizShopId;

    /** 价格 */
    @Excel(name = "价格")
    private BigDecimal price;

    /** 销量 */
    @Excel(name = "销量")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long salesVolume;

    /** 综合评分 */
    @Excel(name = "综合评分")
    private BigDecimal overallRating;

    /** 服务次数 */
    @Excel(name = "服务次数")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long serviceFreq;

    /** 服务封面 */
    @Excel(name = "服务封面")
    private String shopImgUrl;

    /** 评分次数 */
    @Excel(name = "评分次数")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long ratingNum;

    /** 好评率 */
    @Excel(name = "好评率")
    private BigDecimal goodFreqProp;

    /** 删除标记 0:未删除 1:删除 */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long delFlag;

    // 所属行业id
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long bizTaskId;

    @TableField(exist = false)
    private String bizTaskStr;
}
