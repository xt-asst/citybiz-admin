package com.citybiz.system.domain.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.citybiz.common.annotation.Excel;
import com.citybiz.common.core.domain.BasePlusEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 交易维权信息对象 biz_dispute_task
 * 
 * @author citybiz
 * @date 2023-10-15
 */
@Data
public class BizDisputeTaskDto extends BasePlusEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 任务id */
    private Long taskId;

    /** 维权订单类型 :悬赏任务:bounty,招标任务:tender */
    private String disputeTaskType;

    /** 创建时间 */
    private Date createTime;

    /** 雇主所得比例(二者比例相加等于1) */
    private BigDecimal employProp;

    /** 威客所得比例 */
    private BigDecimal workerProp;

    /** 处理人id */
    private Long processorId;

    /** 处理人手机号 */
    private String processorPhone;

    /** 处理人姓名 */
    private String processorName;

    /** 是否打款完毕 0:未打款 1:已打款 */
    @Excel(name = "是否打款完毕 0:未打款 1:已打款")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long makePaymentFlag;

    /** 维权结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "维权结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;
}
