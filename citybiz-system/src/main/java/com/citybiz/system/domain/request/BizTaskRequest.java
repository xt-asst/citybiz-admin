package com.citybiz.system.domain.request;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Create by Ykg on 2023/12/21 15:13
 * effect:
 */
@Data
public class BizTaskRequest {

    private Long taskId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date taskStartTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date taskEndTime;

    private String taskName;

    private String taskDetail;

    private String taskSort;

    private Long employId;

    private Long workerId;

    private String workName;

    private String taskStatus;

    private BigDecimal taskPrice;

    private Long taskTypeId;

    // 招标任务状态
    private String tenderTaskStatus;

    // 招标任务开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date tenderTaskStartTime;

    // 招标任务结束时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date tenderTaskFinishTime;

    // 地址信息id
    private Long addressId;

    // 任务联系方式
    private String taskPhone;

    // 创建时间排序
    private Integer createTimeDesc;

    // 投稿数排序
    private Integer manuscriptCountDesc;

}
