package com.citybiz.system.domain.biz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.citybiz.common.core.domain.BasePlusEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.citybiz.common.annotation.Excel;
import com.citybiz.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 支付计划对象 biz_pay_plan
 *
 * @author citybiz
 * @date 2023-10-18
 */
@Data
public class BizPayPlan extends BasePlusEntity
{
    private static final long serialVersionUID = 1L;

    /** 逐渐 */
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 任务id */
    private Long taskId;

    // 付款金额
    private BigDecimal payAmount;

    // 付款比例
    private Integer payProp;

    // 阶段开始时间
    private Date stageStartTime;

    // 阶段结束时间
    private Date stageEndTime;

    // 阶段备注
    private String remark;

    // 方案阶段
    private String planStage;

    // 阶段状态
    private String stageStatus;

}
