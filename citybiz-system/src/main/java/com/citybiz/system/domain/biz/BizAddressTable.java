package com.citybiz.system.domain.biz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * Create by Ykg on 2023/10/19 14:45
 * effect: 地址表格
 */
@Data
public class BizAddressTable {

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId(type = IdType.NONE)
    private Long id;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long parentId;

    private String type;

    private String province;

    private String city;

    private String name;

}
