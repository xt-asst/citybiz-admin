package com.citybiz.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.citybiz.common.core.domain.BasePlusEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.citybiz.common.annotation.Excel;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 微信用户管理对象 wx_user
 *
 * @author citybiz
 * @date 2023-10-01
 */
@TableName("wx_user")
@Data
public class WxUser extends BasePlusEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 小程序openid */
    private String openId;

    /** 用户手机号 */
    @Excel(name = "用户手机号")
    private String phone;

    /** 手机号验证码 */
    @TableField(exist = false)
    private String phoneCode;

    /** 登录密码 */
    private String loginPassword;

    /** 支付密码 */
    private String payPassword;

    /** 用户名称 */
    @Excel(name = "用户名称")
    private String username;

    /** 用户邮箱 */
    @Excel(name = "用户邮箱")
    private String email;

    /** 性别 */
    @Excel(name = "性别")
    private String sex;

    /** 备注 */
    @Excel(name = "备注")
    private String remark;

    /** 状态 */
    @Excel(name = "状态")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long status;

    /** 删除标记 */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long delFlag;

    /** 所在地区 */
    @Excel(name = "所在地区id")
    private Long address;

    @TableField(exist = false)
    private String addressStr;

    /** 头像地址 */
    private String avatar;

    /** 私人认证 0未认证，1认证通过 */
    @Excel(name = "私人认证 0未认证，1认证通过")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long privateAuth;

    /** 企业认证 0未认证 1认证通过 */
    @Excel(name = "企业认证 0未认证 1认证通过")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long companyAuth;

    /** 用户昵称 */
    @Excel(name = "用户昵称")
    private String nickname;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /** 用户余额 */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal balance;

    /** 是否是初始化状态，0:初始化，1:已完善*/
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Integer initUser;

    @TableField(exist = false)
    private WxUserRole wxUserRole;

    /**
     * 店铺状态  -1: 不存在， 0: 审核中 1: 审核成功 2: 审核失败
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long shopFlag;

}
