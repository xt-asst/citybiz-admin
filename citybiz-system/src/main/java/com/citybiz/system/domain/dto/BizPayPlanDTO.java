package com.citybiz.system.domain.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Create by Ykg on 2024/2/11 17:11
 * effect: 付款计划DTO
 */
@Data
public class BizPayPlanDTO {

    private static final long serialVersionUID = 1L;

    /** 逐渐 */
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 任务id */
    private Long taskId;

    // 付款金额
    private BigDecimal payAmount;

    // 付款比例
    private Integer payProp;

    // 阶段开始时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date stageStartTime;

    // 阶段结束时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date stageEndTime;

    // 阶段备注
    private String remark;

    // 方案阶段
    private String planStage;

    // 阶段状态
    private String stageStatus;

}
