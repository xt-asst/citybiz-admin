package com.citybiz.system.domain.biz;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.citybiz.common.annotation.Excel;
import com.citybiz.common.core.domain.BaseEntity;

/**
 * 用户钱包明细对象 wx_wallet_logs
 * 
 * @author citybiz
 * @date 2023-10-18
 */
public class WxWalletLogs extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /** 微信用户id */
    @Excel(name = "微信用户id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long wxUserId;

    /** 收支类型: 充值top_up, 支出:expenditure, 提现:expenditure */
    @Excel(name = "收支类型: 充值top_up, 支出:expenditure, 提现:expenditure")
    private String walletType;

    /** 日志时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "日志时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date logTime;

    /** 拓展信息 */
    private String extendInfo;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setWxUserId(Long wxUserId) 
    {
        this.wxUserId = wxUserId;
    }

    public Long getWxUserId() 
    {
        return wxUserId;
    }
    public void setWalletType(String walletType) 
    {
        this.walletType = walletType;
    }

    public String getWalletType() 
    {
        return walletType;
    }
    public void setLogTime(Date logTime) 
    {
        this.logTime = logTime;
    }

    public Date getLogTime() 
    {
        return logTime;
    }
    public void setExtendInfo(String extendInfo) 
    {
        this.extendInfo = extendInfo;
    }

    public String getExtendInfo() 
    {
        return extendInfo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("wxUserId", getWxUserId())
            .append("walletType", getWalletType())
            .append("logTime", getLogTime())
            .append("extendInfo", getExtendInfo())
            .toString();
    }
}
