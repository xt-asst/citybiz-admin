package com.citybiz.system.domain.biz;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.citybiz.common.core.domain.BasePlusEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.citybiz.common.annotation.Excel;
import com.citybiz.common.core.domain.BaseEntity;

/**
 * 公司认证审核对象 biz_wx_company_approve_info
 * 
 * @author citybiz
 * @date 2023-10-04
 */
@Data
@TableName("biz_wx_company_approve_info")
public class BizWxCompanyApproveInfo extends BasePlusEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据主键 */
    @TableId(type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /** 公司名称 */
    @Excel(name = "公司名称")
    private String companyName;

    /** 所属行业id */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long bizTypeId;

    /**
     * 微信用户id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableField("wx_user_id")
    private Long wxUserId;

    /** 员工人数 */
    @Excel(name = "员工人数")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long employeesNum;

    /** 统一社会信用码 */
    @Excel(name = "统一社会信用码")
    private String unifiedSocialCreditCode;

    /** 公司成立时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "公司成立时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date companyCreateTime;

    /** 企业网址 */
    @Excel(name = "企业网址")
    private String companyWebsite;

    /** 公司地址id */
    @Excel(name = "公司地址id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long addressId;

    @TableField(exist = false)
    private String addressStr;

    /** 公司详细地址 */
    @Excel(name = "公司详细地址")
    private String addressDetail;

    /** 通过标记 0:未通过 1:通过 */
    @Excel(name = "通过标记 0:未通过 1:通过")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long passFlag;

    /** 删除标记 0:未删除 1:删除 */
    private Long delFlag;

    /** 资质证明图片 */
    @Excel(name = "资质证明图片")
    private String proveImages;
}
