package com.citybiz.system.domain.biz;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.citybiz.common.annotation.Excel;
import com.citybiz.common.core.domain.BaseEntity;

/**
 * 消息业务格对象 biz_message
 * 
 * @author citybiz
 * @date 2023-11-04
 */
public class BizMessage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    @Excel(name = "主键id")
    private Long id;

    /** 消息内容 */
    @Excel(name = "消息内容")
    private String message;

    /** 微信用户id */
    @Excel(name = "微信用户id")
    private Long wxUserId;

    /** 未读标记 0:未读 1:已读 */
    @Excel(name = "未读标记 0:未读 1:已读")
    private Long readFlag;

    /** 消息类型 */
    @Excel(name = "消息类型")
    private String msgType;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setMessage(String message) 
    {
        this.message = message;
    }

    public String getMessage() 
    {
        return message;
    }
    public void setWxUserId(Long wxUserId) 
    {
        this.wxUserId = wxUserId;
    }

    public Long getWxUserId() 
    {
        return wxUserId;
    }
    public void setReadFlag(Long readFlag) 
    {
        this.readFlag = readFlag;
    }

    public Long getReadFlag() 
    {
        return readFlag;
    }
    public void setMsgType(String msgType) 
    {
        this.msgType = msgType;
    }

    public String getMsgType() 
    {
        return msgType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("message", getMessage())
            .append("wxUserId", getWxUserId())
            .append("readFlag", getReadFlag())
            .append("createTime", getCreateTime())
            .append("msgType", getMsgType())
            .toString();
    }
}
