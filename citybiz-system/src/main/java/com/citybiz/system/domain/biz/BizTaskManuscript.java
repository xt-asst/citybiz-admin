package com.citybiz.system.domain.biz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.citybiz.common.core.domain.BasePlusEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.citybiz.common.annotation.Excel;
import com.citybiz.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 任务投标信息对象 biz_task_manuscript
 *
 * @author citybiz
 * @date 2023-10-18
 */
public class BizTaskManuscript extends BasePlusEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @Excel(name = "主键")
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 稿件说明 */
    @Excel(name = "稿件说明")
    private String manuscriptInfo;

    /** 稿件地址 */
    @Excel(name = "稿件地址")
    private String manuscriptUrls;

    /** 任务id */
    @Excel(name = "任务id")
    private Long taskId;

    /** 创建时间 */
    private Date createTime;

    /** 工作者id */
    @Excel(name = "工作者id")
    private Long workerId;

    /** 报价金额 */
    @Excel(name = "报价金额")
    private BigDecimal quote;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setManuscriptInfo(String manuscriptInfo)
    {
        this.manuscriptInfo = manuscriptInfo;
    }

    public String getManuscriptInfo()
    {
        return manuscriptInfo;
    }
    public void setManuscriptUrls(String manuscriptUrls)
    {
        this.manuscriptUrls = manuscriptUrls;
    }

    public String getManuscriptUrls()
    {
        return manuscriptUrls;
    }
    public void setTaskId(Long taskId)
    {
        this.taskId = taskId;
    }

    public Long getTaskId()
    {
        return taskId;
    }
    public void setWorkerId(Long workerId)
    {
        this.workerId = workerId;
    }

    public Long getWorkerId()
    {
        return workerId;
    }
    public void setQuote(BigDecimal quote)
    {
        this.quote = quote;
    }

    public BigDecimal getQuote()
    {
        return quote;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("manuscriptInfo", getManuscriptInfo())
            .append("manuscriptUrls", getManuscriptUrls())
            .append("taskId", getTaskId())
            .append("workerId", getWorkerId())
            .append("quote", getQuote())
            .toString();
    }
}
