package com.citybiz.system.domain.biz;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.citybiz.common.annotation.Excel;
import com.citybiz.common.core.domain.BasePlusEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 任务扩展信息对象 biz_task_extend_info
 *
 * @author citybiz
 * @date 2023-10-01
 */
@TableName("biz_task_extend_info")
@Data
public class BizTaskExtendInfo extends BasePlusEntity
{
    /** 主键 */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 行业id */
    @Excel(name = "行业id")
    private Long industryId;

    /** 雇主联系方式 */
    @Excel(name = "雇主联系方式")
    private String employerPhone;

    /** 地址id */
    @Excel(name = "地址id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long addressId;

    /** 是否选用交付工具 0未选用，1选用 */
    @Excel(name = "是否选用交付工具 0未选用，1选用")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long deliveryToolFlag;

    /** 任务附件地址，限制三个 */
    @Excel(name = "任务附件地址，限制三个")
    private String attachmentUrls;

    /** 预算价格 */
    @Excel(name = "预算价格")
    private BigDecimal budget;

    /** 增值服务 */
    @Excel(name = "增值服务")
    private String extraServices;

    /** 任务id */
    @Excel(name = "任务id")
    private Long taskId;

    private String feature;

}

