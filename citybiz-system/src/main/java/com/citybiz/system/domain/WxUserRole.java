package com.citybiz.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * Create by Ykg on 2023/10/3 09:46
 * effect: 微信角色信息，
 */
@Data
@TableName("wx_user_role")
public class WxUserRole {

    @TableId(type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long wxUserId;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Integer role;

    private String roleName;

}
