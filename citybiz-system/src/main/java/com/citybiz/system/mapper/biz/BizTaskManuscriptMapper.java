package com.citybiz.system.mapper.biz;

import com.citybiz.system.domain.biz.BizTaskManuscript;

import java.util.List;

/**
 * 任务投标信息Mapper接口
 * 
 * @author citybiz
 * @date 2023-10-18
 */
public interface BizTaskManuscriptMapper 
{
    /**
     * 查询任务投标信息
     * 
     * @param id 任务投标信息主键
     * @return 任务投标信息
     */
    public BizTaskManuscript selectBizTaskManuscriptById(Long id);

    /**
     * 查询任务投标信息列表
     * 
     * @param bizTaskManuscript 任务投标信息
     * @return 任务投标信息集合
     */
    public List<BizTaskManuscript> selectBizTaskManuscriptList(BizTaskManuscript bizTaskManuscript);

    /**
     * 新增任务投标信息
     * 
     * @param bizTaskManuscript 任务投标信息
     * @return 结果
     */
    public int insertBizTaskManuscript(BizTaskManuscript bizTaskManuscript);

    /**
     * 修改任务投标信息
     * 
     * @param bizTaskManuscript 任务投标信息
     * @return 结果
     */
    public int updateBizTaskManuscript(BizTaskManuscript bizTaskManuscript);

    /**
     * 删除任务投标信息
     * 
     * @param id 任务投标信息主键
     * @return 结果
     */
    public int deleteBizTaskManuscriptById(Long id);

    /**
     * 批量删除任务投标信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBizTaskManuscriptByIds(Long[] ids);
}
