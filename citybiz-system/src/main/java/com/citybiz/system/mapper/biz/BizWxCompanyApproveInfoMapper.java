package com.citybiz.system.mapper.biz;

import com.citybiz.system.domain.biz.BizWxCompanyApproveInfo;

import java.util.List;

/**
 * 公司认证审核Mapper接口
 * 
 * @author citybiz
 * @date 2023-10-04
 */
public interface BizWxCompanyApproveInfoMapper 
{
    /**
     * 查询公司认证审核
     * 
     * @param id 公司认证审核主键
     * @return 公司认证审核
     */
    public BizWxCompanyApproveInfo selectBizWxCompanyApproveInfoById(Long id);

    /**
     * 查询公司认证审核列表
     * 
     * @param bizWxCompanyApproveInfo 公司认证审核
     * @return 公司认证审核集合
     */
    public List<BizWxCompanyApproveInfo> selectBizWxCompanyApproveInfoList(BizWxCompanyApproveInfo bizWxCompanyApproveInfo);

    /**
     * 新增公司认证审核
     * 
     * @param bizWxCompanyApproveInfo 公司认证审核
     * @return 结果
     */
    public int insertBizWxCompanyApproveInfo(BizWxCompanyApproveInfo bizWxCompanyApproveInfo);

    /**
     * 修改公司认证审核
     * 
     * @param bizWxCompanyApproveInfo 公司认证审核
     * @return 结果
     */
    public int updateBizWxCompanyApproveInfo(BizWxCompanyApproveInfo bizWxCompanyApproveInfo);

    /**
     * 删除公司认证审核
     * 
     * @param id 公司认证审核主键
     * @return 结果
     */
    public int deleteBizWxCompanyApproveInfoById(Long id);

    /**
     * 批量删除公司认证审核
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBizWxCompanyApproveInfoByIds(Long[] ids);
}
