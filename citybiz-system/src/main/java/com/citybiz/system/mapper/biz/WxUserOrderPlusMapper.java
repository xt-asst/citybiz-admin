package com.citybiz.system.mapper.biz;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.biz.WxUserOrder;
import org.apache.ibatis.annotations.Mapper;

/**
 * Create by Ykg on 2023/10/18 23:17
 * effect: 微信用户订单plus接口
 */
@Mapper
public interface WxUserOrderPlusMapper extends BaseMapper<WxUserOrder> {

}
