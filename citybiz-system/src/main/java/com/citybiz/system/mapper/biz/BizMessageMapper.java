package com.citybiz.system.mapper.biz;

import com.citybiz.system.domain.biz.BizMessage;

import java.util.List;

/**
 * 消息业务格Mapper接口
 * 
 * @author citybiz
 * @date 2023-11-04
 */
public interface BizMessageMapper 
{
    /**
     * 查询消息业务格
     * 
     * @param id 消息业务格主键
     * @return 消息业务格
     */
    public BizMessage selectBizMessageById(Long id);

    /**
     * 查询消息业务格列表
     * 
     * @param bizMessage 消息业务格
     * @return 消息业务格集合
     */
    public List<BizMessage> selectBizMessageList(BizMessage bizMessage);

    /**
     * 新增消息业务格
     * 
     * @param bizMessage 消息业务格
     * @return 结果
     */
    public int insertBizMessage(BizMessage bizMessage);

    /**
     * 修改消息业务格
     * 
     * @param bizMessage 消息业务格
     * @return 结果
     */
    public int updateBizMessage(BizMessage bizMessage);

    /**
     * 删除消息业务格
     * 
     * @param id 消息业务格主键
     * @return 结果
     */
    public int deleteBizMessageById(Long id);

    /**
     * 批量删除消息业务格
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBizMessageByIds(Long[] ids);
}
