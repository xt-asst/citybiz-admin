package com.citybiz.system.mapper.biz;

import com.citybiz.system.domain.biz.BizShop;

import java.util.List;

/**
 * 店铺格Mapper接口
 * 
 * @author citybiz
 * @date 2023-10-02
 */
public interface BizShopMapper 
{
    /**
     * 查询店铺格
     * 
     * @param id 店铺格主键
     * @return 店铺格
     */
    public BizShop selectBizShopById(Long id);

    /**
     * 查询店铺格列表
     * 
     * @param bizShop 店铺格
     * @return 店铺格集合
     */
    public List<BizShop> selectBizShopList(BizShop bizShop);

    /**
     * 新增店铺格
     * 
     * @param bizShop 店铺格
     * @return 结果
     */
    public int insertBizShop(BizShop bizShop);

    /**
     * 修改店铺格
     * 
     * @param bizShop 店铺格
     * @return 结果
     */
    public int updateBizShop(BizShop bizShop);

    /**
     * 删除店铺格
     * 
     * @param id 店铺格主键
     * @return 结果
     */
    public int deleteBizShopById(Long id);

    /**
     * 批量删除店铺格
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBizShopByIds(Long[] ids);
}
