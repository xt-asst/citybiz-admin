package com.citybiz.system.mapper.biz;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.biz.WxWalletLogs;
import org.apache.ibatis.annotations.Mapper;

/**
 * Create by Ykg on 2023/10/18 00:48
 * effect: 钱包明细plus接口
 */
@Mapper
public interface WxWalletLogsPlusMapper extends BaseMapper<WxWalletLogs> {

}
