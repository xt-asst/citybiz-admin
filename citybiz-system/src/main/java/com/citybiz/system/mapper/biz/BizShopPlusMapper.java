package com.citybiz.system.mapper.biz;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.biz.BizShop;
import org.apache.ibatis.annotations.Mapper;

/**
 * Create by Ykg on 2023/10/2 22:32
 * effect:
 */
@Mapper
public interface BizShopPlusMapper extends BaseMapper<BizShop> {

}
