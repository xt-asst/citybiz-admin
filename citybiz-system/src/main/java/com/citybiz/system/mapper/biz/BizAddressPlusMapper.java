package com.citybiz.system.mapper.biz;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.biz.BizAddressTable;
import org.apache.ibatis.annotations.Mapper;

/**
 * Create by Ykg on 2023/10/19 14:47
 * effect: 地址信息plus接口
 */
@Mapper
public interface BizAddressPlusMapper extends BaseMapper<BizAddressTable> {

}
