package com.citybiz.system.mapper.biz;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.biz.BizDisputeTask;
import org.apache.ibatis.annotations.Mapper;

/**
 * Create by Ykg on 2023/10/15 23:39
 * effect: 维权表格plus接口
 */
@Mapper
public interface BizDisputeTasPlusMapper extends BaseMapper<BizDisputeTask> {
}
