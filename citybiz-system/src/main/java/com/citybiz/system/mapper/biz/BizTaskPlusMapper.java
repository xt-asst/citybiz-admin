package com.citybiz.system.mapper.biz;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.biz.BizTask;
import org.apache.ibatis.annotations.Mapper;

/**
 * Create by Ykg on 2023/10/1 21:06
 * effect: 业务表格mybatis plus接口
 */
@Mapper
public interface BizTaskPlusMapper extends BaseMapper<BizTask> {

}
