package com.citybiz.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.WxUserRole;

/**
 * Create by Ykg on 2023/10/3 09:55
 * effect:
 */
public interface WxUserRolePlusMapper extends BaseMapper<WxUserRole> {
}
