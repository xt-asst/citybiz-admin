package com.citybiz.system.mapper.biz;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.biz.BizEvaluate;
import org.apache.ibatis.annotations.Mapper;

/**
 * Create by Ykg on 2023/10/18 00:44
 * effect: 评价列表
 */
@Mapper
public interface BizEvaluateListPlusMapper extends BaseMapper<BizEvaluate> {
}
