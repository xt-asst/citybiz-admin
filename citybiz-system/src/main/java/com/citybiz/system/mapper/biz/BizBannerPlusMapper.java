package com.citybiz.system.mapper.biz;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.biz.BizBanner;
import org.apache.ibatis.annotations.Mapper;

/**
 * banner轮播图列Mybatisplus接口
 *
 * @author ykg
 * @date 2023-09-29
 */
@Mapper
public interface BizBannerPlusMapper extends BaseMapper<BizBanner> {



}
