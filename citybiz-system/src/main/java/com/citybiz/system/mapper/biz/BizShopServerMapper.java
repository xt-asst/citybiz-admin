package com.citybiz.system.mapper.biz;

import com.citybiz.system.domain.biz.BizShopServer;

import java.util.List;

/**
 * 店铺服务列Mapper接口
 * 
 * @author citybiz
 * @date 2023-10-18
 */
public interface BizShopServerMapper 
{
    /**
     * 查询店铺服务列
     * 
     * @param id 店铺服务列主键
     * @return 店铺服务列
     */
    public BizShopServer selectBizShopServerById(Long id);

    /**
     * 查询店铺服务列列表
     * 
     * @param bizShopServer 店铺服务列
     * @return 店铺服务列集合
     */
    public List<BizShopServer> selectBizShopServerList(BizShopServer bizShopServer);

    /**
     * 新增店铺服务列
     * 
     * @param bizShopServer 店铺服务列
     * @return 结果
     */
    public int insertBizShopServer(BizShopServer bizShopServer);

    /**
     * 修改店铺服务列
     * 
     * @param bizShopServer 店铺服务列
     * @return 结果
     */
    public int updateBizShopServer(BizShopServer bizShopServer);

    /**
     * 删除店铺服务列
     * 
     * @param id 店铺服务列主键
     * @return 结果
     */
    public int deleteBizShopServerById(Long id);

    /**
     * 批量删除店铺服务列
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBizShopServerByIds(Long[] ids);
}
