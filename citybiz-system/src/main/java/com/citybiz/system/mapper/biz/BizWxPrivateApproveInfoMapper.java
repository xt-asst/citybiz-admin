package com.citybiz.system.mapper.biz;

import com.citybiz.system.domain.biz.BizWxPrivateApproveInfo;

import java.util.List;

/**
 * 微信用户私人认证信息Mapper接口
 * 
 * @author citybiz
 * @date 2023-10-04
 */
public interface BizWxPrivateApproveInfoMapper 
{
    /**
     * 查询微信用户私人认证信息
     * 
     * @param id 微信用户私人认证信息主键
     * @return 微信用户私人认证信息
     */
    public BizWxPrivateApproveInfo selectBizWxPrivateApproveInfoById(Long id);

    /**
     * 查询微信用户私人认证信息列表
     * 
     * @param bizWxPrivateApproveInfo 微信用户私人认证信息
     * @return 微信用户私人认证信息集合
     */
    public List<BizWxPrivateApproveInfo> selectBizWxPrivateApproveInfoList(BizWxPrivateApproveInfo bizWxPrivateApproveInfo);

    /**
     * 新增微信用户私人认证信息
     * 
     * @param bizWxPrivateApproveInfo 微信用户私人认证信息
     * @return 结果
     */
    public int insertBizWxPrivateApproveInfo(BizWxPrivateApproveInfo bizWxPrivateApproveInfo);

    /**
     * 修改微信用户私人认证信息
     * 
     * @param bizWxPrivateApproveInfo 微信用户私人认证信息
     * @return 结果
     */
    public int updateBizWxPrivateApproveInfo(BizWxPrivateApproveInfo bizWxPrivateApproveInfo);

    /**
     * 删除微信用户私人认证信息
     * 
     * @param id 微信用户私人认证信息主键
     * @return 结果
     */
    public int deleteBizWxPrivateApproveInfoById(Long id);

    /**
     * 批量删除微信用户私人认证信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBizWxPrivateApproveInfoByIds(Long[] ids);
}
