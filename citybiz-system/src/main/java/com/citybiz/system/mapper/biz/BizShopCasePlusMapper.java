package com.citybiz.system.mapper.biz;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.biz.BizShopCase;
import org.apache.ibatis.annotations.Mapper;

/**
 * Create by Ykg on 2023/10/18 00:46
 * effect: 店铺案例plus接口
 */
@Mapper
public interface BizShopCasePlusMapper extends BaseMapper<BizShopCase> {
}
