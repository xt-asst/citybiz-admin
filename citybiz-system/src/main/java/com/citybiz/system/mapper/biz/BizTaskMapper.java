package com.citybiz.system.mapper.biz;

import com.citybiz.system.domain.biz.BizTask;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 任务格Mapper接口
 * 
 * @author citybiz
 * @date 2023-10-01
 */
public interface BizTaskMapper 
{
    /**
     * 查询任务格
     * 
     * @param id 任务格主键
     * @return 任务格
     */
    public BizTask selectBizTaskById(Long id);

    /**
     * 查询任务格列表
     * 
     * @param bizTask 任务格
     * @return 任务格集合
     */
    public List<BizTask> selectBizTaskList(BizTask bizTask);

    /**
     * 新增任务格
     * 
     * @param bizTask 任务格
     * @return 结果
     */
    public int insertBizTask(BizTask bizTask);

    /**
     * 修改任务格
     * 
     * @param bizTask 任务格
     * @return 结果
     */
    public int updateBizTask(BizTask bizTask);

    /**
     * 删除任务格
     * 
     * @param id 任务格主键
     * @return 结果
     */
    public int deleteBizTaskById(Long id);

    /**
     * 批量删除任务格
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBizTaskByIds(Long[] ids);

    public List<BizTask> selectBizTaskListByWorker(@Param("workerId") Long workerId);
}
