package com.citybiz.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.WxUser;

/**
 * Create by Ykg on 2023/10/1 21:24
 * effect: 微信plus接口
 */
public interface WxUserPlusMapper extends BaseMapper<WxUser> {
}
