package com.citybiz.system.mapper.biz;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.biz.BizWxCompanyApproveInfo;
import com.citybiz.system.domain.biz.BizWxPrivateApproveInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * Create by Ykg on 2023/10/4 23:28
 * effect: 微信私人认证plus接口
 */
@Mapper
public interface BizWxPrivateApproveInfoPlusMapper extends BaseMapper<BizWxPrivateApproveInfo> {

}
