package com.citybiz.system.mapper.biz;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.biz.BizTaskManuscript;
import org.apache.ibatis.annotations.Mapper;

/**
 * Create by Ykg on 2023/10/18 00:46
 * effect: 任务投标信息plus接口
 */
@Mapper
public interface BizTaskManuscriptPlusMapper extends BaseMapper<BizTaskManuscript> {
}
