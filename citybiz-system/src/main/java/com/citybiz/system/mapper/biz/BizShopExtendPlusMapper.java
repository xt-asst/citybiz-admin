package com.citybiz.system.mapper.biz;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.biz.BizShopExtendInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * Create by Ykg on 2023/10/18 00:49
 * effect: 店铺扩展信息plus接口
 */
@Mapper
public interface BizShopExtendPlusMapper extends BaseMapper<BizShopExtendInfo> {

}
