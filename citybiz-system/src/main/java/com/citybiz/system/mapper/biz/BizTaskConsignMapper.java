package com.citybiz.system.mapper.biz;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.biz.BizTaskConsign;
import org.apache.ibatis.annotations.Mapper;

/**
 * Create by Ykg on 2024/2/12 16:47
 * effect:
 */
@Mapper
public interface BizTaskConsignMapper extends BaseMapper<BizTaskConsign> {
}
