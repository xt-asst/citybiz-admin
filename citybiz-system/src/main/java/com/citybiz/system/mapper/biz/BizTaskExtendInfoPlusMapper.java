package com.citybiz.system.mapper.biz;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.biz.BizTaskExtendInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * Create by Ykg on 2023/10/1 21:24
 * effect: 任务扩展信息plus mapper接口
 */
@Mapper
public interface BizTaskExtendInfoPlusMapper extends BaseMapper<BizTaskExtendInfo> {

}
