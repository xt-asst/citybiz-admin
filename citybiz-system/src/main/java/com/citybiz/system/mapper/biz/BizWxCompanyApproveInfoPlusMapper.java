package com.citybiz.system.mapper.biz;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.biz.BizWxCompanyApproveInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * Create by Ykg on 2023/10/4 23:27
 * effect: 微信公司认证plus接口
 */
@Mapper
public interface BizWxCompanyApproveInfoPlusMapper extends BaseMapper<BizWxCompanyApproveInfo> {


}

