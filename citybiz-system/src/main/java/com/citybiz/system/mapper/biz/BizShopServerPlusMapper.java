package com.citybiz.system.mapper.biz;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.biz.BizShopServer;
import org.apache.ibatis.annotations.Mapper;

/**
 * Create by Ykg on 2023/10/18 00:48
 * effect: 店铺服务plus接口
 */
@Mapper
public interface BizShopServerPlusMapper extends BaseMapper<BizShopServer> {

}
