package com.citybiz.system.mapper.biz;

import java.util.List;
import com.citybiz.system.domain.biz.BizBanner;

/**
 * banner轮播图列Mapper接口
 * 
 * @author ykg
 * @date 2023-09-29
 */
public interface BizBannerMapper 
{
    /**
     * 查询banner轮播图列
     * 
     * @param id banner轮播图列主键
     * @return banner轮播图列
     */
    public BizBanner selectBizBannerById(Long id);

    /**
     * 查询banner轮播图列列表
     * 
     * @param bizBanner banner轮播图列
     * @return banner轮播图列集合
     */
    public List<BizBanner> selectBizBannerList(BizBanner bizBanner);

    /**
     * 新增banner轮播图列
     * 
     * @param bizBanner banner轮播图列
     * @return 结果
     */
    public int insertBizBanner(BizBanner bizBanner);

    /**
     * 修改banner轮播图列
     * 
     * @param bizBanner banner轮播图列
     * @return 结果
     */
    public int updateBizBanner(BizBanner bizBanner);

    /**
     * 删除banner轮播图列
     * 
     * @param id banner轮播图列主键
     * @return 结果
     */
    public int deleteBizBannerById(Long id);

    /**
     * 批量删除banner轮播图列
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBizBannerByIds(Long[] ids);
}
