package com.citybiz.system.mapper;

import java.util.List;
import com.citybiz.system.domain.WxUser;

/**
 * 微信用户管理Mapper接口
 * 
 * @author citybiz
 * @date 2023-10-01
 */
public interface WxUserMapper 
{
    /**
     * 查询微信用户管理
     * 
     * @param id 微信用户管理主键
     * @return 微信用户管理
     */
    public WxUser selectWxUserById(Long id);

    /**
     * 查询微信用户管理列表
     * 
     * @param wxUser 微信用户管理
     * @return 微信用户管理集合
     */
    public List<WxUser> selectWxUserList(WxUser wxUser);

    /**
     * 新增微信用户管理
     * 
     * @param wxUser 微信用户管理
     * @return 结果
     */
    public int insertWxUser(WxUser wxUser);

    /**
     * 修改微信用户管理
     * 
     * @param wxUser 微信用户管理
     * @return 结果
     */
    public int updateWxUser(WxUser wxUser);

    /**
     * 删除微信用户管理
     * 
     * @param id 微信用户管理主键
     * @return 结果
     */
    public int deleteWxUserById(Long id);

    /**
     * 批量删除微信用户管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWxUserByIds(Long[] ids);
}
