package com.citybiz.system.mapper.biz;

import com.citybiz.system.domain.biz.WxWalletLogs;

import java.util.List;

/**
 * 用户钱包明细Mapper接口
 * 
 * @author citybiz
 * @date 2023-10-18
 */
public interface WxWalletLogsMapper 
{
    /**
     * 查询用户钱包明细
     * 
     * @param id 用户钱包明细主键
     * @return 用户钱包明细
     */
    public WxWalletLogs selectWxWalletLogsById(Long id);

    /**
     * 查询用户钱包明细列表
     * 
     * @param wxWalletLogs 用户钱包明细
     * @return 用户钱包明细集合
     */
    public List<WxWalletLogs> selectWxWalletLogsList(WxWalletLogs wxWalletLogs);

    /**
     * 新增用户钱包明细
     * 
     * @param wxWalletLogs 用户钱包明细
     * @return 结果
     */
    public int insertWxWalletLogs(WxWalletLogs wxWalletLogs);

    /**
     * 修改用户钱包明细
     * 
     * @param wxWalletLogs 用户钱包明细
     * @return 结果
     */
    public int updateWxWalletLogs(WxWalletLogs wxWalletLogs);

    /**
     * 删除用户钱包明细
     * 
     * @param id 用户钱包明细主键
     * @return 结果
     */
    public int deleteWxWalletLogsById(Long id);

    /**
     * 批量删除用户钱包明细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWxWalletLogsByIds(Long[] ids);
}
