package com.citybiz.system.mapper.biz;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.citybiz.system.domain.biz.BizPayPlan;
import org.apache.ibatis.annotations.Mapper;

/**
 * Create by Ykg on 2023/10/18 00:45
 * effect: 支付计划plus接口
 */
@Mapper
public interface BizPayPlanPlusMapper extends BaseMapper<BizPayPlan> {

}
