package com.citybiz.system.service.biz.impl;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.citybiz.common.constant.Constants;
import com.citybiz.common.enums.biz.BizShopCheckStatusEnum;
import com.citybiz.common.exception.ServiceException;
import com.citybiz.common.utils.DateUtils;
import com.citybiz.common.utils.StringUtils;
import com.citybiz.system.domain.WxUser;
import com.citybiz.system.domain.biz.BizShop;
import com.citybiz.system.domain.biz.BizShopExtendInfo;
import com.citybiz.system.domain.biz.BizTaskType;
import com.citybiz.system.mapper.biz.BizShopExtendPlusMapper;
import com.citybiz.system.mapper.biz.BizShopMapper;
import com.citybiz.system.mapper.biz.BizShopPlusMapper;
import com.citybiz.system.service.IWxUserService;
import com.citybiz.system.service.biz.IBizAddressService;
import com.citybiz.system.service.biz.IBizShopService;
import com.citybiz.system.service.biz.IBizTaskTypeService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 店铺格Service业务层处理
 *
 * @author citybiz
 * @date 2023-10-02
 */
@Service
public class BizShopServiceImpl implements IBizShopService
{
    @Autowired
    private BizShopMapper bizShopMapper;

    @Autowired
    private BizShopPlusMapper bizShopPlusMapper;

    @Autowired
    private BizShopExtendPlusMapper bizShopExtendPlusMapper;

    @Autowired
    private IBizAddressService bizAddressService;

    @Autowired
    private IWxUserService wxUserService;

    @Autowired
    private IBizTaskTypeService bizTaskTypeService;

    /**
     * 查询店铺格
     *
     * @param id 店铺格主键
     * @return 店铺格
     */
    @Override
    public BizShop selectBizShopById(Long id)
    {
        BizShop bizShop = bizShopMapper.selectBizShopById(id);
        if(bizShop == null){
            throw new ServiceException("店铺不存在，请确认");
        }
        LambdaQueryWrapper<BizShopExtendInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BizShopExtendInfo::getShopId, bizShop.getId());
        BizShopExtendInfo extendInfo = bizShopExtendPlusMapper.selectOne(wrapper);
        bizShop.setExtend(extendInfo);

//        if(bizShop.getAddressId() != null){
//            String addressMsg = bizAddressService.getAddressMsg(bizShop.getAddressId());
//            bizShop.setAddressStr(addressMsg);
//        }
        Long bizTaskId = bizShop.getBizTaskId();
        if(bizTaskId != null){
            BizTaskType bizTaskType = bizTaskTypeService.selectBizTaskTypeById(bizTaskId);
            bizShop.setBizTaskStr(bizTaskType.getName());
        }
        return bizShop;
    }

    /**
     * 查询店铺格列表
     *
     * @param bizShop 店铺格
     * @return 店铺格
     */
    @Override
    public List<BizShop> selectBizShopList(BizShop bizShop, Boolean checkFlag)
    {
        LambdaQueryWrapper<BizShop> wrapper = new LambdaQueryWrapper<>();
        if(checkFlag) {
            wrapper.eq(BizShop::getPassFlag, BizShopCheckStatusEnum.CHECK_SUCCESS.getCode());
        }
        if(StringUtils.isNotEmpty(bizShop.getName())){
            wrapper.like(BizShop::getName, bizShop.getName());
        }
        if(bizShop.getId() != null){
            wrapper.eq(BizShop::getId, bizShop.getId());
        }
        if(bizShop.getBizTaskId() != null){
            wrapper.eq(BizShop::getBizTaskId, bizShop.getBizTaskId());
        }
        wrapper.eq(BizShop::getDelFlag, 0L);
        List<BizShop> bizShops = bizShopPlusMapper.selectList(wrapper);
        for (BizShop shop : bizShops) {
//            if(shop.getAddressId() != null){
//                String addressMsg = bizAddressService.getAddressMsg(shop.getAddressId());
//                shop.setAddressStr(addressMsg);
//            }
            WxUser workerWxUser = wxUserService.selectWxUserById(shop.getWorkerId());
            if (workerWxUser != null) {
                shop.setWorkerWxUserName(workerWxUser.getUsername());
            }

            Long bizTaskId = shop.getBizTaskId();
            if(bizTaskId != null){
                BizTaskType bizTaskType = bizTaskTypeService.selectBizTaskTypeById(bizTaskId);
                shop.setBizTaskStr(bizTaskType.getName());
            }
        }
        return bizShops;
    }

    /**
     * 查询店铺格列表
     *
     * @param bizShop 店铺格
     * @return 店铺格
     */
    @Override
    public Page<BizShop> selectBizShopListPage(BizShop bizShop, Boolean checkFlag, Long pageNo, Long pageSize) {
        LambdaQueryWrapper<BizShop> wrapper = new LambdaQueryWrapper<>();
        Page<BizShop> page = new Page<>();
        page.setCurrent(pageNo);
        page.setSize(pageSize);
        if(checkFlag) {
            wrapper.eq(BizShop::getPassFlag, BizShopCheckStatusEnum.CHECK_SUCCESS.getCode());
        }
        if(StringUtils.isNotEmpty(bizShop.getName())){
            wrapper.like(BizShop::getName, bizShop.getName());
        }
        if(bizShop.getId() != null){
            wrapper.eq(BizShop::getId, bizShop.getId());
        }
        if(bizShop.getBizTaskId() != null){
            wrapper.eq(BizShop::getBizTaskId, bizShop.getBizTaskId());
        }
        wrapper.eq(BizShop::getDelFlag, 0L);
        Page<BizShop> result = bizShopPlusMapper.selectPage(page, wrapper);
        Integer count = bizShopPlusMapper.selectCount(wrapper);
        result.setTotal(Long.parseLong(count.toString()));
        List<BizShop> bizShops = result.getRecords();
        for (BizShop shop : bizShops) {
//            if(shop.getAddressId() != null){
//                String addressMsg = bizAddressService.getAddressMsg(shop.getAddressId());
//                shop.setAddressStr(addressMsg);
//            }
            WxUser workerWxUser = wxUserService.selectWxUserById(shop.getWorkerId());
            if (workerWxUser != null) {
                shop.setWorkerWxUserName(workerWxUser.getUsername());
            }
            Long bizTaskId = shop.getBizTaskId();
            if(bizTaskId != null){
                BizTaskType bizTaskType = bizTaskTypeService.selectBizTaskTypeById(bizTaskId);
                shop.setBizTaskStr(bizTaskType.getName());
            }
        }
        return result;
    }

    @Override
    public List<BizShop> getNewShop() {
        LambdaQueryWrapper<BizShop> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByDesc(BizShop::getCreateTime);
        wrapper.eq(BizShop::getPassFlag, BizShopCheckStatusEnum.CHECK_SUCCESS.getCode());
        wrapper.last("LIMIT 10");
        return bizShopPlusMapper.selectList(wrapper);
    }

    /**
     * 新增店铺格
     *
     * @param bizShop 店铺格
     * @return 结果
     */
    @Override
    public int insertBizShop(BizShop bizShop)
    {
        LambdaQueryWrapper<BizShop> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BizShop::getWorkerId, bizShop.getWorkerId());
        List<BizShop> bizShops = bizShopPlusMapper.selectList(wrapper);
        if(CollectionUtils.isNotEmpty(bizShops)){
            throw new ServiceException("该用户已有商铺，请确认");
        }

        bizShop.setCreateTime(DateUtils.getNowDate());
        bizShop.setDelFlag(0L);
        bizShop.setPassFlag(BizShopCheckStatusEnum.NOT_CHECK.getCode());
        BizShopExtendInfo extendInfo = new BizShopExtendInfo();
        extendInfo.setServiceFreq(0L);
        extendInfo.setRatingNum(0L);
        extendInfo.setGoodFreqProp(BigDecimal.valueOf(0));
        extendInfo.setOverallRating(BigDecimal.valueOf(0));
        Long workerId = bizShop.getWorkerId();
        int insert = bizShopPlusMapper.insert(bizShop);
        extendInfo.setShopId(bizShop.getId());
        bizShopExtendPlusMapper.insert(extendInfo);
        WxUser wxUser = wxUserService.selectWxUserById(workerId);
        wxUser.setShopFlag(BizShopCheckStatusEnum.NOT_CHECK.getCode());
        return insert;
    }

    /**
     * 修改店铺格
     *
     * @param bizShop 店铺格
     * @return 结果
     */
    @Override
    public int updateBizShop(BizShop bizShop)
    {
        bizShop.setPassFlag(BizShopCheckStatusEnum.NOT_CHECK.getCode());
        Long workerId = bizShop.getWorkerId();
        WxUser wxUser = wxUserService.selectWxUserById(workerId);
        wxUser.setShopFlag(BizShopCheckStatusEnum.NOT_CHECK.getCode());
        return bizShopPlusMapper.updateById(bizShop);
    }

    /**
     * 批量删除店铺格
     *
     * @param ids 需要删除的店铺格主键
     * @return 结果
     */
    @Override
    public int deleteBizShopByIds(Long[] ids)
    {
        return bizShopMapper.deleteBizShopByIds(ids);
    }

    /**
     * 删除店铺格信息
     *
     * @param id 店铺格主键
     * @return 结果
     */
    @Override
    public int deleteBizShopById(Long id)
    {
        return bizShopMapper.deleteBizShopById(id);
    }

    /**
     * 获取用户自己的店铺
     * @param wxUserId 微信用户id
     * @return 结果
     */
    @Override
    public BizShop selectByUserId(Long wxUserId) {
        LambdaQueryWrapper<BizShop> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BizShop::getWorkerId, wxUserId);
        BizShop bizShop = bizShopPlusMapper.selectOne(wrapper);
        LambdaQueryWrapper<BizShopExtendInfo> extendWrapper = new LambdaQueryWrapper<>();
        extendWrapper.eq(BizShopExtendInfo::getShopId, bizShop.getId());
        BizShopExtendInfo extendInfo = bizShopExtendPlusMapper.selectOne(extendWrapper);
        bizShop.setExtend(extendInfo);
        Long bizTaskId = bizShop.getBizTaskId();
        if(bizTaskId != null){
            BizTaskType bizTaskType = bizTaskTypeService.selectBizTaskTypeById(bizTaskId);
            bizShop.setBizTaskStr(bizTaskType.getName());
        }
        return bizShop;
    }

    /**
     * 审核店铺
     * @param id
     */
    @Override
    public void checkBizShop(Long id, Long checkId) {
        if(!BizShopCheckStatusEnum.contains(checkId)){
            throw new ServiceException("审核状态错误，请确认");
        }

        BizShop bizShop = bizShopPlusMapper.selectById(id);
        if(bizShop == null){
            throw new ServiceException("店铺不存在，请确认");
        }

        bizShop.setPassFlag(checkId);
        Long workerId = bizShop.getWorkerId();
        WxUser wxUser = wxUserService.selectWxUserById(workerId);
        wxUser.setShopFlag(checkId);
        wxUserService.updateWxUser(wxUser);
        bizShopPlusMapper.updateById(bizShop);
    }

    @Override
    public BizShop getBizShopForUserId(Long id) {
        LambdaQueryWrapper<BizShop> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BizShop::getWorkerId, id);
        wrapper.eq(BizShop::getDelFlag, Constants.EXIST_VALUE);
        BizShop bizShop = bizShopPlusMapper.selectOne(wrapper);
        Long bizTaskId = bizShop.getBizTaskId();
        if(bizTaskId != null){
            BizTaskType bizTaskType = bizTaskTypeService.selectBizTaskTypeById(bizTaskId);
            bizShop.setBizTaskStr(bizTaskType.getName());
        }
        return bizShop;
    }
}
