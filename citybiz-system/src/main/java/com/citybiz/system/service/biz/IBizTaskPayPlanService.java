package com.citybiz.system.service.biz;

import com.citybiz.system.domain.dto.BizPayPlanDTO;
import com.citybiz.system.domain.dto.BizTaskConsignDTO;

import java.math.BigDecimal;
import java.util.List;

/**
 * Create by Ykg on 2024/2/11 17:51
 * effect: 任务付款阶段服务类
 */

public interface IBizTaskPayPlanService {

    /**
     * 添加付款方案
     * @param bizPayPlanDTO 付款方案信息
     */
    void addPayPlan(BizPayPlanDTO bizPayPlanDTO);

    /**
     * 修改付款方案
     * @param bizPayPlanDTO 付款方案信息
     */
    void updatePayPlan(BizPayPlanDTO bizPayPlanDTO);

    /**
     * 雇主提交付款方案
     * @param taskId 任务id
     */
    void submitPayPlan(Long taskId);

    /**
     * 威客确认付款方案
     * @param taskId 任务id
     * @param remark 备注信息
     * @param confirmFlag 是否同意 0: 不同意 1: 同意
     */
    void confirmPayPlan(Long taskId, String remark, Integer confirmFlag);

    /**
     * 雇主托管付款金额
     * @param taskPayPlanId 任务付款方案id
     * @param payAmount 托管金额
     */
    void hostingPayPlanAmount(Long taskPayPlanId, BigDecimal payAmount);

    /**
     * 雇主支付付款金额
     * @param taskPayPlanId 任务付款方案id
     * @param payAmount 托管金额
     */
    void payPlanAmount(Long taskPayPlanId, BigDecimal payAmount);

    /**
     * 招标任务交付服务
     * @param bizTaskConsignDTO 交付内容
     */
    void consignWorker(BizTaskConsignDTO bizTaskConsignDTO);

    /**
     * 雇主确认交付任务
     * @param taskPayPlanId 任务付款方案id
     */
    void confirmConsign(Long taskPayPlanId);

    /**
     * 根据任务id获取付款方案计划
     * @param taskId 任务id
     * @return 付款方案计划
     */
    List<BizPayPlanDTO> getPayPlanListForTaskId(Long taskId);
}
