package com.citybiz.system.service.biz.impl;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.citybiz.common.enums.biz.ServerOrderStatusEnum;
import com.citybiz.common.enums.biz.ServerTypeEnum;
import com.citybiz.common.exception.ServiceException;
import com.citybiz.common.utils.AssertUtils;
import com.citybiz.system.domain.biz.BizShopServer;
import com.citybiz.system.domain.biz.BizTaskType;
import com.citybiz.system.domain.biz.WxUserOrder;
import com.citybiz.system.mapper.biz.BizShopServerMapper;
import com.citybiz.system.mapper.biz.BizShopServerPlusMapper;
import com.citybiz.system.mapper.biz.WxUserOrderPlusMapper;
import com.citybiz.system.service.biz.IBizShopServerService;
import com.citybiz.system.service.biz.IBizTaskTypeService;
import com.citybiz.system.service.biz.IWxUserOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 店铺服务列Service业务层处理
 *
 * @author citybiz
 * @date 2023-10-18
 */
@Service
public class BizShopServerServiceImpl implements IBizShopServerService
{
    @Autowired
    private BizShopServerMapper bizShopServerMapper;

    @Autowired
    private BizShopServerPlusMapper bizShopServerPlusMapper;

    @Autowired
    private IWxUserOrderService wxUserOrderService;

    @Autowired
    private IBizTaskTypeService bizTaskTypeService;

    /**
     * 查询店铺服务列
     *
     * @param id 店铺服务列主键
     * @return 店铺服务列
     */
    @Override
    public BizShopServer selectBizShopServerById(Long id)
    {
        BizShopServer bizShopServer = bizShopServerMapper.selectBizShopServerById(id);
        if(bizShopServer == null){
            return null;
        }
        Long bizTaskId = bizShopServer.getBizTaskId();
        if(bizTaskId != null){
            BizTaskType bizTaskType = bizTaskTypeService.selectBizTaskTypeById(bizTaskId);
            bizShopServer.setBizTaskStr(bizTaskType.getName());
        }
        return bizShopServer;
    }

    /**
     * 查询店铺服务列列表
     *
     * @param bizShopServer 店铺服务列
     * @return 店铺服务列
     */
    @Override
    public List<BizShopServer> selectBizShopServerList(BizShopServer bizShopServer)
    {
        return bizShopServerMapper.selectBizShopServerList(bizShopServer);
    }

    /**
     * 新增店铺服务列
     *
     * @param bizShopServer 店铺服务列
     * @return 结果
     */
    @Override
    public int insertBizShopServer(BizShopServer bizShopServer)
    {
        bizShopServer.setDelFlag(0L);
        bizShopServer.setSalesVolume(0L);
        bizShopServer.setOverallRating(new BigDecimal(0));
        bizShopServer.setServiceFreq(0L);
        bizShopServer.setRatingNum(0L);
        bizShopServer.setGoodFreqProp(new BigDecimal(0));
        BigDecimal price = bizShopServer.getPrice();
        if(price.compareTo(BigDecimal.ZERO) < 0){
            throw new ServiceException("服务价格不可低于0，请确认");
        }
        return bizShopServerMapper.insertBizShopServer(bizShopServer);
    }

    /**
     * 修改店铺服务列
     *
     * @param bizShopServer 店铺服务列
     * @return 结果
     */
    @Override
    public int updateBizShopServer(BizShopServer bizShopServer)
    {
        bizShopServer.setDelFlag(0L);
        return bizShopServerMapper.updateBizShopServer(bizShopServer);
    }

    /**
     * 批量删除店铺服务列
     *
     * @param ids 需要删除的店铺服务列主键
     * @return 结果
     */
    @Override
    public int deleteBizShopServerByIds(Long[] ids)
    {
        return bizShopServerMapper.deleteBizShopServerByIds(ids);
    }

    /**
     * 删除店铺服务列信息
     *
     * @param id 店铺服务列主键
     * @return 结果
     */
    @Override
    public int deleteBizShopServerById(Long id)
    {
        return bizShopServerPlusMapper.deleteById(id);
    }

    @Override
    public List<BizShopServer> queryShopServer(Long shopId, String serverType) {
        LambdaQueryWrapper<BizShopServer> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BizShopServer::getBizShopId, shopId);
        wrapper.eq(BizShopServer::getServerType, serverType);
        wrapper.eq(BizShopServer::getDelFlag, 0L);
        List<BizShopServer> bizShopServers = bizShopServerPlusMapper.selectList(wrapper);
        for (BizShopServer bizShopServer : bizShopServers) {
            Long bizTaskId = bizShopServer.getBizTaskId();
            if(bizTaskId != null){
                BizTaskType bizTaskType = bizTaskTypeService.selectBizTaskTypeById(bizTaskId);
                bizShopServer.setBizTaskStr(bizTaskType.getName());
            }
        }

        return bizShopServers;
    }

    @Override
    @Transactional
    public void byShopServer(Long wxUserId, Long shopServerId, Long num) {
        BizShopServer bizShopServer = bizShopServerPlusMapper.selectById(shopServerId);
        if(bizShopServer == null){
            throw new ServiceException("该服务/作品不存在，请确认");
        }

        //todo 拉起微信支付订单

        WxUserOrder wxUserOrder = new WxUserOrder();
        wxUserOrder.setWxUserId(wxUserId);
        wxUserOrder.setShopBizServerId(shopServerId);
        Long bizShopId = bizShopServer.getBizShopId();
        wxUserOrder.setShopId(bizShopId);
        if(ServerTypeEnum.SERVER.getCode().equals(bizShopServer.getServerType())){
            wxUserOrder.setServerType(ServerTypeEnum.SERVER.getCode());
            wxUserOrder.setPrice(bizShopServer.getPrice());
        }else if(ServerTypeEnum.WORK.getCode().equals(bizShopServer.getServerType())){
            wxUserOrder.setServerType(ServerTypeEnum.WORK.getCode());
            AssertUtils.isNotNull(num, "作品数量不可为空");
            wxUserOrder.setUnitPrice(bizShopServer.getPrice());
            wxUserOrder.setNum(num);
            BigDecimal priceDecimal = bizShopServer.getPrice();
            BigDecimal numDecimal = new BigDecimal(num);
            wxUserOrder.setPrice(priceDecimal.multiply(numDecimal));
        }
        wxUserOrderService.insertWxUserOrder(wxUserOrder);
    }

}
