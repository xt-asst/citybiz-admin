package com.citybiz.system.service.biz.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.citybiz.common.constant.Constants;
import com.citybiz.common.exception.WxBizException;
import com.citybiz.system.domain.WxUser;
import com.citybiz.system.domain.biz.BizWxCompanyApproveInfo;
import com.citybiz.system.domain.biz.BizWxPrivateApproveInfo;
import com.citybiz.system.mapper.biz.BizWxPrivateApproveInfoMapper;
import com.citybiz.system.mapper.biz.BizWxPrivateApproveInfoPlusMapper;
import com.citybiz.system.service.IWxUserService;
import com.citybiz.system.service.biz.IBizWxPrivateApproveInfoService;
import com.citybiz.wxcommon.enums.ApproveStatusEnums;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 微信用户私人认证信息Service业务层处理
 * 
 * @author citybiz
 * @date 2023-10-04
 */
@Service
public class BizWxPrivateApproveInfoServiceImpl implements IBizWxPrivateApproveInfoService
{
    @Autowired
    private BizWxPrivateApproveInfoMapper bizWxPrivateApproveInfoMapper;

    @Autowired
    private BizWxPrivateApproveInfoPlusMapper bizWxPrivateApproveInfoPlusMapper;

    @Autowired
    private IWxUserService wxUserService;

    /**
     * 查询微信用户私人认证信息
     * 
     * @param id 微信用户私人认证信息主键
     * @return 微信用户私人认证信息
     */
    @Override
    public BizWxPrivateApproveInfo selectBizWxPrivateApproveInfoById(Long id)
    {
        return bizWxPrivateApproveInfoMapper.selectBizWxPrivateApproveInfoById(id);
    }

    @Override
    public void checkApproveInfo(Long id, Long status) {
        BizWxPrivateApproveInfo bizWxPrivateApproveInfo = bizWxPrivateApproveInfoPlusMapper.selectById(id);
        if(bizWxPrivateApproveInfo == null){
            throw new WxBizException("该认证信息不存在");
        }

        Long wxUserId = bizWxPrivateApproveInfo.getWxUserId();
        WxUser wxUser = wxUserService.selectWxUserById(wxUserId);
        // 认证成功
        if(ApproveStatusEnums.CHECK_PASS.getCode().equals(status)) {
            wxUser.setPrivateAuth(ApproveStatusEnums.CHECK_PASS.getCode());
        }else {
            wxUser.setPrivateAuth(ApproveStatusEnums.CHECK_NOT_PASS.getCode());
        }
        wxUserService.updateWxUser(wxUser);
        bizWxPrivateApproveInfo.setPassFlag(status);
        bizWxPrivateApproveInfoPlusMapper.updateById(bizWxPrivateApproveInfo);
    }

    @Override
    public BizWxPrivateApproveInfo queryApproveForUserId(Long userId) {
        LambdaQueryWrapper<BizWxPrivateApproveInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BizWxPrivateApproveInfo::getWxUserId, userId);
        wrapper.eq(BizWxPrivateApproveInfo::getDelFlag, Constants.EXIST_VALUE);
        return bizWxPrivateApproveInfoPlusMapper.selectOne(wrapper);
    }

    /**
     * 查询微信用户私人认证信息列表
     * 
     * @param bizWxPrivateApproveInfo 微信用户私人认证信息
     * @return 微信用户私人认证信息
     */
    @Override
    public List<BizWxPrivateApproveInfo> selectBizWxPrivateApproveInfoList(BizWxPrivateApproveInfo bizWxPrivateApproveInfo)
    {
        return bizWxPrivateApproveInfoMapper.selectBizWxPrivateApproveInfoList(bizWxPrivateApproveInfo);
    }

    /**
     * 新增微信用户私人认证信息
     * 
     * @param bizWxPrivateApproveInfo 微信用户私人认证信息
     * @return 结果
     */
    @Override
    public int insertBizWxPrivateApproveInfo(BizWxPrivateApproveInfo bizWxPrivateApproveInfo)
    {
        BizWxPrivateApproveInfo newInfo = new BizWxPrivateApproveInfo();
        newInfo.setRealName(bizWxPrivateApproveInfo.getRealName());
        newInfo.setWxUserId(bizWxPrivateApproveInfo.getWxUserId());
        newInfo.setIdentityCard(bizWxPrivateApproveInfo.getIdentityCard());
        newInfo.setIdentityCardFrontImg(bizWxPrivateApproveInfo.getIdentityCardFrontImg());
        newInfo.setIdentityCardBackImg(bizWxPrivateApproveInfo.getIdentityCardBackImg());
        newInfo.setPassFlag(ApproveStatusEnums.NOT_CHECK.getCode());
        newInfo.setDelFlag(0l);
        return bizWxPrivateApproveInfoPlusMapper.insert(newInfo);
    }

    /**
     * 修改微信用户私人认证信息
     * 
     * @param bizWxPrivateApproveInfo 微信用户私人认证信息
     * @return 结果
     */
    @Override
    public int updateBizWxPrivateApproveInfo(BizWxPrivateApproveInfo bizWxPrivateApproveInfo)
    {
        BizWxPrivateApproveInfo newInfo = new BizWxPrivateApproveInfo();
        newInfo.setId(bizWxPrivateApproveInfo.getId());
        newInfo.setRealName(bizWxPrivateApproveInfo.getRealName());
        newInfo.setWxUserId(bizWxPrivateApproveInfo.getWxUserId());
        newInfo.setIdentityCard(bizWxPrivateApproveInfo.getIdentityCard());
        newInfo.setIdentityCardFrontImg(bizWxPrivateApproveInfo.getIdentityCardFrontImg());
        newInfo.setIdentityCardBackImg(bizWxPrivateApproveInfo.getIdentityCardBackImg());
        newInfo.setPassFlag(ApproveStatusEnums.NOT_CHECK.getCode());
        return bizWxPrivateApproveInfoPlusMapper.updateById(bizWxPrivateApproveInfo);
    }

    /**
     * 批量删除微信用户私人认证信息
     * 
     * @param ids 需要删除的微信用户私人认证信息主键
     * @return 结果
     */
    @Override
    public int deleteBizWxPrivateApproveInfoByIds(Long[] ids)
    {
        return bizWxPrivateApproveInfoMapper.deleteBizWxPrivateApproveInfoByIds(ids);
    }

    /**
     * 删除微信用户私人认证信息信息
     * 
     * @param id 微信用户私人认证信息主键
     * @return 结果
     */
    @Override
    public int deleteBizWxPrivateApproveInfoById(Long id)
    {
        return bizWxPrivateApproveInfoMapper.deleteBizWxPrivateApproveInfoById(id);
    }

    @Override
    public BizWxPrivateApproveInfo selectByUserId(Long wxUserId) {
        LambdaQueryWrapper<BizWxPrivateApproveInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BizWxPrivateApproveInfo::getWxUserId, wxUserId);
        wrapper.eq(BizWxPrivateApproveInfo::getDelFlag,0L);
        return bizWxPrivateApproveInfoPlusMapper.selectOne(wrapper);
    }
}
