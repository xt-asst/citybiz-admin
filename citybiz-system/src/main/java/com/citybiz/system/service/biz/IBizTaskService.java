package com.citybiz.system.service.biz;

import com.citybiz.common.core.page.BizTableDataInfo;
import com.citybiz.system.domain.biz.BizTask;
import com.citybiz.system.domain.dto.BizTaskDto;
import com.citybiz.system.domain.dto.BizTaskExtendInfoDTO;
import com.citybiz.system.domain.request.BizTaskRequest;

import java.util.List;

/**
 * 任务格Service接口
 *
 * @author citybiz
 * @date 2023-10-01
 */
public interface IBizTaskService
{
    /**
     * 查询任务格
     *
     * @param id 任务格主键
     * @return 任务格
     */
    public BizTaskDto selectBizTaskById(Long id);

    /**
     * 大厅查询任务分页列表
     * @param request 请求体
     * @return 分页结果
     */
    public BizTableDataInfo<BizTaskDto> indexTask(BizTaskRequest request);

    /**
     * 查询任务格列表
     *
     * @param request 任务格请求信息
     * @return 任务格集合
     */
    public List<BizTaskDto> selectBizTaskList(BizTaskRequest request);


    public BizTableDataInfo<BizTaskDto> selectBizTaskListByPage(BizTaskRequest request);

    /**
     * 根据工作者id查询任务信息
     * @param workerId 工作者id
     * @return 任务列表
     */
    public BizTableDataInfo<BizTaskDto> selectBizTaskListByWorker(Long workerId, String taskSort);

    /**
     * 新增任务格
     *
     * @param request 任务格请求信息
     * @return 结果
     */
    public int insertBizTask(BizTaskRequest request);

    /**
     * 修改任务格
     *
     * @param request 任务格请求信息
     * @return 结果
     */
    public int updateBizTask(BizTaskRequest request);

    /**
     * 批量删除任务格
     *
     * @param ids 需要删除的任务格主键集合
     * @return 结果
     */
    public int deleteBizTaskByIds(Long[] ids);

    /**
     * 删除任务格信息
     *
     * @param id 任务格主键
     * @return 结果
     */
    public int deleteBizTaskById(Long id);

    public BizTaskExtendInfoDTO getTaskExtend(Long taskId);

    public void updateTaskExtend(BizTaskExtendInfoDTO bizTaskExtendInfoDTO);
}
