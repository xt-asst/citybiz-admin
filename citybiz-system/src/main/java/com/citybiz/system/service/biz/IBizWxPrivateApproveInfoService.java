package com.citybiz.system.service.biz;

import com.citybiz.system.domain.biz.BizWxCompanyApproveInfo;
import com.citybiz.system.domain.biz.BizWxPrivateApproveInfo;

import java.util.List;

/**
 * 微信用户私人认证信息Service接口
 * 
 * @author citybiz
 * @date 2023-10-04
 */
public interface IBizWxPrivateApproveInfoService 
{
    /**
     * 查询微信用户私人认证信息
     * 
     * @param id 微信用户私人认证信息主键
     * @return 微信用户私人认证信息
     */
    public BizWxPrivateApproveInfo selectBizWxPrivateApproveInfoById(Long id);

    /**
     * 审核/拒绝私人认证信息
     * @param id 私人认证信息id
     */
    public void checkApproveInfo(Long id, Long status);


    /**
     * 根据用户id查询企业认证信息
     * @param userId 用户id
     * @return 企业认证信息
     */
    public BizWxPrivateApproveInfo queryApproveForUserId(Long userId);

    /**
     * 查询微信用户私人认证信息列表
     * 
     * @param bizWxPrivateApproveInfo 微信用户私人认证信息
     * @return 微信用户私人认证信息集合
     */
    public List<BizWxPrivateApproveInfo> selectBizWxPrivateApproveInfoList(BizWxPrivateApproveInfo bizWxPrivateApproveInfo);

    /**
     * 新增微信用户私人认证信息
     * 
     * @param bizWxPrivateApproveInfo 微信用户私人认证信息
     * @return 结果
     */
    public int insertBizWxPrivateApproveInfo(BizWxPrivateApproveInfo bizWxPrivateApproveInfo);

    /**
     * 查询私人认证信息
     * @param wxUserId 微信用户id
     * @return 私人认证信息
     */
    public BizWxPrivateApproveInfo selectByUserId(Long wxUserId);

    /**
     * 修改微信用户私人认证信息
     * 
     * @param bizWxPrivateApproveInfo 微信用户私人认证信息
     * @return 结果
     */
    public int updateBizWxPrivateApproveInfo(BizWxPrivateApproveInfo bizWxPrivateApproveInfo);

    /**
     * 批量删除微信用户私人认证信息
     * 
     * @param ids 需要删除的微信用户私人认证信息主键集合
     * @return 结果
     */
    public int deleteBizWxPrivateApproveInfoByIds(Long[] ids);

    /**
     * 删除微信用户私人认证信息信息
     * 
     * @param id 微信用户私人认证信息主键
     * @return 结果
     */
    public int deleteBizWxPrivateApproveInfoById(Long id);
}
