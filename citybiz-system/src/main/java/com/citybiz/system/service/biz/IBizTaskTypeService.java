package com.citybiz.system.service.biz;

import com.citybiz.system.domain.biz.BizTaskType;

import java.util.List;

/**
 * 任务类型管理Service接口
 * 
 * @author citybiz
 * @date 2023-10-01
 */
public interface IBizTaskTypeService 
{
    /**
     * 查询任务类型管理
     * 
     * @param id 任务类型管理主键
     * @return 任务类型管理
     */
    public BizTaskType selectBizTaskTypeById(Long id);

    /**
     * 查询任务类型管理列表
     * 
     * @param bizTaskType 任务类型管理
     * @return 任务类型管理集合
     */
    public List<BizTaskType> selectBizTaskTypeList(BizTaskType bizTaskType);

    /**
     * 新增任务类型管理
     * 
     * @param bizTaskType 任务类型管理
     * @return 结果
     */
    public int insertBizTaskType(BizTaskType bizTaskType);

    /**
     * 修改任务类型管理
     * 
     * @param bizTaskType 任务类型管理
     * @return 结果
     */
    public int updateBizTaskType(BizTaskType bizTaskType);

    /**
     * 批量删除任务类型管理
     * 
     * @param ids 需要删除的任务类型管理主键集合
     * @return 结果
     */
    public int deleteBizTaskTypeByIds(Long[] ids);

    /**
     * 删除任务类型管理信息
     * 
     * @param id 任务类型管理主键
     * @return 结果
     */
    public int deleteBizTaskTypeById(Long id);

    /**
     * 获取父级业务类型列表
     * @return 结果
     */
    public List<BizTaskType> getParentTypes();

    /**
     * 获取子级业务类型列表
     * @param parentId 父级id
     * @return 结果
     */
    public List<BizTaskType> getChildTypes(Long parentId);

}
