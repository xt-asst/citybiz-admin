package com.citybiz.system.service.biz;

import com.citybiz.system.domain.biz.BizWxCompanyApproveInfo;

import java.util.List;

/**
 * 公司认证审核Service接口
 * 
 * @author citybiz
 * @date 2023-10-04
 */
public interface IBizWxCompanyApproveInfoService 
{
    /**
     * 查询公司认证审核
     * 
     * @param id 公司认证审核主键
     * @return 公司认证审核
     */
    public BizWxCompanyApproveInfo selectBizWxCompanyApproveInfoById(Long id);

    /**
     * 审核公司认证信息
     * @param id
     * @param status
     */
    public void checkApproveInfo(Long id, Long status);

    /**
     * 根据用户id查询企业认证信息
     * @param userId 用户id
     * @return 企业认证信息
     */
    public BizWxCompanyApproveInfo queryApproveForUserId(Long userId);

    /**
     * 查询公司认证审核列表
     * 
     * @param bizWxCompanyApproveInfo 公司认证审核
     * @return 公司认证审核集合
     */
    public List<BizWxCompanyApproveInfo> selectBizWxCompanyApproveInfoList(BizWxCompanyApproveInfo bizWxCompanyApproveInfo);

    /**
     * 新增公司认证审核
     * 
     * @param bizWxCompanyApproveInfo 公司认证审核
     * @return 结果
     */
    public int insertBizWxCompanyApproveInfo(BizWxCompanyApproveInfo bizWxCompanyApproveInfo);

    /**
     * 修改公司认证审核
     * 
     * @param bizWxCompanyApproveInfo 公司认证审核
     * @return 结果
     */
    public int updateBizWxCompanyApproveInfo(BizWxCompanyApproveInfo bizWxCompanyApproveInfo);

    /**
     * 批量删除公司认证审核
     * 
     * @param ids 需要删除的公司认证审核主键集合
     * @return 结果
     */
    public int deleteBizWxCompanyApproveInfoByIds(Long[] ids);

    /**
     * 删除公司认证审核信息
     * 
     * @param id 公司认证审核主键
     * @return 结果
     */
    public int deleteBizWxCompanyApproveInfoById(Long id);

    /**
     * 根据用户id获取公司认证信息
     * @param id 用户id
     * @return 公司认证信息
     */
    public BizWxCompanyApproveInfo selectByUserId(Long id);
}
