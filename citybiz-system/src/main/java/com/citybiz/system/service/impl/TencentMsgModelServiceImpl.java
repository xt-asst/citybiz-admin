package com.citybiz.system.service.impl;

import com.citybiz.common.core.redis.RedisCache;
import com.citybiz.common.exception.ServiceException;
import com.citybiz.common.utils.StringUtils;
import com.citybiz.common.utils.VerifyCodeUtils;
import com.github.qcloudsms.SmsSingleSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * create by Ykg on 2022/12/04
 * effect: 腾讯云短信服务类
 */
@Service
@Slf4j
public class TencentMsgModelServiceImpl {

    private final static String META_TYPE = "system";

    private final static String CONFIG_GROUP = "tencent_text_message";

    @Value(("${txsms.appId}"))
    private Integer APP_ID;

    @Value(("${txsms.appKey}"))
    private String APP_KEY;

    @Value(("${txsms.templateId}"))
    private Integer TEMPLATE_ID;

    @Value(("${txsms.signName}"))
    private String SIGN_NAME;

    @Autowired
    private RedisCache redisCache;

    private static final Integer expireTime = 10 * 60 * 1000;

    private static final String PREFIX = "VerifyCode_";

    /**
     * 发送短信
     * @param phoneNumber 手机号码
     */
    public void sendMsgModel(String phoneNumber){
        String code = VerifyCodeUtils.getCode();
        String[] params = {code};
        // 构建短信发送器
        SmsSingleSender sender = new SmsSingleSender(APP_ID, APP_KEY);
        //数据存入redis
        try {
            String redisCode = (String)redisCache.getCacheObject(PREFIX + phoneNumber);
            if(StringUtils.isNotEmpty(redisCode)){
                //如果有数据则删除
                redisCache.deleteObject(PREFIX + phoneNumber);
            }
            log.error("验证码:" + code);
            redisCache.setCacheObject(PREFIX + phoneNumber, code, expireTime, TimeUnit.SECONDS);
            sender.sendWithParam("86", phoneNumber, TEMPLATE_ID, params, SIGN_NAME, "", "");
        }catch (Exception e){
            e.printStackTrace();
            log.error("短信发送失败，手机号:{}, 验证码:{}, templateId:{}",phoneNumber, code,TEMPLATE_ID);
            throw new ServiceException("短信发送失败");
        }
    }

    /**
     * 删除关于手机号的验证码信息
     * @param phoneNumber 手机号码
     */
    public void deleted(String phoneNumber){
        try{
            redisCache.deleteObject(PREFIX + phoneNumber);
        }catch (Exception e){
            e.printStackTrace();
            log.error("缓存删除失败，手机号:{}",phoneNumber);
            throw new ServiceException("短信发送失败");
        }
    }

    /**
     * 获取手机验证码
     * @param phoneNumber 手机号码
     * @return 缓存中的验证码
     */
    public String getVerifyCode(String phoneNumber){
        String code = "";
        try{
            code = (String)redisCache.getCacheObject(PREFIX + phoneNumber);
            redisCache.deleteObject(PREFIX + phoneNumber);
        }catch (Exception e){
            e.printStackTrace();
            log.error("获取手机验证码，手机号:{}",phoneNumber);
            throw new ServiceException("验证码校验失败，请重新尝试");
        }
        if(StringUtils.isEmpty(code)){
            throw new ServiceException("验证码校验失败，请重新尝试");
        }
        return code;
    }

}
