package com.citybiz.system.service.biz;

import com.citybiz.system.domain.biz.BizBanner;

import java.util.List;

/**
 * banner轮播图列Service接口
 * 
 * @author ykg
 * @date 2023-09-29
 */
public interface IBizBannerService 
{
    /**
     * 查询banner轮播图列
     * 
     * @param id banner轮播图列主键
     * @return banner轮播图列
     */
    public BizBanner selectBizBannerById(Long id);

    /**
     * 查询banner轮播图列列表
     * 
     * @param bizBanner banner轮播图列
     * @return banner轮播图列集合
     */
    public List<BizBanner> selectBizBannerList(BizBanner bizBanner);

    /**
     * 新增banner轮播图列
     * 
     * @param bizBanner banner轮播图列
     * @return 结果
     */
    public int insertBizBanner(BizBanner bizBanner);

    /**
     * 修改banner轮播图列
     * 
     * @param bizBanner banner轮播图列
     * @return 结果
     */
    public int updateBizBanner(BizBanner bizBanner);

    /**
     * 批量删除banner轮播图列
     * 
     * @param ids 需要删除的banner轮播图列主键集合
     * @return 结果
     */
    public int deleteBizBannerByIds(Long[] ids);

    /**
     * 删除banner轮播图列信息
     * 
     * @param id banner轮播图列主键
     * @return 结果
     */
    public int deleteBizBannerById(Long id);

    /**
     * 首页获取轮播图列表
     * @param count 需要几个
     * @return 列表
     */
    public List<BizBanner> getHomeBannerList(int count);
}
