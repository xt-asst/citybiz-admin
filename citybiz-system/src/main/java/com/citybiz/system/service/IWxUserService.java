package com.citybiz.system.service;

import java.util.List;
import com.citybiz.system.domain.WxUser;

/**
 * 微信用户管理Service接口
 * 
 * @author citybiz
 * @date 2023-10-01
 */
public interface IWxUserService 
{
    /**
     * 查询微信用户管理
     * 
     * @param id 微信用户管理主键
     * @return 微信用户管理
     */
    public WxUser selectWxUserById(Long id);

    /**
     * 查询微信用户管理列表
     * 
     * @param wxUser 微信用户管理
     * @return 微信用户管理集合
     */
    public List<WxUser> selectWxUserList(WxUser wxUser);

    /**
     * 新增微信用户管理
     * 
     * @param wxUser 微信用户管理
     * @return 结果
     */
    public int insertWxUser(WxUser wxUser);

    /**
     * 修改微信用户管理
     * 
     * @param wxUser 微信用户管理
     * @return 结果
     */
    public int updateWxUser(WxUser wxUser);

    /**
     * 批量删除微信用户管理
     * 
     * @param ids 需要删除的微信用户管理主键集合
     * @return 结果
     */
    public int deleteWxUserByIds(Long[] ids);

    /**
     * 删除微信用户管理信息
     * 
     * @param id 微信用户管理主键
     * @return 结果
     */
    public int deleteWxUserById(Long id);

    /**
     * 根据微信openid获取用户信息
     * @param openId 微信openid
     * @return 用户
     */
    public WxUser getWxUserForOpenId(String openId);
}
