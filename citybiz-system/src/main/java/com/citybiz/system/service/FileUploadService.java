package com.citybiz.system.service;

import java.io.File;

/**
 * Create by Ykg on 2022/11/23
 * effect: 文件上传接口
 */
public interface FileUploadService {
    String upload(File file, String suffix);
}

