package com.citybiz.system.service.biz.impl;

import java.util.List;
import com.citybiz.common.utils.DateUtils;
import com.citybiz.system.domain.biz.BizMessage;
import com.citybiz.system.mapper.biz.BizMessageMapper;
import com.citybiz.system.service.biz.IBizMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 消息业务格Service业务层处理
 * 
 * @author citybiz
 * @date 2023-11-04
 */
@Service
public class BizMessageServiceImpl implements IBizMessageService
{
    @Autowired
    private BizMessageMapper bizMessageMapper;

    /**
     * 查询消息业务格
     * 
     * @param id 消息业务格主键
     * @return 消息业务格
     */
    @Override
    public BizMessage selectBizMessageById(Long id)
    {
        return bizMessageMapper.selectBizMessageById(id);
    }

    /**
     * 查询消息业务格列表
     * 
     * @param bizMessage 消息业务格
     * @return 消息业务格
     */
    @Override
    public List<BizMessage> selectBizMessageList(BizMessage bizMessage)
    {
        return bizMessageMapper.selectBizMessageList(bizMessage);
    }

    /**
     * 新增消息业务格
     * 
     * @param bizMessage 消息业务格
     * @return 结果
     */
    @Override
    public int insertBizMessage(BizMessage bizMessage)
    {
        bizMessage.setCreateTime(DateUtils.getNowDate());
        return bizMessageMapper.insertBizMessage(bizMessage);
    }

    /**
     * 修改消息业务格
     * 
     * @param bizMessage 消息业务格
     * @return 结果
     */
    @Override
    public int updateBizMessage(BizMessage bizMessage)
    {
        return bizMessageMapper.updateBizMessage(bizMessage);
    }

    /**
     * 批量删除消息业务格
     * 
     * @param ids 需要删除的消息业务格主键
     * @return 结果
     */
    @Override
    public int deleteBizMessageByIds(Long[] ids)
    {
        return bizMessageMapper.deleteBizMessageByIds(ids);
    }

    /**
     * 删除消息业务格信息
     * 
     * @param id 消息业务格主键
     * @return 结果
     */
    @Override
    public int deleteBizMessageById(Long id)
    {
        return bizMessageMapper.deleteBizMessageById(id);
    }
}
