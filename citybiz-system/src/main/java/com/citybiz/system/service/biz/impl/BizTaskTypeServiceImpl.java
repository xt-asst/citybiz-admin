package com.citybiz.system.service.biz.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.citybiz.common.exception.ServiceException;
import com.citybiz.common.utils.AssertUtils;
import com.citybiz.common.utils.DateUtils;
import com.citybiz.system.domain.biz.BizTaskType;
import com.citybiz.system.mapper.biz.BizTaskPlusMapper;
import com.citybiz.system.mapper.biz.BizTaskTypeMapper;
import com.citybiz.system.mapper.biz.BizTaskTypePlusMapper;
import com.citybiz.system.service.biz.IBizTaskTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 任务类型管理Service业务层处理
 * 
 * @author citybiz
 * @date 2023-10-01
 */
@Service
public class BizTaskTypeServiceImpl implements IBizTaskTypeService
{
    @Autowired
    private BizTaskTypeMapper bizTaskTypeMapper;

    @Autowired
    private BizTaskTypePlusMapper bizTaskTypePlusMapper;

    /**
     * 查询任务类型管理
     * 
     * @param id 任务类型管理主键
     * @return 任务类型管理
     */
    @Override
    public BizTaskType selectBizTaskTypeById(Long id)
    {
        return bizTaskTypeMapper.selectBizTaskTypeById(id);
    }

    /**
     * 查询任务类型管理列表
     * 
     * @param bizTaskType 任务类型管理
     * @return 任务类型管理
     */
    @Override
    public List<BizTaskType> selectBizTaskTypeList(BizTaskType bizTaskType)
    {
        return bizTaskTypeMapper.selectBizTaskTypeList(bizTaskType);
    }

    /**
     * 新增任务类型管理
     * 
     * @param bizTaskType 任务类型管理
     * @return 结果
     */
    @Override
    public int insertBizTaskType(BizTaskType bizTaskType)
    {
        bizTaskType.setCreateTime(DateUtils.getNowDate());
        bizTaskType.setDelFlag(0L);
        return bizTaskTypeMapper.insertBizTaskType(bizTaskType);
    }

    /**
     * 修改任务类型管理
     * 
     * @param bizTaskType 任务类型管理
     * @return 结果
     */
    @Override
    public int updateBizTaskType(BizTaskType bizTaskType)
    {
        return bizTaskTypeMapper.updateBizTaskType(bizTaskType);
    }

    /**
     * 批量删除任务类型管理
     * 
     * @param ids 需要删除的任务类型管理主键
     * @return 结果
     */
    @Override
    public int deleteBizTaskTypeByIds(Long[] ids)
    {
        return bizTaskTypeMapper.deleteBizTaskTypeByIds(ids);
    }

    /**
     * 删除任务类型管理信息
     * 
     * @param id 任务类型管理主键
     * @return 结果
     */
    @Override
    public int deleteBizTaskTypeById(Long id)
    {
        return bizTaskTypeMapper.deleteBizTaskTypeById(id);
    }

    @Override
    public List<BizTaskType> getParentTypes() {
        LambdaQueryWrapper<BizTaskType> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BizTaskType::getDelFlag, 0L);
        wrapper.eq(BizTaskType::getParentId, 0L);
        List<BizTaskType> bizTaskTypes = bizTaskTypePlusMapper.selectList(wrapper);
        for (BizTaskType bizTaskType : bizTaskTypes) {
            List<BizTaskType> childTypes = getChildTypes(bizTaskType.getId());
            bizTaskType.setChildTypes(childTypes);
        }
        return bizTaskTypes;
    }

    @Override
    public List<BizTaskType> getChildTypes(Long parentId) {
        AssertUtils.isNotNull(parentId, "父级id不可为空");
        BizTaskType parentTaskType = bizTaskTypeMapper.selectBizTaskTypeById(parentId);
        if(parentTaskType == null){
            throw new ServiceException("父级组织不存在，请确认");
        }


        LambdaQueryWrapper<BizTaskType> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BizTaskType::getDelFlag, 0L);
        wrapper.eq(BizTaskType::getParentId, parentId);
        return bizTaskTypePlusMapper.selectList(wrapper);
    }
}
