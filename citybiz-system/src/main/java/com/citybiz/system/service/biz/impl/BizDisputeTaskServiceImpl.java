package com.citybiz.system.service.biz.impl;

import java.util.List;
import com.citybiz.common.utils.DateUtils;
import com.citybiz.system.domain.biz.BizDisputeTask;
import com.citybiz.system.mapper.biz.BizDisputeTaskMapper;
import com.citybiz.system.service.biz.IBizDisputeTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 交易维权信息Service业务层处理
 * 
 * @author citybiz
 * @date 2023-10-15
 */
@Service
public class BizDisputeTaskServiceImpl implements IBizDisputeTaskService
{
    @Autowired
    private BizDisputeTaskMapper bizDisputeTaskMapper;

    /**
     * 查询交易维权信息
     * 
     * @param id 交易维权信息主键
     * @return 交易维权信息
     */
    @Override
    public BizDisputeTask selectBizDisputeTaskById(Long id)
    {
        return bizDisputeTaskMapper.selectBizDisputeTaskById(id);
    }

    /**
     * 查询交易维权信息列表
     * 
     * @param bizDisputeTask 交易维权信息
     * @return 交易维权信息
     */
    @Override
    public List<BizDisputeTask> selectBizDisputeTaskList(BizDisputeTask bizDisputeTask)
    {
        return bizDisputeTaskMapper.selectBizDisputeTaskList(bizDisputeTask);
    }

    /**
     * 新增交易维权信息
     * 
     * @param bizDisputeTask 交易维权信息
     * @return 结果
     */
    @Override
    public int insertBizDisputeTask(BizDisputeTask bizDisputeTask)
    {
        bizDisputeTask.setCreateTime(DateUtils.getNowDate());
        return bizDisputeTaskMapper.insertBizDisputeTask(bizDisputeTask);
    }

    /**
     * 修改交易维权信息
     * 
     * @param bizDisputeTask 交易维权信息
     * @return 结果
     */
    @Override
    public int updateBizDisputeTask(BizDisputeTask bizDisputeTask)
    {
        return bizDisputeTaskMapper.updateBizDisputeTask(bizDisputeTask);
    }

    /**
     * 批量删除交易维权信息
     * 
     * @param ids 需要删除的交易维权信息主键
     * @return 结果
     */
    @Override
    public int deleteBizDisputeTaskByIds(Long[] ids)
    {
        return bizDisputeTaskMapper.deleteBizDisputeTaskByIds(ids);
    }

    /**
     * 删除交易维权信息信息
     * 
     * @param id 交易维权信息主键
     * @return 结果
     */
    @Override
    public int deleteBizDisputeTaskById(Long id)
    {
        return bizDisputeTaskMapper.deleteBizDisputeTaskById(id);
    }
}
