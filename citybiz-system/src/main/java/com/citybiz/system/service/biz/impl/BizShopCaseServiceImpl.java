package com.citybiz.system.service.biz.impl;

import java.util.List;

import com.citybiz.system.domain.biz.BizShopCase;
import com.citybiz.system.mapper.biz.BizShopCaseMapper;
import com.citybiz.system.service.biz.IBizShopCaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 店铺案例Service业务层处理
 *
 * @author citybiz
 * @date 2023-10-18
 */
@Service
public class BizShopCaseServiceImpl implements IBizShopCaseService
{
    @Autowired
    private BizShopCaseMapper bizShopCaseMapper;

    /**
     * 查询店铺案例
     *
     * @param id 店铺案例主键
     * @return 店铺案例
     */
    @Override
    public BizShopCase selectBizShopCaseById(Long id)
    {
        return bizShopCaseMapper.selectBizShopCaseById(id);
    }

    /**
     * 查询店铺案例列表
     *
     * @param bizShopCase 店铺案例
     * @return 店铺案例
     */
    @Override
    public List<BizShopCase> selectBizShopCaseList(BizShopCase bizShopCase)
    {
        return bizShopCaseMapper.selectBizShopCaseList(bizShopCase);
    }

    /**
     * 新增店铺案例
     *
     * @param bizShopCase 店铺案例
     * @return 结果
     */
    @Override
    public int insertBizShopCase(BizShopCase bizShopCase)
    {
        bizShopCase.setDelFlag(0L);
        return bizShopCaseMapper.insertBizShopCase(bizShopCase);
    }

    /**
     * 修改店铺案例
     *
     * @param bizShopCase 店铺案例
     * @return 结果
     */
    @Override
    public int updateBizShopCase(BizShopCase bizShopCase)
    {
        return bizShopCaseMapper.updateBizShopCase(bizShopCase);
    }

    /**
     * 批量删除店铺案例
     *
     * @param ids 需要删除的店铺案例主键
     * @return 结果
     */
    @Override
    public int deleteBizShopCaseByIds(Long[] ids)
    {
        return bizShopCaseMapper.deleteBizShopCaseByIds(ids);
    }

    /**
     * 删除店铺案例信息
     *
     * @param id 店铺案例主键
     * @return 结果
     */
    @Override
    public int deleteBizShopCaseById(Long id)
    {
        return bizShopCaseMapper.deleteBizShopCaseById(id);
    }
}
