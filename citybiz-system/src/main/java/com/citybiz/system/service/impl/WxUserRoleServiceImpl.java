package com.citybiz.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.citybiz.common.exception.WxBizException;
import com.citybiz.system.domain.WxUserRole;
import com.citybiz.system.mapper.WxUserRolePlusMapper;
import com.citybiz.system.service.IWxUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Create by Ykg on 2023/10/3 10:58
 * effect:
 */
@Service
public class WxUserRoleServiceImpl implements IWxUserRoleService {

    @Autowired
    private WxUserRolePlusMapper wxUserRolePlusMapper;

    @Override
    public WxUserRole getWxUserRoleForWxUserId(Long wxUserId, Integer role) {
        LambdaQueryWrapper<WxUserRole> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(WxUserRole::getWxUserId, wxUserId)
                .eq(WxUserRole::getRole, role);
        WxUserRole wxUserRole = wxUserRolePlusMapper.selectOne(wrapper);
        if(wxUserRole == null){
            throw new WxBizException("微信角色有误，请联系管理员");
        }
        return wxUserRole;
    }

}
