package com.citybiz.system.service.biz.impl;

import java.util.List;

import com.citybiz.system.domain.biz.WxWalletLogs;
import com.citybiz.system.mapper.biz.WxWalletLogsMapper;
import com.citybiz.system.service.biz.IWxWalletLogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 用户钱包明细Service业务层处理
 * 
 * @author citybiz
 * @date 2023-10-18
 */
@Service
public class WxWalletLogsServiceImpl implements IWxWalletLogsService
{
    @Autowired
    private WxWalletLogsMapper wxWalletLogsMapper;

    /**
     * 查询用户钱包明细
     * 
     * @param id 用户钱包明细主键
     * @return 用户钱包明细
     */
    @Override
    public WxWalletLogs selectWxWalletLogsById(Long id)
    {
        return wxWalletLogsMapper.selectWxWalletLogsById(id);
    }

    /**
     * 查询用户钱包明细列表
     * 
     * @param wxWalletLogs 用户钱包明细
     * @return 用户钱包明细
     */
    @Override
    public List<WxWalletLogs> selectWxWalletLogsList(WxWalletLogs wxWalletLogs)
    {
        return wxWalletLogsMapper.selectWxWalletLogsList(wxWalletLogs);
    }

    /**
     * 新增用户钱包明细
     * 
     * @param wxWalletLogs 用户钱包明细
     * @return 结果
     */
    @Override
    public int insertWxWalletLogs(WxWalletLogs wxWalletLogs)
    {
        return wxWalletLogsMapper.insertWxWalletLogs(wxWalletLogs);
    }

    /**
     * 修改用户钱包明细
     * 
     * @param wxWalletLogs 用户钱包明细
     * @return 结果
     */
    @Override
    public int updateWxWalletLogs(WxWalletLogs wxWalletLogs)
    {
        return wxWalletLogsMapper.updateWxWalletLogs(wxWalletLogs);
    }

    /**
     * 批量删除用户钱包明细
     * 
     * @param ids 需要删除的用户钱包明细主键
     * @return 结果
     */
    @Override
    public int deleteWxWalletLogsByIds(Long[] ids)
    {
        return wxWalletLogsMapper.deleteWxWalletLogsByIds(ids);
    }

    /**
     * 删除用户钱包明细信息
     * 
     * @param id 用户钱包明细主键
     * @return 结果
     */
    @Override
    public int deleteWxWalletLogsById(Long id)
    {
        return wxWalletLogsMapper.deleteWxWalletLogsById(id);
    }
}
