package com.citybiz.system.service.biz.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.citybiz.common.exception.ServiceException;
import com.citybiz.common.utils.StringUtils;
import com.citybiz.system.domain.biz.BizAddressTable;
import com.citybiz.system.mapper.biz.BizAddressPlusMapper;
import com.citybiz.system.service.biz.IBizAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Create by Ykg on 2023/10/19 15:32
 * effect:
 */
@Service
public class BizAddressServiceImpl implements IBizAddressService {

    private final static String PROVINCE = "province";
    private final static String CITY = "city";
    private final static String DISTRICT = "district";

    @Autowired
    private BizAddressPlusMapper bizAddressPlusMapper;

    @Override
    public List<BizAddressTable> getBizAddress(Long parentId, String type) {
        LambdaQueryWrapper<BizAddressTable> wrapper = new LambdaQueryWrapper<>();

        if(parentId == null){
            // 查询省份地址
            if(!PROVINCE.equals(type)){
                throw new ServiceException("请查询省份类型");
            }
            wrapper.eq(BizAddressTable::getType, PROVINCE);
        }else {
            if(PROVINCE.equals(type)){
                throw new ServiceException("请查询城市/区域类型");
            }

            if(!CITY.equals(type) && !DISTRICT.equals(type)){
                throw new ServiceException("查询类型错误，请确认");
            }
            wrapper.eq(BizAddressTable::getType, type);
            wrapper.eq(BizAddressTable::getParentId, parentId);
        }
        return bizAddressPlusMapper.selectList(wrapper);
    }

    @Override
    public String getAddressMsg(Long id) {
        BizAddressTable bizAddressTable = bizAddressPlusMapper.selectById(id);
        if(bizAddressTable != null) {
            String province = bizAddressTable.getProvince();
            String city = bizAddressTable.getCity();
            String name = bizAddressTable.getName();

            List<String> address = new ArrayList<>();

            if (StringUtils.isNotEmpty(province)) {
                address.add(province);
            }
            if (StringUtils.isNotEmpty(city)) {
                address.add(city);
            }
            if (StringUtils.isNotEmpty(name)) {
                address.add(name);
            }
            return String.join("-", address);
        }

        return "";
    }
}
