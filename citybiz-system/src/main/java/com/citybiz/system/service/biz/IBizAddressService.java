package com.citybiz.system.service.biz;

import com.citybiz.system.domain.biz.BizAddressTable;

import java.util.List;

/**
 * Create by Ykg on 2023/10/19 15:31
 * effect: 地址服务类
 */

public interface IBizAddressService {

    List<BizAddressTable> getBizAddress(Long parentId, String type);

    String getAddressMsg(Long id);

}
