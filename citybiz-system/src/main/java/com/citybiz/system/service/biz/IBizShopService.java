package com.citybiz.system.service.biz;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.citybiz.system.domain.biz.BizShop;

import java.util.List;

/**
 * 店铺格Service接口
 *
 * @author citybiz
 * @date 2023-10-02
 */
public interface IBizShopService
{
    /**
     * 查询店铺格
     *
     * @param id 店铺格主键
     * @return 店铺格
     */
    public BizShop selectBizShopById(Long id);

    /**
     * 查询店铺格列表
     *
     * @param bizShop 店铺格
     * @return 店铺格集合
     */
    public List<BizShop> selectBizShopList(BizShop bizShop, Boolean checkFlag);

    public Page<BizShop> selectBizShopListPage(BizShop bizShop, Boolean checkFlag, Long pageNo, Long pageSize);


        public List<BizShop> getNewShop();


    /**
     * 新增店铺格
     *
     * @param bizShop 店铺格
     * @return 结果
     */
    public int insertBizShop(BizShop bizShop);

    /**
     * 审核店铺
     * @param id
     */
    public void checkBizShop(Long id, Long checkCode);

    /**
     * 根据用户id获取店铺信息
     * @param id 用户id
     * @return 店铺信息
     */
    public BizShop getBizShopForUserId(Long id);

    /**
     * 修改店铺格
     *
     * @param bizShop 店铺格
     * @return 结果
     */
    public int updateBizShop(BizShop bizShop);

    /**
     * 批量删除店铺格
     *
     * @param ids 需要删除的店铺格主键集合
     * @return 结果
     */
    public int deleteBizShopByIds(Long[] ids);

    /**
     * 删除店铺格信息
     *
     * @param id 店铺格主键
     * @return 结果
     */
    public int deleteBizShopById(Long id);

    /**
     * 根据微信用户id查询店铺
     * @param wxUserId 微信用户id
     * @return
     */
    public BizShop selectByUserId(Long wxUserId);
}
