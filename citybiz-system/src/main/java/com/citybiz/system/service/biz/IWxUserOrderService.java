package com.citybiz.system.service.biz;

import com.citybiz.system.domain.biz.WxUserOrder;
import com.citybiz.system.mapper.biz.WxUserOrderPlusMapper;

import java.util.List;

/**
 * Create by Ykg on 2023/10/18 23:14
 * effect: 用户订单服务类
 */

public interface IWxUserOrderService {

    List<WxUserOrder> queryList(WxUserOrder wxUserOrder);

    void insertWxUserOrder(WxUserOrder wxUserOrder);

    void updateWxUserOrder(WxUserOrder wxUserOrder);

    void delWxUserOrder(Long orderID);

    WxUserOrder queryById(Long orderId);

}
