package com.citybiz.system.service.biz.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import com.citybiz.common.enums.biz.EvaluateLevelEnum;
import com.citybiz.common.enums.biz.EvaluateTypeEnum;
import com.citybiz.common.exception.ServiceException;
import com.citybiz.system.domain.biz.BizEvaluate;
import com.citybiz.system.domain.biz.BizShop;
import com.citybiz.system.domain.biz.BizShopServer;
import com.citybiz.system.mapper.biz.BizEvaluateListMapper;
import com.citybiz.system.service.biz.IBizEvaluateListService;
import com.citybiz.system.service.biz.IBizShopServerService;
import com.citybiz.system.service.biz.IBizShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 评级列Service业务层处理
 * 
 * @author citybiz
 * @date 2023-10-16
 */
@Service
public class BizEvaluateListServiceImpl implements IBizEvaluateListService
{
    @Autowired
    private BizEvaluateListMapper bizEvaluateListMapper;

    @Autowired
    private IBizShopServerService bizShopServerService;

    @Autowired
    private IBizShopService bizShopService;

    /**
     * 查询评级列
     * 
     * @param id 评级列主键
     * @return 评级列
     */
    @Override
    public BizEvaluate selectBizEvaluateListById(Long id)
    {
        return bizEvaluateListMapper.selectBizEvaluateListById(id);
    }

    /**
     * 查询评级列列表
     * 
     * @param bizEvaluate 评级列
     * @return 评级列
     */
    @Override
    public List<BizEvaluate> selectBizEvaluateListList(BizEvaluate bizEvaluate)
    {
        return bizEvaluateListMapper.selectBizEvaluateListList(bizEvaluate);
    }

    /**
     * 新增评级列
     * 
     * @param bizEvaluate 评级列
     * @return 结果
     */
    @Override
    @Transactional
    public int insertBizEvaluateList(BizEvaluate bizEvaluate)
    {
        BizEvaluate newBizEvaluate = new BizEvaluate();
        newBizEvaluate.setEvaluateLevel(bizEvaluate.getEvaluateLevel());
        newBizEvaluate.setEvaluateType(bizEvaluate.getEvaluateType());
        newBizEvaluate.setEvaluateFraction(bizEvaluate.getEvaluateFraction());
        newBizEvaluate.setBizId(bizEvaluate.getBizId());
        newBizEvaluate.setShopEvaluaterId(bizEvaluate.getShopEvaluaterId());
        newBizEvaluate.setDelFlag(0L);
        return bizEvaluateListMapper.insertBizEvaluateList(newBizEvaluate);

    }

    /**
     * 修改评级列
     * 
     * @param bizEvaluate 评级列
     * @return 结果
     */
    @Override
    public int updateBizEvaluateList(BizEvaluate bizEvaluate)
    {
        return bizEvaluateListMapper.updateBizEvaluateList(bizEvaluate);
    }

    /**
     * 批量删除评级列
     * 
     * @param ids 需要删除的评级列主键
     * @return 结果
     */
    @Override
    public int deleteBizEvaluateListByIds(Long[] ids)
    {
        return bizEvaluateListMapper.deleteBizEvaluateListByIds(ids);
    }

    /**
     * 删除评级列信息
     * 
     * @param id 评级列主键
     * @return 结果
     */
    @Override
    public int deleteBizEvaluateListById(Long id)
    {
        return bizEvaluateListMapper.deleteBizEvaluateListById(id);
    }
}
