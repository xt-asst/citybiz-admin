package com.citybiz.system.service.biz.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.citybiz.common.constant.TaskConstant;
import com.citybiz.common.enums.biz.BizMessageTypeEnums;
import com.citybiz.common.enums.biz.BizTaskSortEnum;
import com.citybiz.common.enums.biz.BizTaskStatusEnum;
import com.citybiz.common.enums.biz.TenderTaskStatusEnum;
import com.citybiz.common.exception.ServiceException;
import com.citybiz.common.utils.AssertUtils;
import com.citybiz.system.domain.WxUser;
import com.citybiz.system.domain.biz.BizMessage;
import com.citybiz.system.domain.biz.BizTask;
import com.citybiz.system.domain.biz.BizTaskExtendInfo;
import com.citybiz.system.domain.biz.BizTaskManuscript;
import com.citybiz.system.domain.dto.BizTaskDto;
import com.citybiz.system.domain.dto.BizTaskExtendInfoDTO;
import com.citybiz.system.domain.dto.BizTaskManuscriptDto;
import com.citybiz.system.domain.process.req.MasterHandlerRequest;
import com.citybiz.system.domain.request.BizTaskRequest;
import com.citybiz.system.domain.vo.BizTaskManuscriptVo;
import com.citybiz.system.mapper.biz.BizTaskExtendInfoPlusMapper;
import com.citybiz.system.mapper.biz.BizTaskManuscriptMapper;
import com.citybiz.system.service.IWxUserService;
import com.citybiz.system.service.biz.IBizMessageService;
import com.citybiz.system.service.biz.IBizTaskManuscriptService;
import com.citybiz.system.service.biz.IBizTaskService;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 任务投标信息Service业务层处理
 *
 * @author citybiz
 * @date 2023-10-18
 */
@Service
@AllArgsConstructor
public class BizTaskManuscriptServiceImpl implements IBizTaskManuscriptService
{
    private BizTaskManuscriptMapper bizTaskManuscriptMapper;

    private IBizTaskService bizTaskService;

    private IBizMessageService bizMessageService;

    private BizTaskExtendInfoPlusMapper bizTaskExtendInfoPlusMapper;

    private IWxUserService wxUserService;
    /**
     * 查询任务投标信息
     *
     * @param id 任务投标信息主键
     * @return 任务投标信息
     */
    @Override
    public BizTaskManuscript selectBizTaskManuscriptById(Long id)
    {
        return bizTaskManuscriptMapper.selectBizTaskManuscriptById(id);
    }

    /**
     * 查询任务投标信息列表
     *
     * @param bizTaskManuscript 任务投标信息
     * @return 任务投标信息
     */
    @Override
    public List<BizTaskManuscriptDto> selectBizTaskManuscriptList(BizTaskManuscript bizTaskManuscript)
    {
        List<BizTaskManuscript> bizTaskManuscripts = bizTaskManuscriptMapper.selectBizTaskManuscriptList(bizTaskManuscript);
        return convert(bizTaskManuscripts);
    }

    /**
     * 新增任务投标信息
     *
     * @param bizTaskManuscript 任务投标信息
     * @return 结果
     */
    @Override
    public int insertBizTaskManuscript(BizTaskManuscript bizTaskManuscript)
    {
        bizTaskManuscript.setCreateTime(new Date());
        Long taskId = bizTaskManuscript.getTaskId();
        BizTaskDto bizTaskDto = bizTaskService.selectBizTaskById(taskId);
        if(bizTaskDto == null){
            throw new ServiceException("任务不存在，请确认");
        }
        if(new Date().after(bizTaskDto.getTaskEndTime())){
            throw new ServiceException("投稿时间已过，请确认");
        }
        if(!bizTaskDto.getTaskStatus().equals(BizTaskStatusEnum.TENDERING.getCode())){
            throw new ServiceException("非投标状态，请确认");
        }

        // 增加消息体
        String taskName = bizTaskDto.getTaskName();
        BizMessage message = new BizMessage();
        message.setCreateTime(new Date());
        message.setWxUserId(bizTaskDto.getEmployId());
        message.setMessage("你的任务:" + taskName + ", 有新的投稿，请查看。");
        message.setReadFlag(0L);
        message.setMsgType(BizMessageTypeEnums.TASK_MANUSCRIPT.getCode());
        bizMessageService.insertBizMessage(message);

        return bizTaskManuscriptMapper.insertBizTaskManuscript(bizTaskManuscript);
    }

    /**
     * 修改任务投标信息
     *
     * @param bizTaskManuscript 任务投标信息
     * @return 结果
     */
    @Override
    public int updateBizTaskManuscript(BizTaskManuscript bizTaskManuscript)
    {
        return bizTaskManuscriptMapper.updateBizTaskManuscript(bizTaskManuscript);
    }

    /**
     * 批量删除任务投标信息
     *
     * @param ids 需要删除的任务投标信息主键
     * @return 结果
     */
    @Override
    public int deleteBizTaskManuscriptByIds(Long[] ids)
    {
        return bizTaskManuscriptMapper.deleteBizTaskManuscriptByIds(ids);
    }

    /**
     * 删除任务投标信息信息
     *
     * @param id 任务投标信息主键
     * @return 结果
     */
    @Override
    public int deleteBizTaskManuscriptById(Long id)
    {
        return bizTaskManuscriptMapper.deleteBizTaskManuscriptById(id);
    }

    @Override
    @Transactional
    public void selectedTaskManuscript(Long taskManuscriptId) {
        BizTaskManuscript bizTaskManuscript = bizTaskManuscriptMapper.selectBizTaskManuscriptById(taskManuscriptId);
        if(bizTaskManuscript == null){
            throw new ServiceException("稿件信息不存在，请确认");
        }

        Long taskId = bizTaskManuscript.getTaskId();
        AssertUtils.isNotNull(taskId, "任务id不可为空");
        BizTaskDto bizTaskDto = bizTaskService.selectBizTaskById(taskId);
        if(!bizTaskDto.getTaskStatus().equals(BizTaskStatusEnum.TENDERING.getCode())){
            throw new ServiceException("非投标中状态，请确认");
        }
        BizTaskExtendInfoDTO bizTaskExtendInfoDTO = bizTaskDto.getBizTaskExtendInfoDTO();
        bizTaskExtendInfoDTO.setContext(TaskConstant.taskSelectManuscriptId, String.valueOf(taskManuscriptId));

        // 更新任务信息
        BizTaskRequest request = new BizTaskRequest();
        request.setTaskId(bizTaskDto.getId());
        request.setTaskStatus(BizTaskStatusEnum.WORKING.getCode());
        request.setTenderTaskStatus(TenderTaskStatusEnum.COUNSEL_PAY_PLAN.getCode());
        request.setTaskPrice(bizTaskManuscript.getQuote());
        request.setWorkerId(bizTaskManuscript.getWorkerId());
        WxUser wxUser = wxUserService.selectWxUserById(bizTaskManuscript.getWorkerId());
        request.setWorkName(wxUser.getUsername());

        // 更新任务扩展信息
        BizTaskExtendInfo bizTaskExtendInfo = new BizTaskExtendInfo();
        BeanUtils.copyProperties(bizTaskExtendInfoDTO, bizTaskExtendInfo);
        bizTaskExtendInfoPlusMapper.updateById(bizTaskExtendInfo);
        request.setTenderTaskStartTime(new Date());
        bizTaskService.updateBizTask(request);

    }

    public BizTaskManuscriptDto convert(BizTaskManuscript bizTaskManuscript){
        BizTaskManuscriptDto dto = new BizTaskManuscriptDto();
        if(bizTaskManuscript == null){
            return null;
        }

        BeanUtils.copyProperties(bizTaskManuscript, dto);
        return dto;
    }

    public List<BizTaskManuscriptDto> convert(List<BizTaskManuscript> bizTaskManuscripts){
        return bizTaskManuscripts.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    @Override
    public List<BizTaskManuscriptVo> convertVO(List<BizTaskManuscriptDto> bizTaskManuscriptDtos) {
        if(CollectionUtils.isEmpty(bizTaskManuscriptDtos)){
            return new ArrayList<>();
        }
        return bizTaskManuscriptDtos.stream()
                .map(this::convertVO)
                .collect(Collectors.toList());
    }

    @Override
    public BizTaskManuscriptVo convertVO(BizTaskManuscriptDto bizTaskManuscriptDto) {
        BizTaskManuscriptVo result = new BizTaskManuscriptVo();
        if(bizTaskManuscriptDto == null){
            return null;
        }
        BeanUtils.copyProperties(bizTaskManuscriptDto, result);
        Long workerId = bizTaskManuscriptDto.getWorkerId();
        WxUser wxUser = wxUserService.selectWxUserById(workerId);
        result.setWorkerName(wxUser.getUsername());
        result.setAvatar(wxUser.getAvatar());
        return result;
    }
}
