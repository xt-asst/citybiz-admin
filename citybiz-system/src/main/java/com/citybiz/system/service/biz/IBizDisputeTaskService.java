package com.citybiz.system.service.biz;

import com.citybiz.system.domain.biz.BizDisputeTask;

import java.util.List;

/**
 * 交易维权信息Service接口
 * 
 * @author citybiz
 * @date 2023-10-15
 */
public interface IBizDisputeTaskService 
{
    /**
     * 查询交易维权信息
     * 
     * @param id 交易维权信息主键
     * @return 交易维权信息
     */
    public BizDisputeTask selectBizDisputeTaskById(Long id);

    /**
     * 查询交易维权信息列表
     * 
     * @param bizDisputeTask 交易维权信息
     * @return 交易维权信息集合
     */
    public List<BizDisputeTask> selectBizDisputeTaskList(BizDisputeTask bizDisputeTask);

    /**
     * 新增交易维权信息
     * 
     * @param bizDisputeTask 交易维权信息
     * @return 结果
     */
    public int insertBizDisputeTask(BizDisputeTask bizDisputeTask);

    /**
     * 修改交易维权信息
     * 
     * @param bizDisputeTask 交易维权信息
     * @return 结果
     */
    public int updateBizDisputeTask(BizDisputeTask bizDisputeTask);

    /**
     * 批量删除交易维权信息
     * 
     * @param ids 需要删除的交易维权信息主键集合
     * @return 结果
     */
    public int deleteBizDisputeTaskByIds(Long[] ids);

    /**
     * 删除交易维权信息信息
     * 
     * @param id 交易维权信息主键
     * @return 结果
     */
    public int deleteBizDisputeTaskById(Long id);
}
