package com.citybiz.system.service.process.start;

/**
 * Create by Ykg on 2023/10/23 16:02
 * effect:
 */

public interface StartService <T>{

    /**
     * 流程发起
     */
    public void startProcess(T context);

}
