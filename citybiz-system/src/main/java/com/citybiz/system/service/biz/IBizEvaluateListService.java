package com.citybiz.system.service.biz;

import com.citybiz.system.domain.biz.BizEvaluate;

import java.util.List;

/**
 * 评级列Service接口
 * 
 * @author citybiz
 * @date 2023-10-16
 */
public interface IBizEvaluateListService 
{
    /**
     * 查询评级列
     * 
     * @param id 评级列主键
     * @return 评级列
     */
    public BizEvaluate selectBizEvaluateListById(Long id);

    /**
     * 查询评级列列表
     * 
     * @param bizEvaluate 评级列
     * @return 评级列集合
     */
    public List<BizEvaluate> selectBizEvaluateListList(BizEvaluate bizEvaluate);

    /**
     * 新增评级列
     * 
     * @param bizEvaluate 评级列
     * @return 结果
     */
    public int insertBizEvaluateList(BizEvaluate bizEvaluate);

    /**
     * 修改评级列
     * 
     * @param bizEvaluate 评级列
     * @return 结果
     */
    public int updateBizEvaluateList(BizEvaluate bizEvaluate);

    /**
     * 批量删除评级列
     * 
     * @param ids 需要删除的评级列主键集合
     * @return 结果
     */
    public int deleteBizEvaluateListByIds(Long[] ids);

    /**
     * 删除评级列信息
     * 
     * @param id 评级列主键
     * @return 结果
     */
    public int deleteBizEvaluateListById(Long id);
}
