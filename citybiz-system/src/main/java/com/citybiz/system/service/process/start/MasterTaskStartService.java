//package com.citybiz.system.service.process.start;
//
//import com.citybiz.common.enums.biz.BizTaskStatusEnum;
//import com.citybiz.common.exception.ServiceException;
//import com.citybiz.common.utils.AssertUtils;
//import com.citybiz.system.domain.biz.BizTask;
//import com.citybiz.system.domain.dto.BizTaskDto;
//import com.citybiz.system.domain.process.MasterTaskProcessContext;
//import com.citybiz.system.domain.process.ProcessContext;
//import com.citybiz.system.domain.request.BizTaskRequest;
//import com.citybiz.system.service.biz.IBizTaskService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * Create by Ykg on 2023/10/23 15:45
// * effect:
// */
//@Service
//@Slf4j
//public class MasterTaskStartService extends AbstractTaskService<BizTaskDto>{
//
//    private final static String bpmKey = "TaskMasterProcessV001";
//
//    @Autowired
//    private IBizTaskService bizTaskService;
//
//    @Override
//    protected void pre(ProcessContext<BizTaskDto> context) {
//        String bizNo = context.getBizNo();
//        Long taskId = Long.valueOf(bizNo);
//
//        BizTaskDto bizTaskDto = bizTaskService.selectBizTaskById(taskId);
//        if(context.getTaskInfo() == null) {
//            context.setTaskInfo(bizTaskDto);
//        }
//        Map<String, Object> variable = new HashMap<>();
//        if(bizTaskDto != null){
//            variable.put(ProcessContext.EMPLOY_ID, bizTaskDto.getEmployId());
//        }
//        context.setVariable(variable);
//    }
//
//    @Override
//    protected void preCheck(ProcessContext<BizTaskDto> context) {
//        BizTaskDto taskInfo = context.getTaskInfo();
//        if(taskInfo == null){
//            throw new ServiceException("MasterTaskStartService error, taskInfo is null!");
//        }
//
//        AssertUtils.isNotNull(taskInfo.getId(), "bizId con not be null");
//
//        Map<String, Object> variable = context.getVariable();
//        if(variable == null){
//            throw new ServiceException("variable is null");
//        }
//
//        Long employId = Long.valueOf(variable.get(ProcessContext.EMPLOY_ID).toString());
//        AssertUtils.isNotNull(employId, "雇主id不可为空");
//    }
//
//    @Override
//    protected void after(ProcessContext<BizTaskDto> context) {
//        BizTaskDto bizTaskDto = context.getTaskInfo();
//        BizTaskRequest request = new BizTaskRequest();
//        request.setTaskId(bizTaskDto.getId());
//        request.setTaskStatus(BizTaskStatusEnum.START.getCode());
//        bizTaskService.updateBizTask(request);
//    }
//
//    @Override
//    protected String getBpmKey() {
//        return bpmKey;
//    }
//
//    @Override
//    protected String getBizID(BizTaskDto taskInfo) {
//        return String.valueOf(taskInfo.getId());
//    }
//}
