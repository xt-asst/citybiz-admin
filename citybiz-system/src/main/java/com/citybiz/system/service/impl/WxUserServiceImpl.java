package com.citybiz.system.service.impl;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.citybiz.common.exception.WxBizException;
import com.citybiz.common.utils.AssertUtils;
import com.citybiz.common.utils.DateUtils;
import com.citybiz.common.utils.StringUtils;
import com.citybiz.system.mapper.WxUserPlusMapper;
import com.citybiz.system.service.biz.IBizAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.citybiz.system.mapper.WxUserMapper;
import com.citybiz.system.domain.WxUser;
import com.citybiz.system.service.IWxUserService;

/**
 * 微信用户管理Service业务层处理
 *
 * @author citybiz
 * @date 2023-10-01
 */
@Service
public class WxUserServiceImpl implements IWxUserService
{
    @Autowired
    private WxUserMapper wxUserMapper;

    @Autowired
    private WxUserPlusMapper wxUserPlusMapper;

    @Autowired
    private IBizAddressService bizAddressService;

    /**
     * 查询微信用户管理
     *
     * @param id 微信用户管理主键
     * @return 微信用户管理
     */
    @Override
    public WxUser selectWxUserById(Long id)
    {
        WxUser wxUser = wxUserMapper.selectWxUserById(id);
        if(wxUser != null) {
            if (wxUser.getAddress() != null) {
                String addressMsg = bizAddressService.getAddressMsg(wxUser.getAddress());
                wxUser.setAddressStr(addressMsg);
            }
        }
        return wxUser;
    }

    /**
     * 查询微信用户管理列表
     *
     * @param wxUser 微信用户管理
     * @return 微信用户管理
     */
    @Override
    public List<WxUser> selectWxUserList(WxUser wxUser)
    {
        List<WxUser> wxUsers = wxUserMapper.selectWxUserList(wxUser);
        for (WxUser user : wxUsers) {
            if(user.getAddress() != null){
                String addressMsg = bizAddressService.getAddressMsg(user.getAddress());
                user.setAddressStr(addressMsg);
            }
        }

        return wxUsers;
    }

    /**
     * 新增微信用户管理
     *
     * @param wxUser 微信用户管理
     * @return 结果
     */
    @Override
    public int insertWxUser(WxUser wxUser)
    {
        wxUser.setCreateTime(DateUtils.getNowDate());
        return wxUserMapper.insertWxUser(wxUser);
    }

    /**
     * 修改微信用户管理
     *
     * @param wxUser 微信用户管理
     * @return 结果
     */
    @Override
    public int updateWxUser(WxUser wxUser)
    {
        WxUser newWxUser = new WxUser();
        WxUser oldWxUser = wxUserPlusMapper.selectById(wxUser.getId());
        if(oldWxUser == null){
            throw new WxBizException("该用户不存在");
        }
        newWxUser.setId(wxUser.getId());
        if(wxUser.getShopFlag() != null){
            newWxUser.setShopFlag(wxUser.getShopFlag());
        }
        if(wxUser.getAddress() != null) {
            newWxUser.setAddress(wxUser.getAddress());
        }
        if(StringUtils.isNotEmpty(wxUser.getNickname())) {
            newWxUser.setNickname(wxUser.getNickname());
        }
        if(StringUtils.isNotEmpty(wxUser.getAvatar())) {
            newWxUser.setAvatar(wxUser.getAvatar());
        }
        if(wxUser.getCompanyAuth() != null){
            newWxUser.setCompanyAuth(wxUser.getCompanyAuth());
        }
        if(wxUser.getPrivateAuth() != null){
            newWxUser.setPrivateAuth(wxUser.getPrivateAuth());
        }

        if(StringUtils.isNotEmpty(wxUser.getPhone())){
            newWxUser.setPhone(wxUser.getPhone());
            // 标记用户初始化完成
            newWxUser.setInitUser(1);
        }

        if(StringUtils.isNotEmpty(wxUser.getUsername())){
            newWxUser.setUsername(wxUser.getUsername());
        }

        if(StringUtils.isNotEmpty(wxUser.getEmail())){
            newWxUser.setEmail(wxUser.getEmail());
        }

        newWxUser.setUpdateTime(new Date());
        int result = wxUserPlusMapper.updateById(newWxUser);
        WxUser user = wxUserPlusMapper.selectById(newWxUser.getId());
        if(StringUtils.isEmpty(user.getPhone())){
            user.setInitUser(0);
            wxUserPlusMapper.updateById(newWxUser);
        }
        return result;
    }

    /**
     * 批量删除微信用户管理
     *
     * @param ids 需要删除的微信用户管理主键
     * @return 结果
     */
    @Override
    public int deleteWxUserByIds(Long[] ids)
    {
        return wxUserMapper.deleteWxUserByIds(ids);
    }

    /**
     * 删除微信用户管理信息
     *
     * @param id 微信用户管理主键
     * @return 结果
     */
    @Override
    public int deleteWxUserById(Long id)
    {
        return wxUserMapper.deleteWxUserById(id);
    }

    @Override
    public WxUser getWxUserForOpenId(String openId) {
        LambdaQueryWrapper<WxUser> wrapper = new LambdaQueryWrapper<>();
        WxUser wxUser = wxUserPlusMapper.selectOne(wrapper.eq(WxUser::getOpenId, openId));
        if(wxUser == null){
            throw new WxBizException("该用户不存在");
        }
        return wxUser;
    }
}
