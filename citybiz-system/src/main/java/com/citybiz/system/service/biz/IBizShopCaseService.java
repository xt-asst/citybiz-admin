package com.citybiz.system.service.biz;

import com.citybiz.system.domain.biz.BizShopCase;

import java.util.List;

/**
 * 店铺案例Service接口
 * 
 * @author citybiz
 * @date 2023-10-18
 */
public interface IBizShopCaseService 
{
    /**
     * 查询店铺案例
     * 
     * @param id 店铺案例主键
     * @return 店铺案例
     */
    public BizShopCase selectBizShopCaseById(Long id);

    /**
     * 查询店铺案例列表
     * 
     * @param bizShopCase 店铺案例
     * @return 店铺案例集合
     */
    public List<BizShopCase> selectBizShopCaseList(BizShopCase bizShopCase);

    /**
     * 新增店铺案例
     * 
     * @param bizShopCase 店铺案例
     * @return 结果
     */
    public int insertBizShopCase(BizShopCase bizShopCase);

    /**
     * 修改店铺案例
     * 
     * @param bizShopCase 店铺案例
     * @return 结果
     */
    public int updateBizShopCase(BizShopCase bizShopCase);

    /**
     * 批量删除店铺案例
     * 
     * @param ids 需要删除的店铺案例主键集合
     * @return 结果
     */
    public int deleteBizShopCaseByIds(Long[] ids);

    /**
     * 删除店铺案例信息
     * 
     * @param id 店铺案例主键
     * @return 结果
     */
    public int deleteBizShopCaseById(Long id);
}
