package com.citybiz.system.service.biz.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.citybiz.common.utils.DateUtils;
import com.citybiz.system.domain.biz.BizBanner;
import com.citybiz.system.mapper.biz.BizBannerMapper;
import com.citybiz.system.mapper.biz.BizBannerPlusMapper;
import com.citybiz.system.service.biz.IBizBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.stereotype.Service;

/**
 * banner轮播图列Service业务层处理
 * 
 * @author ykg
 * @date 2023-09-29
 */
@Service
public class BizBannerServiceImpl implements IBizBannerService
{
    @Autowired
    private BizBannerMapper bizBannerMapper;

    @Autowired
    private BizBannerPlusMapper bizBannerPlusMapper;

    /**
     * 查询banner轮播图列
     * 
     * @param id banner轮播图列主键
     * @return banner轮播图列
     */
    @Override
    public BizBanner selectBizBannerById(Long id)
    {
        return bizBannerMapper.selectBizBannerById(id);
    }

    /**
     * 查询banner轮播图列列表
     * 
     * @param bizBanner banner轮播图列
     * @return banner轮播图列
     */
    @Override
    public List<BizBanner> selectBizBannerList(BizBanner bizBanner)
    {
        return bizBannerMapper.selectBizBannerList(bizBanner);
    }

    /**
     * 新增banner轮播图列
     * 
     * @param bizBanner banner轮播图列
     * @return 结果
     */
    @Override
    public int insertBizBanner(BizBanner bizBanner)
    {
        bizBanner.setDelFlag(0L);
        bizBanner.setCreateTime(DateUtils.getNowDate());
        return bizBannerMapper.insertBizBanner(bizBanner);
    }

    /**
     * 修改banner轮播图列
     * 
     * @param bizBanner banner轮播图列
     * @return 结果
     */
    @Override
    public int updateBizBanner(BizBanner bizBanner)
    {
        return bizBannerMapper.updateBizBanner(bizBanner);
    }

    /**
     * 批量删除banner轮播图列
     * 
     * @param ids 需要删除的banner轮播图列主键
     * @return 结果
     */
    @Override
    public int deleteBizBannerByIds(Long[] ids)
    {
        return bizBannerMapper.deleteBizBannerByIds(ids);
    }

    /**
     * 删除banner轮播图列信息
     * 
     * @param id banner轮播图列主键
     * @return 结果
     */
    @Override
    public int deleteBizBannerById(Long id)
    {
        return bizBannerMapper.deleteBizBannerById(id);
    }

    @Override
    public List<BizBanner> getHomeBannerList(int count) {
        if(count <= 0){
            count = 3;
        }

        LambdaQueryWrapper<BizBanner> queryWrapper = Wrappers.lambdaQuery(BizBanner.class);
        queryWrapper.eq(BizBanner::getDelFlag, 0)
                .orderByAsc(BizBanner::getRank)
                .last("LIMIT " + count);
        return bizBannerPlusMapper.selectList(queryWrapper);
    }
}
