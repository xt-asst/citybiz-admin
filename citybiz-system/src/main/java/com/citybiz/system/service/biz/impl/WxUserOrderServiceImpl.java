package com.citybiz.system.service.biz.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.citybiz.common.enums.biz.ServerOrderStatusEnum;
import com.citybiz.common.utils.StringUtils;
import com.citybiz.system.domain.biz.WxUserOrder;
import com.citybiz.system.mapper.biz.WxUserOrderPlusMapper;
import com.citybiz.system.service.biz.IWxUserOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Create by Ykg on 2023/10/18 23:16
 * effect: 微信订单服务类实现类
 */
@Service
@Slf4j
public class WxUserOrderServiceImpl implements IWxUserOrderService {

    @Autowired
    private WxUserOrderPlusMapper wxUserOrderPlusMapper;

    @Override
    public List<WxUserOrder> queryList(WxUserOrder wxUserOrder) {
        LambdaQueryWrapper<WxUserOrder> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(WxUserOrder::getDelFlag, 0L);
        wrapper.eq(WxUserOrder::getWxUserId, wxUserOrder.getWxUserId());
        if(StringUtils.isNotEmpty(wxUserOrder.getOrderStatus())){
            wrapper.eq(WxUserOrder::getOrderStatus, wxUserOrder.getOrderStatus());
        }
        return wxUserOrderPlusMapper.selectList(wrapper);
    }

    @Override
    public void insertWxUserOrder(WxUserOrder wxUserOrder) {
        WxUserOrder newOrder = new WxUserOrder();
        newOrder.setCreateTime(new Date());
        newOrder.setOrderStatus(ServerOrderStatusEnum.PENDING_PAYMENT.getCode());
        newOrder.setPrice(wxUserOrder.getPrice());
        newOrder.setServerType(wxUserOrder.getServerType());
        if(wxUserOrder.getUnitPrice() != null){
            newOrder.setUnitPrice(wxUserOrder.getUnitPrice());
        }

        if(wxUserOrder.getNum() != null){
            newOrder.setNum(wxUserOrder.getNum());
        }
        newOrder.setWxUserId(wxUserOrder.getWxUserId());
        newOrder.setShopBizServerId(wxUserOrder.getShopBizServerId());
        newOrder.setDelFlag(0L);
        wxUserOrderPlusMapper.insert(newOrder);
    }

    @Override
    public void updateWxUserOrder(WxUserOrder wxUserOrder) {
        WxUserOrder upOrder = new WxUserOrder();
        upOrder.setId(wxUserOrder.getId());
        upOrder.setOrderStatus(wxUserOrder.getOrderStatus());
        wxUserOrderPlusMapper.updateById(upOrder);
    }

    @Override
    public void delWxUserOrder(Long orderID) {
        WxUserOrder upOrder = new WxUserOrder();
        upOrder.setId(orderID);
        upOrder.setDelFlag(1L);
        wxUserOrderPlusMapper.updateById(upOrder);
    }

    @Override
    public WxUserOrder queryById(Long orderId) {
        return wxUserOrderPlusMapper.selectById(orderId);
    }
}
