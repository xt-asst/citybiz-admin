package com.citybiz.system.service;

import com.citybiz.system.domain.WxUserRole;

/**
 * Create by Ykg on 2023/10/3 10:57
 * effect: 微信角色服务类
 */
public interface IWxUserRoleService {

    /**
     * 根据微信用户id与角色获取角色信息
     * @param wxUserId 微信用户id
     * @param role 角色类型
     * @return 用户角色信息
     */
    WxUserRole getWxUserRoleForWxUserId(Long wxUserId, Integer role);

}
