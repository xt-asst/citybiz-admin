package com.citybiz.system.service.biz.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.citybiz.common.constant.Constants;
import com.citybiz.common.exception.WxBizException;
import com.citybiz.common.utils.StringUtils;
import com.citybiz.system.domain.WxUser;
import com.citybiz.system.domain.biz.BizWxCompanyApproveInfo;
import com.citybiz.system.domain.biz.BizWxPrivateApproveInfo;
import com.citybiz.system.mapper.biz.BizWxCompanyApproveInfoMapper;
import com.citybiz.system.mapper.biz.BizWxCompanyApproveInfoPlusMapper;
import com.citybiz.system.service.IWxUserService;
import com.citybiz.system.service.biz.IBizAddressService;
import com.citybiz.system.service.biz.IBizWxCompanyApproveInfoService;
import com.citybiz.wxcommon.enums.ApproveStatusEnums;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 公司认证审核Service业务层处理
 * 
 * @author citybiz
 * @date 2023-10-04
 */
@Service
public class BizWxCompanyApproveInfoServiceImpl implements IBizWxCompanyApproveInfoService
{
    @Autowired
    private BizWxCompanyApproveInfoMapper bizWxCompanyApproveInfoMapper;

    @Autowired
    private BizWxCompanyApproveInfoPlusMapper  bizWxCompanyApproveInfoPlusMapper;

    @Autowired
    private IWxUserService wxUserService;

    @Autowired
    private IBizAddressService bizAddressService;

    /**
     * 查询公司认证审核
     * 
     * @param id 公司认证审核主键
     * @return 公司认证审核
     */
    @Override
    public BizWxCompanyApproveInfo selectBizWxCompanyApproveInfoById(Long id)
    {
        BizWxCompanyApproveInfo bizWxCompanyApproveInfo = bizWxCompanyApproveInfoMapper.selectBizWxCompanyApproveInfoById(id);
        if(bizWxCompanyApproveInfo.getAddressId() != null){
            String addressMsg = bizAddressService.getAddressMsg(bizWxCompanyApproveInfo.getAddressId());
            bizWxCompanyApproveInfo.setAddressStr(addressMsg);
        }
        return bizWxCompanyApproveInfo;
    }

    /**
     * 查询公司认证审核列表
     * 
     * @param bizWxCompanyApproveInfo 公司认证审核
     * @return 公司认证审核
     */
    @Override
    public List<BizWxCompanyApproveInfo> selectBizWxCompanyApproveInfoList(BizWxCompanyApproveInfo bizWxCompanyApproveInfo)
    {
        bizWxCompanyApproveInfo.setDelFlag(0l);
        List<BizWxCompanyApproveInfo> bizWxCompanyApproveInfos = bizWxCompanyApproveInfoMapper.selectBizWxCompanyApproveInfoList(bizWxCompanyApproveInfo);
        for (BizWxCompanyApproveInfo wxCompanyApproveInfo : bizWxCompanyApproveInfos) {
            if(wxCompanyApproveInfo.getAddressId() != null){
                String addressMsg = bizAddressService.getAddressMsg(wxCompanyApproveInfo.getAddressId());
                wxCompanyApproveInfo.setAddressStr(addressMsg);
            }
        }

        return bizWxCompanyApproveInfos;
    }

    /**
     * 新增公司认证审核
     * 
     * @param bizWxCompanyApproveInfo 公司认证审核
     * @return 结果
     */
    @Override
    public int insertBizWxCompanyApproveInfo(BizWxCompanyApproveInfo bizWxCompanyApproveInfo)
    {
        BizWxCompanyApproveInfo newInfo = new BizWxCompanyApproveInfo();
        newInfo.setWxUserId(bizWxCompanyApproveInfo.getWxUserId());
        newInfo.setCompanyName(bizWxCompanyApproveInfo.getCompanyName());
        newInfo.setWxUserId(bizWxCompanyApproveInfo.getWxUserId());
        newInfo.setBizTypeId(bizWxCompanyApproveInfo.getBizTypeId());
        newInfo.setEmployeesNum(bizWxCompanyApproveInfo.getEmployeesNum());
        newInfo.setUnifiedSocialCreditCode(bizWxCompanyApproveInfo.getUnifiedSocialCreditCode());
        newInfo.setCompanyCreateTime(bizWxCompanyApproveInfo.getCompanyCreateTime());
        newInfo.setCompanyWebsite(bizWxCompanyApproveInfo.getCompanyWebsite());
        newInfo.setAddressId(bizWxCompanyApproveInfo.getAddressId());
        newInfo.setAddressDetail(bizWxCompanyApproveInfo.getAddressDetail());
        newInfo.setPassFlag(ApproveStatusEnums.NOT_CHECK.getCode());
        newInfo.setProveImages(bizWxCompanyApproveInfo.getProveImages());
        newInfo.setDelFlag(0l);
        return bizWxCompanyApproveInfoPlusMapper.insert(newInfo);
    }

    /**
     * 修改公司认证审核
     * 
     * @param bizWxCompanyApproveInfo 公司认证审核
     * @return 结果
     */
    @Override
    public int updateBizWxCompanyApproveInfo(BizWxCompanyApproveInfo bizWxCompanyApproveInfo)
    {
        BizWxCompanyApproveInfo newInfo = new BizWxCompanyApproveInfo();
        newInfo.setId(bizWxCompanyApproveInfo.getId());
        if(StringUtils.isNotEmpty(bizWxCompanyApproveInfo.getCompanyName())) {
            newInfo.setCompanyName(bizWxCompanyApproveInfo.getCompanyName());
        }
        if(null != bizWxCompanyApproveInfo.getWxUserId()) {
            newInfo.setWxUserId(bizWxCompanyApproveInfo.getWxUserId());
        }
        if(null != bizWxCompanyApproveInfo.getBizTypeId()) {
            newInfo.setBizTypeId(bizWxCompanyApproveInfo.getBizTypeId());
        }
        if(null != bizWxCompanyApproveInfo.getEmployeesNum()) {
            newInfo.setEmployeesNum(bizWxCompanyApproveInfo.getEmployeesNum());
        }
        if(StringUtils.isNotEmpty(bizWxCompanyApproveInfo.getUnifiedSocialCreditCode())) {
            newInfo.setUnifiedSocialCreditCode(bizWxCompanyApproveInfo.getUnifiedSocialCreditCode());
        }
        if(null != bizWxCompanyApproveInfo.getCompanyCreateTime()) {
            newInfo.setCompanyCreateTime(bizWxCompanyApproveInfo.getCompanyCreateTime());
        }
        if(StringUtils.isNotEmpty(bizWxCompanyApproveInfo.getCompanyWebsite())) {
            newInfo.setCompanyWebsite(bizWxCompanyApproveInfo.getCompanyWebsite());
        }
        if(null != bizWxCompanyApproveInfo.getAddressId()) {
            newInfo.setAddressId(bizWxCompanyApproveInfo.getAddressId());
        }
        if(StringUtils.isNotEmpty(bizWxCompanyApproveInfo.getAddressDetail())) {
            newInfo.setAddressDetail(bizWxCompanyApproveInfo.getAddressDetail());
        }

        newInfo.setPassFlag(ApproveStatusEnums.NOT_CHECK.getCode());

        if(StringUtils.isNotEmpty(bizWxCompanyApproveInfo.getProveImages())) {
            newInfo.setProveImages(bizWxCompanyApproveInfo.getProveImages());
        }
        return bizWxCompanyApproveInfoPlusMapper.updateById(newInfo);
    }

    /**
     * 批量删除公司认证审核
     * 
     * @param ids 需要删除的公司认证审核主键
     * @return 结果
     */
    @Override
    public int deleteBizWxCompanyApproveInfoByIds(Long[] ids)
    {
        return bizWxCompanyApproveInfoMapper.deleteBizWxCompanyApproveInfoByIds(ids);
    }

    /**
     * 删除公司认证审核信息
     * 
     * @param id 公司认证审核主键
     * @return 结果
     */
    @Override
    public int deleteBizWxCompanyApproveInfoById(Long id)
    {
        return bizWxCompanyApproveInfoMapper.deleteBizWxCompanyApproveInfoById(id);
    }

    @Override
    public BizWxCompanyApproveInfo selectByUserId(Long wxUserId) {
        LambdaQueryWrapper<BizWxCompanyApproveInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BizWxCompanyApproveInfo::getWxUserId, wxUserId);
        wrapper.eq(BizWxCompanyApproveInfo::getDelFlag, 0l);
        BizWxCompanyApproveInfo bizWxCompanyApproveInfo = bizWxCompanyApproveInfoPlusMapper.selectOne(wrapper);
        if(bizWxCompanyApproveInfo != null){
            if(bizWxCompanyApproveInfo.getAddressId() != null){
                String addressMsg = bizAddressService.getAddressMsg(bizWxCompanyApproveInfo.getAddressId());
                bizWxCompanyApproveInfo.setAddressStr(addressMsg);
            }
        }
        return bizWxCompanyApproveInfo;
    }

    @Override
    @Transactional
    public void checkApproveInfo(Long id, Long status) {
        BizWxCompanyApproveInfo bizWxCompanyApproveInfo = bizWxCompanyApproveInfoPlusMapper.selectById(id);
        if(bizWxCompanyApproveInfo == null){
            throw new WxBizException("该认证信息不存在");
        }
        Long wxUserId = bizWxCompanyApproveInfo.getWxUserId();
        WxUser wxUser = wxUserService.selectWxUserById(wxUserId);
        // 认证成功
        if(ApproveStatusEnums.CHECK_PASS.getCode().equals(status)) {
            wxUser.setCompanyAuth(ApproveStatusEnums.CHECK_PASS.getCode());
        }else {
            wxUser.setCompanyAuth(ApproveStatusEnums.CHECK_NOT_PASS.getCode());
        }
        wxUserService.updateWxUser(wxUser);
        bizWxCompanyApproveInfo.setPassFlag(status);
        bizWxCompanyApproveInfoPlusMapper.updateById(bizWxCompanyApproveInfo);
    }

    @Override
    public BizWxCompanyApproveInfo queryApproveForUserId(Long userId) {
        LambdaQueryWrapper<BizWxCompanyApproveInfo> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BizWxCompanyApproveInfo::getWxUserId, userId);
        wrapper.eq(BizWxCompanyApproveInfo::getDelFlag, Constants.EXIST_VALUE);
        return bizWxCompanyApproveInfoPlusMapper.selectOne(wrapper);
    }
}
