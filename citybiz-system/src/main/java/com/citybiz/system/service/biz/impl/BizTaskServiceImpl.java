package com.citybiz.system.service.biz.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.citybiz.common.core.page.BizTableDataInfo;
import com.citybiz.common.core.service.BaseService;
import com.citybiz.common.enums.biz.BizTaskSortEnum;
import com.citybiz.common.enums.biz.BizTaskStatusEnum;
import com.citybiz.common.exception.ServiceException;
import com.citybiz.common.utils.DateUtils;
import com.citybiz.common.utils.StringUtils;
import com.citybiz.system.domain.WxUser;
import com.citybiz.system.domain.biz.*;
import com.citybiz.system.domain.dto.BizTaskConsignDTO;
import com.citybiz.system.domain.dto.BizTaskDto;
import com.citybiz.system.domain.dto.BizTaskExtendInfoDTO;
import com.citybiz.system.domain.dto.BizTaskManuscriptDto;
import com.citybiz.system.domain.request.BizTaskRequest;
import com.citybiz.system.mapper.biz.*;
import com.citybiz.system.service.IWxUserService;
import com.citybiz.system.service.biz.IBizTaskService;
import com.citybiz.system.service.biz.IBizTaskTypeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 任务格Service业务层处理
 *
 * @author citybiz
 * @date 2023-10-01
 */
@Service
@Slf4j
@AllArgsConstructor
public class BizTaskServiceImpl extends BaseService implements IBizTaskService
{
    private BizTaskMapper bizTaskMapper;

    private BizTaskPlusMapper bizTaskPlusMapper;

    private IBizTaskTypeService bizTaskTypeService;

    private BizTaskManuscriptPlusMapper bizTaskManuscriptPlusMapper;

    private BizTaskExtendInfoPlusMapper bizTaskExtendInfoPlusMapper;

    private IWxUserService wxUserService;

    private BizTaskConsignMapper bizTaskConsignMapper;

    /**
     * 查询任务格
     *
     * @param id 任务格主键
     * @return 任务格
     */
    @Override
    public BizTaskDto selectBizTaskById(Long id)
    {
        BizTask bizTask = bizTaskMapper.selectBizTaskById(id);
        return convert(bizTask);
    }

    public static void main(String[] args) {
        String str = "企业信息,车辆信息,注册用户信息,从业人员信息,电子围栏,电子围栏,电子围栏,营运数据,定位信息,从业人员信息,企业信息,车辆信息,车辆设备信息,车辆监控信息,营运数据,定位信息,营运数据,冶超站点信息,企业信息,冶超站点信息,执法设备车辆,执法设备设备,电子围栏,执法人员信息,执法设备车辆,执法设备,企业信息,治超称重信息,治超站点信息,治超称重信息,高速收费站,高速超速记录信息,高速出入异常信息,违法案件信息,失信车辆信息,失信驾驶员信息,失信企业信息,企业信息,车辆信息,定位信息,营运数据,企业信息,车辆信息,定位信息,营运数据,企业信息,车辆信息,定位信息,营运数据,事件信息,车辆信息,车辆监控数据,车辆信息,监控历史文件,车辆信息,车辆报警信息,企业信息,企业信息,企业信息,企业证件信息,从业人员信息,从业人员资格证信息,车辆信息,车辆设备信息,车辆证件信息,定位信息,电子围栏,电子运单数据,车辆报警信息,车辆违法案件信息,场站监控视频,场站视频信息,线路信息,班次信息,车辆信息,定位信息,企业信息,营运数据,场站信息,车辆违法案件信息,车辆报警信息,线路信息,企业信息,车辆信息,从业人员信息,营运信息,班次信息,线路信息,班次信息,车辆定位,车辆报警信息,营运信息,定位信息,企业信息,企业信息,企业信息,企业证件信息,从业人员信息,从业人员资格证信息,依靠各个公司的从业记录,车辆信息,车辆证件信息,定位信息,车辆报警信息,车辆案件信息,车辆监控数据,路线数据,路段数据（包含在路线内吗）,桥梁数据,隧道数据,高速收费站数据,高速服务区数据,涵洞数据,互通立交基础数据,停车区数据,交调站数据,监控视频数据,路网事件信息,路网预警信息,高速ETC门架信息,高速过车数据,高速过车数据,交调站数据,交调站过车等数据,监控视频信息,道路养护计划,线路管制数据,企业信息,公交线路,公交站点,公交营运日志信息,场站信息,车辆信息,从业人员信息,视频信息,充电设施信息,公交排班表,公交计划表,公交发车记录,定位信息,站点日志,公交营运订单,客运线路信息,企业信息,车辆信息,从业人员信息,客运订单信息,定位信息,车辆告警信息,车辆案件信息,客运订单信息,客运排班表,企业信息,车辆信息,从业人员信息,网约车订单信息,企业资格证,网约车车辆许可证,网约车从业人员资格证,定位信息,网约车订单信息,驾校企业信息,车辆基础信息,从业人员信息,订单信息,学习计划表等,学员考试信息,企业信息,维修记录,维修评价";
        String[] split = str.split(",");
        Set<String> set = new HashSet<>();
        for (String s : split) {
            set.add(s);
        }
        for (String s : set) {
            System.out.println(s);
        }
    }

    @Override
    public BizTableDataInfo<BizTaskDto> indexTask(BizTaskRequest request) {
        LambdaQueryWrapper<BizTask> wrapper = new LambdaQueryWrapper<>();
        if(request.getTaskTypeId() != null) {
            wrapper.eq(BizTask::getTaskTypeId, request.getTaskTypeId());
        }
        if(request.getCreateTimeDesc() != null && request.getCreateTimeDesc() == 1){
            wrapper.orderByDesc(BizTask::getCreateTime);
        }

        if(request.getCreateTimeDesc() != null && request.getCreateTimeDesc() == 0){
            wrapper.orderByAsc(BizTask::getCreateTime);
        }

        if(request.getManuscriptCountDesc() != null &&request.getManuscriptCountDesc() == 1){
            wrapper.orderByDesc(BizTask::getManuscriptCount);
        }

        if(request.getManuscriptCountDesc() != null &&request.getManuscriptCountDesc() == 0){
            wrapper.orderByAsc(BizTask::getManuscriptCount);
        }
        startPage();
        List<BizTask> bizTasks = bizTaskPlusMapper.selectList(wrapper);
        BizTableDataInfo<BizTask> dataTable = getDataTable(bizTasks);
        List<BizTask> rows = dataTable.getRows();
        BizTableDataInfo<BizTaskDto> result = new BizTableDataInfo<>();
        BeanUtils.copyProperties(dataTable, result);
        result.setRows(convertBizTaskList(rows));
        return result;
    }

    @Override
    public BizTableDataInfo<BizTaskDto> selectBizTaskListByPage(BizTaskRequest request) {
        BizTask bizTask = new BizTask();
        BeanUtils.copyProperties(request, bizTask);
        bizTask.setDelFlag(0L);
        startPage();
        List<BizTask> bizTasks = bizTaskMapper.selectBizTaskList(bizTask);
        BizTableDataInfo<BizTask> dataTable = getDataTable(bizTasks);
        List<BizTask> rows = dataTable.getRows();
        BizTableDataInfo<BizTaskDto> result = new BizTableDataInfo<>();
        BeanUtils.copyProperties(dataTable, result);
        result.setRows(convertBizTaskList(rows));
        return result;
    }

    /**
     * 查询任务格列表
     *
     * @param request 任务查询请求体
     * @return 任务格
     */
    @Override
    public List<BizTaskDto> selectBizTaskList(BizTaskRequest request)
    {
        BizTask bizTask = new BizTask();
        BeanUtils.copyProperties(request, bizTask);
        bizTask.setDelFlag(0L);
        List<BizTask> bizTasks = bizTaskMapper.selectBizTaskList(bizTask);
        return convertBizTaskList(bizTasks);
    }

    @Override
    public BizTableDataInfo<BizTaskDto> selectBizTaskListByWorker(Long workerId,String taskSort) {
        startPage();
        List<BizTask> bizTasks = bizTaskMapper.selectBizTaskListByWorker(workerId);
        BizTableDataInfo<BizTask> dataTable = getDataTable(bizTasks);
        List<BizTask> rows = dataTable.getRows();
        BizTableDataInfo<BizTaskDto> result = new BizTableDataInfo<>();
        BeanUtils.copyProperties(dataTable, result);
        result.setRows(convertBizTaskList(rows));
        return result;
    }

    /**
     * 新增任务列表
     *
     * @param request 任务请求信息
     * @return 结果
     */
    @Override
    @Transactional
    public int insertBizTask(BizTaskRequest request)
    {
        BizTask insert = new BizTask();
        insert.setTaskStatus(BizTaskStatusEnum.START.getCode());
        WxUser wxUser = wxUserService.selectWxUserById(request.getEmployId());
        // 不同的任务走不通的状态
        if(request.getTaskSort().equals(BizTaskSortEnum.BOUNTY.getCode())){
            insert.setTaskStatus(BizTaskStatusEnum.SUBMITTING_WORK.getCode());
        }else if(request.getTaskSort().equals(BizTaskSortEnum.TENDER.getCode())){
            insert.setTaskStatus(BizTaskStatusEnum.TENDERING.getCode());
        }else {
            throw new ServiceException("任务类型错误，请确认");
        }

        insert.setCreateTime(DateUtils.getNowDate());
        insert.setUpdateTime(DateUtils.getNowDate());
        insert.setTaskDetail(request.getTaskDetail());
        insert.setTaskName(request.getTaskName());
        insert.setTaskSort(request.getTaskSort());
        insert.setTaskStartTime(request.getTaskStartTime());
        insert.setTaskEndTime(request.getTaskEndTime());
        insert.setTenderTaskStartTime(DateUtils.getNowDate());
        insert.setTaskTypeId(request.getTaskTypeId());
        insert.setEmployName(wxUser.getUsername());
        insert.setEmployId(request.getEmployId());
        insert.setTaskPhone(request.getTaskPhone());

        int result = bizTaskPlusMapper.insert(insert);
        // 增加扩展信息
        BizTaskExtendInfo bizTaskExtendInfo = new BizTaskExtendInfo();
        bizTaskExtendInfo.setTaskId(insert.getId());
        bizTaskExtendInfo.setBudget(request.getTaskPrice());
        bizTaskExtendInfo.setIndustryId(request.getTaskTypeId());
        bizTaskExtendInfo.setEmployerPhone(wxUser.getPhone());
        bizTaskExtendInfo.setAddressId(request.getAddressId());
        bizTaskExtendInfoPlusMapper.insert(bizTaskExtendInfo);
        return result;
    }



    /**
     * 修改任务格
     *
     * @param request 任务请求信息
     * @return 结果
     */
    @Override
    public int updateBizTask(BizTaskRequest request)
    {
        BizTask update = new BizTask();
        update.setId(request.getTaskId());
        BeanUtils.copyProperties(request, update);
        update.setUpdateTime(DateUtils.getNowDate());
        return bizTaskMapper.updateBizTask(update);
    }

    /**
     * 批量删除任务格
     *
     * @param ids 需要删除的任务格主键
     * @return 结果
     */
    @Override
    public int deleteBizTaskByIds(Long[] ids)
    {
        return bizTaskMapper.deleteBizTaskByIds(ids);
    }

    /**
     * 删除任务格信息
     *
     * @param id 任务格主键
     * @return 结果
     */
    @Override
    public int deleteBizTaskById(Long id)
    {
        return bizTaskMapper.deleteBizTaskById(id);
    }

    private BizTaskDto convert(BizTask bizTask){
        if(bizTask == null){
            return null;
        }
        BizTaskDto bizTaskDto = new BizTaskDto();
        BeanUtils.copyProperties(bizTask, bizTaskDto);
        LambdaQueryWrapper<BizTaskManuscript> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BizTaskManuscript::getTaskId, bizTask.getId());
        if(bizTask.getTaskTypeId() != null){
            BizTaskType bizTaskType = bizTaskTypeService.selectBizTaskTypeById(bizTask.getTaskTypeId());
            bizTaskDto.setBizTaskType(bizTaskType);
        }
        BizTaskExtendInfoDTO taskExtend = getTaskExtend(bizTask.getId());
        List<BizTaskManuscript> bizTaskManuscripts = bizTaskManuscriptPlusMapper.selectList(wrapper);
        bizTaskDto.setBizTaskManuscriptDtoList(convertBizTaskManuscriptList(bizTaskManuscripts));
        bizTaskDto.setBizTaskExtendInfoDTO(taskExtend);

        LambdaQueryWrapper<BizTaskConsign> bizTaskConsignLambdaQueryWrapper = new LambdaQueryWrapper<>();
        bizTaskConsignLambdaQueryWrapper.eq(BizTaskConsign::getTaskId, bizTask.getId());
        BizTaskConsign bizTaskConsign = bizTaskConsignMapper.selectOne(bizTaskConsignLambdaQueryWrapper);
        if(bizTaskConsign != null) {
            BizTaskConsignDTO bizTaskConsignDTO = new BizTaskConsignDTO();
            BeanUtils.copyProperties(bizTaskConsignDTO, bizTaskConsign);
            bizTaskDto.setBizTaskConsignDTO(bizTaskConsignDTO);
        }
        return bizTaskDto;
    }

    private BizTaskManuscriptDto convert(BizTaskManuscript bizTaskManuscript){
        BizTaskManuscriptDto result = new BizTaskManuscriptDto();
        BeanUtils.copyProperties(bizTaskManuscript, result);

        return result;
    }

    @Override
    public BizTaskExtendInfoDTO getTaskExtend(Long taskId) {
        LambdaQueryWrapper<BizTaskExtendInfo> extendInfoLambdaQueryWrapper = new LambdaQueryWrapper<>();
        extendInfoLambdaQueryWrapper.eq(BizTaskExtendInfo::getTaskId, taskId);
        BizTaskExtendInfo bizTaskExtendInfo = bizTaskExtendInfoPlusMapper.selectOne(extendInfoLambdaQueryWrapper);
        BizTaskExtendInfoDTO bizTaskExtendInfoDTO = new BizTaskExtendInfoDTO();
        BeanUtils.copyProperties(bizTaskExtendInfo,bizTaskExtendInfoDTO);
        String feature = bizTaskExtendInfo.getFeature();
        Map<String, String> featureContext;
        if(StringUtils.isEmpty(feature)){
            featureContext = new HashMap<>();
        }else {
            featureContext = JSON.parseObject(feature, new TypeReference<Map<String, String>>() {
            });
        }
        bizTaskExtendInfoDTO.setFeatureContext(featureContext);
        return bizTaskExtendInfoDTO;
    }

    @Override
    public void updateTaskExtend(BizTaskExtendInfoDTO bizTaskExtendInfoDTO) {
        BizTaskExtendInfo update = new BizTaskExtendInfo();
        BeanUtils.copyProperties(bizTaskExtendInfoDTO, update);
        bizTaskExtendInfoPlusMapper.updateById(update);
    }

    private List<BizTaskDto> convertBizTaskList(List<BizTask> bizTaskList){
        if(CollectionUtils.isEmpty(bizTaskList)){
            return new ArrayList<>();
        }
        return bizTaskList.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    private List<BizTaskManuscriptDto> convertBizTaskManuscriptList(List<BizTaskManuscript> bizTaskManuscriptList){
        if(CollectionUtils.isEmpty(bizTaskManuscriptList)){
            return new ArrayList<>();
        }
        return bizTaskManuscriptList.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }
}
