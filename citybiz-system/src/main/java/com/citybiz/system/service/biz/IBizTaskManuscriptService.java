package com.citybiz.system.service.biz;

import com.citybiz.system.domain.biz.BizTaskManuscript;
import com.citybiz.system.domain.dto.BizTaskManuscriptDto;
import com.citybiz.system.domain.vo.BizTaskManuscriptVo;

import java.util.List;

/**
 * 任务投标信息Service接口
 *
 * @author citybiz
 * @date 2023-10-18
 */
public interface IBizTaskManuscriptService
{
    /**
     * 查询任务投标信息
     *
     * @param id 任务投标信息主键
     * @return 任务投标信息
     */
    public BizTaskManuscript selectBizTaskManuscriptById(Long id);

    /**
     * 查询任务投标信息列表
     *
     * @param bizTaskManuscript 任务投标信息
     * @return 任务投标信息集合
     */
    public List<BizTaskManuscriptDto> selectBizTaskManuscriptList(BizTaskManuscript bizTaskManuscript);

    /**
     * 新增任务投标信息
     *
     * @param bizTaskManuscript 任务投标信息
     * @return 结果
     */
    public int insertBizTaskManuscript(BizTaskManuscript bizTaskManuscript);

    /**
     * 修改任务投标信息
     *
     * @param bizTaskManuscript 任务投标信息
     * @return 结果
     */
    public int updateBizTaskManuscript(BizTaskManuscript bizTaskManuscript);

    /**
     * 批量删除任务投标信息
     *
     * @param ids 需要删除的任务投标信息主键集合
     * @return 结果
     */
    public int deleteBizTaskManuscriptByIds(Long[] ids);

    /**
     * 删除任务投标信息信息
     *
     * @param id 任务投标信息主键
     * @return 结果
     */
    public int deleteBizTaskManuscriptById(Long id);

    /**
     * 选中招标任务
     * @param taskManuscriptId 招标信息id
     */
    void selectedTaskManuscript(Long taskManuscriptId);

    /**
     * 投稿列表转换方法
     * @param bizTaskManuscriptDtos
     * @return
     */
    public List<BizTaskManuscriptVo> convertVO(List<BizTaskManuscriptDto> bizTaskManuscriptDtos);


    public BizTaskManuscriptVo convertVO(BizTaskManuscriptDto bizTaskManuscriptDto);
}
