package com.citybiz.web.controller.biz;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.citybiz.system.domain.biz.BizTaskManuscript;
import com.citybiz.system.domain.dto.BizTaskManuscriptDto;
import com.citybiz.system.service.biz.IBizTaskManuscriptService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.citybiz.common.annotation.Log;
import com.citybiz.common.core.controller.BaseController;
import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.enums.BusinessType;
import com.citybiz.common.utils.poi.ExcelUtil;
import com.citybiz.common.core.page.TableDataInfo;

/**
 * 任务投标信息Controller
 *
 * @author citybiz
 * @date 2023-10-18
 */
@RestController
@RequestMapping("/system/taskManuscript")
public class BizTaskManuscriptController extends BaseController
{
    @Autowired
    private IBizTaskManuscriptService bizTaskManuscriptService;

    /**
     * 查询任务投标信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:taskManuscript:list')")
    @GetMapping("/list")
    public TableDataInfo list(BizTaskManuscript bizTaskManuscript)
    {
        startPage();
        List<BizTaskManuscriptDto> list = bizTaskManuscriptService.selectBizTaskManuscriptList(bizTaskManuscript);
        return getDataTable(list);
    }

//    /**
//     * 导出任务投标信息列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:taskManuscript:export')")
//    @Log(title = "任务投标信息", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, BizTaskManuscript bizTaskManuscript)
//    {
//        List<BizTaskManuscriptDto> list = bizTaskManuscriptService.selectBizTaskManuscriptList(bizTaskManuscript);
//        ExcelUtil<BizTaskManuscriptDto> util = new ExcelUtil<BizTaskManuscript>(BizTaskManuscript.class);
//        util.exportExcel(response, list, "任务投标信息数据");
//    }

    /**
     * 获取任务投标信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:taskManuscript:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(bizTaskManuscriptService.selectBizTaskManuscriptById(id));
    }

    /**
     * 新增任务投标信息
     */
    @PreAuthorize("@ss.hasPermi('system:taskManuscript:add')")
    @Log(title = "任务投标信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BizTaskManuscript bizTaskManuscript)
    {
        return toAjax(bizTaskManuscriptService.insertBizTaskManuscript(bizTaskManuscript));
    }

    /**
     * 修改任务投标信息
     */
    @PreAuthorize("@ss.hasPermi('system:taskManuscript:edit')")
    @Log(title = "任务投标信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BizTaskManuscript bizTaskManuscript)
    {
        return toAjax(bizTaskManuscriptService.updateBizTaskManuscript(bizTaskManuscript));
    }

    /**
     * 删除任务投标信息
     */
    @PreAuthorize("@ss.hasPermi('system:taskManuscript:remove')")
    @Log(title = "任务投标信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(bizTaskManuscriptService.deleteBizTaskManuscriptByIds(ids));
    }
}
