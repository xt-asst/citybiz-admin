package com.citybiz.web.controller.biz;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.citybiz.system.domain.biz.BizTaskType;
import com.citybiz.system.service.biz.IBizTaskTypeService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.citybiz.common.annotation.Log;
import com.citybiz.common.core.controller.BaseController;
import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.enums.BusinessType;
import com.citybiz.common.utils.poi.ExcelUtil;

/**
 * 任务类型管理Controller
 * 
 * @author citybiz
 * @date 2023-10-01
 */
@RestController
@RequestMapping("/system/bizTaskType")
public class BizTaskTypeController extends BaseController
{
    @Autowired
    private IBizTaskTypeService bizTaskTypeService;

    /**
     * 查询任务类型管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:bizTaskType:list')")
    @GetMapping("/list")
    public AjaxResult list(BizTaskType bizTaskType)
    {
        List<BizTaskType> list = bizTaskTypeService.selectBizTaskTypeList(bizTaskType);
        return success(list);
    }

    /**
     * 导出任务类型管理列表
     */
    @PreAuthorize("@ss.hasPermi('system:bizTaskType:export')")
    @Log(title = "任务类型管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BizTaskType bizTaskType)
    {
        List<BizTaskType> list = bizTaskTypeService.selectBizTaskTypeList(bizTaskType);
        ExcelUtil<BizTaskType> util = new ExcelUtil<BizTaskType>(BizTaskType.class);
        util.exportExcel(response, list, "任务类型管理数据");
    }

    /**
     * 获取任务类型管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:bizTaskType:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(bizTaskTypeService.selectBizTaskTypeById(id));
    }

    /**
     * 新增任务类型管理
     */
    @PreAuthorize("@ss.hasPermi('system:bizTaskType:add')")
    @Log(title = "任务类型管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BizTaskType bizTaskType)
    {
        return toAjax(bizTaskTypeService.insertBizTaskType(bizTaskType));
    }

    /**
     * 修改任务类型管理
     */
    @PreAuthorize("@ss.hasPermi('system:bizTaskType:edit')")
    @Log(title = "任务类型管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BizTaskType bizTaskType)
    {
        return toAjax(bizTaskTypeService.updateBizTaskType(bizTaskType));
    }

    /**
     * 删除任务类型管理
     */
    @PreAuthorize("@ss.hasPermi('system:bizTaskType:remove')")
    @Log(title = "任务类型管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(bizTaskTypeService.deleteBizTaskTypeByIds(ids));
    }
}
