package com.citybiz.web.controller.biz;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.citybiz.common.exception.ServiceException;
import com.citybiz.system.domain.biz.BizShop;
import com.citybiz.system.domain.biz.BizShopCase;
import com.citybiz.system.service.biz.IBizShopCaseService;
import com.citybiz.system.service.biz.IBizShopService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.citybiz.common.annotation.Log;
import com.citybiz.common.core.controller.BaseController;
import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.enums.BusinessType;
import com.citybiz.common.utils.poi.ExcelUtil;
import com.citybiz.common.core.page.TableDataInfo;

/**
 * 店铺案例Controller
 *
 * @author citybiz
 * @date 2023-10-18
 */
@RestController
@RequestMapping("/system/shopCase")
@AllArgsConstructor
public class BizShopCaseController extends BaseController
{
    private IBizShopCaseService bizShopCaseService;

    private IBizShopService bizShopService;

    /**
     * 查询店铺案例列表
     */
    @PreAuthorize("@ss.hasPermi('system:shopCase:list')")
    @GetMapping("/list")
    public TableDataInfo list(BizShopCase bizShopCase)
    {
        startPage();
        List<BizShopCase> list = bizShopCaseService.selectBizShopCaseList(bizShopCase);
        return getDataTable(list);
    }

    /**
     * 导出店铺案例列表
     */
    @PreAuthorize("@ss.hasPermi('system:shopCase:export')")
    @Log(title = "店铺案例", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BizShopCase bizShopCase)
    {
        List<BizShopCase> list = bizShopCaseService.selectBizShopCaseList(bizShopCase);
        ExcelUtil<BizShopCase> util = new ExcelUtil<BizShopCase>(BizShopCase.class);
        util.exportExcel(response, list, "店铺案例数据");
    }

    /**
     * 获取店铺案例详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:shopCase:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(bizShopCaseService.selectBizShopCaseById(id));
    }

    /**
     * 新增店铺案例
     */
    @PreAuthorize("@ss.hasPermi('system:shopCase:add')")
    @Log(title = "店铺案例", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BizShopCase bizShopCase)
    {
        return toAjax(bizShopCaseService.insertBizShopCase(bizShopCase));
    }

    /**
     * 修改店铺案例
     */
    @PreAuthorize("@ss.hasPermi('system:shopCase:edit')")
    @Log(title = "店铺案例", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BizShopCase bizShopCase)
    {
        return toAjax(bizShopCaseService.updateBizShopCase(bizShopCase));
    }

    /**
     * 删除店铺案例
     */
    @PreAuthorize("@ss.hasPermi('system:shopCase:remove')")
    @Log(title = "店铺案例", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(bizShopCaseService.deleteBizShopCaseByIds(ids));
    }

    @GetMapping("/getShopCaseByShopId")
    public AjaxResult getShopCases(Long shopId){
        BizShop bizShop = bizShopService.selectBizShopById(shopId);
        if(bizShop == null){
            throw new ServiceException("商店不存在，请确认");
        }
        BizShopCase select = new BizShopCase();
        select.setShopId(bizShop.getId());
        select.setDelFlag(0L);
        List<BizShopCase> bizShopCases = bizShopCaseService.selectBizShopCaseList(select);
        bizShop.setBizShopCases(bizShopCases);
        return AjaxResult.success(bizShopCases);
    }
}
