package com.citybiz.web.controller.biz;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.citybiz.system.domain.biz.WxWalletLogs;
import com.citybiz.system.service.biz.IWxWalletLogsService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.citybiz.common.annotation.Log;
import com.citybiz.common.core.controller.BaseController;
import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.enums.BusinessType;
import com.citybiz.common.utils.poi.ExcelUtil;
import com.citybiz.common.core.page.TableDataInfo;

/**
 * 用户钱包明细Controller
 * 
 * @author citybiz
 * @date 2023-10-18
 */
@RestController
@RequestMapping("/system/walletLogs")
public class WxWalletLogsController extends BaseController
{
    @Autowired
    private IWxWalletLogsService wxWalletLogsService;

    /**
     * 查询用户钱包明细列表
     */
    @PreAuthorize("@ss.hasPermi('system:walletLogs:list')")
    @GetMapping("/list")
    public TableDataInfo list(WxWalletLogs wxWalletLogs)
    {
        startPage();
        List<WxWalletLogs> list = wxWalletLogsService.selectWxWalletLogsList(wxWalletLogs);
        return getDataTable(list);
    }

    /**
     * 导出用户钱包明细列表
     */
    @PreAuthorize("@ss.hasPermi('system:walletLogs:export')")
    @Log(title = "用户钱包明细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WxWalletLogs wxWalletLogs)
    {
        List<WxWalletLogs> list = wxWalletLogsService.selectWxWalletLogsList(wxWalletLogs);
        ExcelUtil<WxWalletLogs> util = new ExcelUtil<WxWalletLogs>(WxWalletLogs.class);
        util.exportExcel(response, list, "用户钱包明细数据");
    }

    /**
     * 获取用户钱包明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:walletLogs:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(wxWalletLogsService.selectWxWalletLogsById(id));
    }

    /**
     * 新增用户钱包明细
     */
    @PreAuthorize("@ss.hasPermi('system:walletLogs:add')")
    @Log(title = "用户钱包明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WxWalletLogs wxWalletLogs)
    {
        return toAjax(wxWalletLogsService.insertWxWalletLogs(wxWalletLogs));
    }

    /**
     * 修改用户钱包明细
     */
    @PreAuthorize("@ss.hasPermi('system:walletLogs:edit')")
    @Log(title = "用户钱包明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WxWalletLogs wxWalletLogs)
    {
        return toAjax(wxWalletLogsService.updateWxWalletLogs(wxWalletLogs));
    }

    /**
     * 删除用户钱包明细
     */
    @PreAuthorize("@ss.hasPermi('system:walletLogs:remove')")
    @Log(title = "用户钱包明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(wxWalletLogsService.deleteWxWalletLogsByIds(ids));
    }
}
