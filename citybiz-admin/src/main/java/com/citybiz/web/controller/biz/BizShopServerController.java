package com.citybiz.web.controller.biz;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.citybiz.common.enums.biz.ServerTypeEnum;
import com.citybiz.common.exception.ServiceException;
import com.citybiz.common.utils.AssertUtils;
import com.citybiz.system.domain.biz.BizShop;
import com.citybiz.system.domain.biz.BizShopCase;
import com.citybiz.system.domain.biz.BizShopServer;
import com.citybiz.system.service.biz.IBizShopCaseService;
import com.citybiz.system.service.biz.IBizShopServerService;
import com.citybiz.system.service.biz.IBizShopService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.citybiz.common.annotation.Log;
import com.citybiz.common.core.controller.BaseController;
import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.enums.BusinessType;
import com.citybiz.common.utils.poi.ExcelUtil;
import com.citybiz.common.core.page.TableDataInfo;

/**
 * 店铺服务列Controller
 *
 * @author citybiz
 * @date 2023-10-18
 */
@RestController
@RequestMapping("/system/shopServer")
@AllArgsConstructor
public class BizShopServerController extends BaseController
{
    private IBizShopServerService bizShopServerService;

    private IBizShopService bizShopService;

    /**
     * 查询店铺服务列列表
     */
    @PreAuthorize("@ss.hasPermi('system:shopServer:list')")
    @GetMapping("/list")
    public TableDataInfo list(BizShopServer bizShopServer)
    {
        startPage();
        List<BizShopServer> list = bizShopServerService.selectBizShopServerList(bizShopServer);
        return getDataTable(list);
    }

    /**
     * 导出店铺服务列列表
     */
    @PreAuthorize("@ss.hasPermi('system:shopServer:export')")
    @Log(title = "店铺服务列", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BizShopServer bizShopServer)
    {
        List<BizShopServer> list = bizShopServerService.selectBizShopServerList(bizShopServer);
        ExcelUtil<BizShopServer> util = new ExcelUtil<BizShopServer>(BizShopServer.class);
        util.exportExcel(response, list, "店铺服务列数据");
    }

    /**
     * 获取店铺服务列详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:shopServer:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(bizShopServerService.selectBizShopServerById(id));
    }

    /**
     * 新增店铺服务列
     */
    @PreAuthorize("@ss.hasPermi('system:shopServer:add')")
    @Log(title = "店铺服务列", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BizShopServer bizShopServer)
    {
        return toAjax(bizShopServerService.insertBizShopServer(bizShopServer));
    }

    /**
     * 修改店铺服务列
     */
    @PreAuthorize("@ss.hasPermi('system:shopServer:edit')")
    @Log(title = "店铺服务列", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BizShopServer bizShopServer)
    {
        return toAjax(bizShopServerService.updateBizShopServer(bizShopServer));
    }

    /**
     * 删除店铺服务列
     */
    @PreAuthorize("@ss.hasPermi('system:shopServer:remove')")
    @Log(title = "店铺服务列", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(bizShopServerService.deleteBizShopServerByIds(ids));
    }

    /**
     * 店铺作品查询
     * @param shopId 店铺id
     * @param sererType 作品类型 work:作品 server:服务
     * @return 查询列表
     */
    @PreAuthorize("@ss.hasPermi('system:shopServer:query')")
    @GetMapping("/queryShopServer")
    public AjaxResult queryShopServer(Long shopId, String sererType){
        AssertUtils.isNotNull(shopId, "店铺id不可为空");
        if(!ServerTypeEnum.contains(sererType)){
            throw new ServiceException("服务类型不在范围内，请确认");
        }

        List<BizShopServer> bizShopServers = bizShopServerService.queryShopServer(shopId, sererType);
        return AjaxResult.success(bizShopServers);
    }


}
