package com.citybiz.web.controller.biz;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.citybiz.system.service.biz.IBizBannerService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.citybiz.common.annotation.Log;
import com.citybiz.common.core.controller.BaseController;
import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.enums.BusinessType;
import com.citybiz.system.domain.biz.BizBanner;
import com.citybiz.common.utils.poi.ExcelUtil;
import com.citybiz.common.core.page.TableDataInfo;

/**
 * banner轮播图列Controller
 * 
 * @author ykg
 * @date 2023-09-29
 */
@RestController
@RequestMapping("/system/banner")
public class BizBannerController extends BaseController
{
    @Autowired
    private IBizBannerService bizBannerService;

    /**
     * 查询banner轮播图列列表
     */
    @PreAuthorize("@ss.hasPermi('system:banner:list')")
    @GetMapping("/list")
    public TableDataInfo list(BizBanner bizBanner)
    {
        startPage();
        List<BizBanner> list = bizBannerService.selectBizBannerList(bizBanner);
        return getDataTable(list);
    }

    /**
     * 导出banner轮播图列列表
     */
    @PreAuthorize("@ss.hasPermi('system:banner:export')")
    @Log(title = "banner轮播图列", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BizBanner bizBanner)
    {
        List<BizBanner> list = bizBannerService.selectBizBannerList(bizBanner);
        ExcelUtil<BizBanner> util = new ExcelUtil<BizBanner>(BizBanner.class);
        util.exportExcel(response, list, "banner轮播图列数据");
    }

    /**
     * 获取banner轮播图列详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:banner:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(bizBannerService.selectBizBannerById(id));
    }

    /**
     * 新增banner轮播图列
     */
    @PreAuthorize("@ss.hasPermi('system:banner:add')")
    @Log(title = "banner轮播图列", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BizBanner bizBanner)
    {
        return toAjax(bizBannerService.insertBizBanner(bizBanner));
    }

    /**
     * 修改banner轮播图列
     */
    @PreAuthorize("@ss.hasPermi('system:banner:edit')")
    @Log(title = "banner轮播图列", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BizBanner bizBanner)
    {
        return toAjax(bizBannerService.updateBizBanner(bizBanner));
    }

    /**
     * 删除banner轮播图列
     */
    @PreAuthorize("@ss.hasPermi('system:banner:remove')")
    @Log(title = "banner轮播图列", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(bizBannerService.deleteBizBannerByIds(ids));
    }
}
