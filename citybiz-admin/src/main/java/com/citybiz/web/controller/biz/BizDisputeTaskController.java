package com.citybiz.web.controller.biz;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.citybiz.system.domain.biz.BizDisputeTask;
import com.citybiz.system.service.biz.IBizDisputeTaskService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.citybiz.common.annotation.Log;
import com.citybiz.common.core.controller.BaseController;
import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.enums.BusinessType;
import com.citybiz.common.utils.poi.ExcelUtil;
import com.citybiz.common.core.page.TableDataInfo;

/**
 * 交易维权信息Controller
 * 
 * @author citybiz
 * @date 2023-10-15
 */
@RestController
@RequestMapping("/system/disputeTask")
public class BizDisputeTaskController extends BaseController
{
    @Autowired
    private IBizDisputeTaskService bizDisputeTaskService;

    /**
     * 查询交易维权信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:disputeTask:list')")
    @GetMapping("/list")
    public TableDataInfo list(BizDisputeTask bizDisputeTask)
    {
        startPage();
        List<BizDisputeTask> list = bizDisputeTaskService.selectBizDisputeTaskList(bizDisputeTask);
        return getDataTable(list);
    }

    /**
     * 导出交易维权信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:disputeTask:export')")
    @Log(title = "交易维权信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BizDisputeTask bizDisputeTask)
    {
        List<BizDisputeTask> list = bizDisputeTaskService.selectBizDisputeTaskList(bizDisputeTask);
        ExcelUtil<BizDisputeTask> util = new ExcelUtil<BizDisputeTask>(BizDisputeTask.class);
        util.exportExcel(response, list, "交易维权信息数据");
    }

    /**
     * 获取交易维权信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:disputeTask:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(bizDisputeTaskService.selectBizDisputeTaskById(id));
    }

    /**
     * 新增交易维权信息
     */
    @PreAuthorize("@ss.hasPermi('system:disputeTask:add')")
    @Log(title = "交易维权信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BizDisputeTask bizDisputeTask)
    {
        return toAjax(bizDisputeTaskService.insertBizDisputeTask(bizDisputeTask));
    }

    /**
     * 修改交易维权信息
     */
    @PreAuthorize("@ss.hasPermi('system:disputeTask:edit')")
    @Log(title = "交易维权信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BizDisputeTask bizDisputeTask)
    {
        return toAjax(bizDisputeTaskService.updateBizDisputeTask(bizDisputeTask));
    }

    /**
     * 删除交易维权信息
     */
    @PreAuthorize("@ss.hasPermi('system:disputeTask:remove')")
    @Log(title = "交易维权信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(bizDisputeTaskService.deleteBizDisputeTaskByIds(ids));
    }
}
