package com.citybiz.web.controller.biz;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.citybiz.system.domain.biz.BizShop;
import com.citybiz.system.service.biz.IBizShopService;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.citybiz.common.annotation.Log;
import com.citybiz.common.core.controller.BaseController;
import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.enums.BusinessType;
import com.citybiz.common.utils.poi.ExcelUtil;
import com.citybiz.common.core.page.TableDataInfo;

/**
 * 店铺格Controller
 * 
 * @author citybiz
 * @date 2023-10-02
 */
@RestController
@RequestMapping("/system/shop")
public class BizShopController extends BaseController
{
    @Autowired
    private IBizShopService bizShopService;

    /**
     * 查询店铺格列表
     */
    @PreAuthorize("@ss.hasPermi('system:shop:list')")
    @GetMapping("/list")
    public TableDataInfo list(BizShop bizShop)
    {
        startPage();
        List<BizShop> list = bizShopService.selectBizShopList(bizShop, false);
        return getDataTable(list);
    }

    /**
     * 导出店铺格列表
     */
    @PreAuthorize("@ss.hasPermi('system:shop:export')")
    @Log(title = "店铺格", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BizShop bizShop)
    {
        List<BizShop> list = bizShopService.selectBizShopList(bizShop, false);
        ExcelUtil<BizShop> util = new ExcelUtil<BizShop>(BizShop.class);
        util.exportExcel(response, list, "店铺格数据");
    }

    /**
     * 获取店铺格详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:shop:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(bizShopService.selectBizShopById(id));
    }

    /**
     * 新增店铺格
     */
    @PreAuthorize("@ss.hasPermi('system:shop:add')")
    @Log(title = "店铺格", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BizShop bizShop)
    {
        return toAjax(bizShopService.insertBizShop(bizShop));
    }

    /**
     * 修改店铺格
     */
    @PreAuthorize("@ss.hasPermi('system:shop:edit')")
    @Log(title = "店铺格", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BizShop bizShop)
    {
        return toAjax(bizShopService.updateBizShop(bizShop));
    }

    /**
     * 删除店铺格
     */
    @PreAuthorize("@ss.hasPermi('system:shop:remove')")
    @Log(title = "店铺格", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(bizShopService.deleteBizShopByIds(ids));
    }

    /**
     * 审核店铺格
     */
    @PreAuthorize("@ss.hasPermi('system:shop:edit')")
    @Log(title = "店铺格", businessType = BusinessType.INSERT)
    @GetMapping("/check")
    public AjaxResult check(Long shopId, Long checkCode)
    {
        bizShopService.checkBizShop(shopId, checkCode);
        return AjaxResult.success("审核结束");
    }


}
