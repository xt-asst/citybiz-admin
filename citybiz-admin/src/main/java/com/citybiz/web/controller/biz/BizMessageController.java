package com.citybiz.web.controller.biz;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.citybiz.system.domain.biz.BizMessage;
import com.citybiz.system.service.biz.IBizMessageService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.citybiz.common.annotation.Log;
import com.citybiz.common.core.controller.BaseController;
import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.enums.BusinessType;
import com.citybiz.common.utils.poi.ExcelUtil;
import com.citybiz.common.core.page.TableDataInfo;

/**
 * 消息业务格Controller
 * 
 * @author citybiz
 * @date 2023-11-04
 */
@RestController
@RequestMapping("/system/biz_message")
public class BizMessageController extends BaseController
{
    @Autowired
    private IBizMessageService bizMessageService;

    /**
     * 查询消息业务格列表
     */
    @PreAuthorize("@ss.hasPermi('system:biz_message:list')")
    @GetMapping("/list")
    public TableDataInfo list(BizMessage bizMessage)
    {
        startPage();
        List<BizMessage> list = bizMessageService.selectBizMessageList(bizMessage);
        return getDataTable(list);
    }

    /**
     * 导出消息业务格列表
     */
    @PreAuthorize("@ss.hasPermi('system:biz_message:export')")
    @Log(title = "消息业务格", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BizMessage bizMessage)
    {
        List<BizMessage> list = bizMessageService.selectBizMessageList(bizMessage);
        ExcelUtil<BizMessage> util = new ExcelUtil<BizMessage>(BizMessage.class);
        util.exportExcel(response, list, "消息业务格数据");
    }

    /**
     * 获取消息业务格详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:biz_message:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(bizMessageService.selectBizMessageById(id));
    }

    /**
     * 新增消息业务格
     */
    @PreAuthorize("@ss.hasPermi('system:biz_message:add')")
    @Log(title = "消息业务格", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BizMessage bizMessage)
    {
        return toAjax(bizMessageService.insertBizMessage(bizMessage));
    }

    /**
     * 修改消息业务格
     */
    @PreAuthorize("@ss.hasPermi('system:biz_message:edit')")
    @Log(title = "消息业务格", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BizMessage bizMessage)
    {
        return toAjax(bizMessageService.updateBizMessage(bizMessage));
    }

    /**
     * 删除消息业务格
     */
    @PreAuthorize("@ss.hasPermi('system:biz_message:remove')")
    @Log(title = "消息业务格", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(bizMessageService.deleteBizMessageByIds(ids));
    }
}
