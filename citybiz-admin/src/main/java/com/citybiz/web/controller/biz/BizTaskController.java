package com.citybiz.web.controller.biz;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.citybiz.common.core.page.BizTableDataInfo;
import com.citybiz.system.domain.biz.BizTask;
import com.citybiz.system.domain.dto.BizTaskDto;
import com.citybiz.system.domain.request.BizTaskRequest;
import com.citybiz.system.service.biz.IBizTaskService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.citybiz.common.annotation.Log;
import com.citybiz.common.core.controller.BaseController;
import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.enums.BusinessType;
import com.citybiz.common.utils.poi.ExcelUtil;
import com.citybiz.common.core.page.TableDataInfo;

/**
 * 任务格Controller
 * 
 * @author citybiz
 * @date 2023-10-01
 */
@RestController
@RequestMapping("/system/task")
public class BizTaskController extends BaseController
{
    @Autowired
    private IBizTaskService bizTaskService;

    /**
     * 查询任务格列表
     */
    @PreAuthorize("@ss.hasPermi('system:task:list')")
    @GetMapping("/list")
    public BizTableDataInfo<BizTaskDto> list(BizTaskRequest request)
    {
        return bizTaskService.selectBizTaskListByPage(request);
    }

//    /**
//     * 导出任务格列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:task:export')")
//    @Log(title = "任务格", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, BizTaskRequest bizTask)
//    {
//        List<BizTaskDto> bizTaskDtos = bizTaskService.selectBizTaskList(bizTask);
//        ExcelUtil<BizTask> util = new ExcelUtil<BizTask>(BizTask.class);
//        util.exportExcel(response, list, "任务格数据");
//    }

    /**
     * 获取任务格详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:task:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(bizTaskService.selectBizTaskById(id));
    }

//    /**
//     * 修改任务格
//     */
//    @PreAuthorize("@ss.hasPermi('system:task:edit')")
//    @Log(title = "任务格", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody BizTaskRequest request)
//    {
//        return toAjax(bizTaskService.updateBizTask(bizTask));
//    }

//    /**
//     * 删除任务格
//     */
//    @PreAuthorize("@ss.hasPermi('system:task:remove')")
//    @Log(title = "任务格", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(bizTaskService.deleteBizTaskByIds(ids));
//    }
}
