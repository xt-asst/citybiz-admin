package com.citybiz.web.controller.biz;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.citybiz.common.exception.WxBizException;
import com.citybiz.system.domain.biz.BizWxPrivateApproveInfo;
import com.citybiz.system.service.biz.IBizWxPrivateApproveInfoService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.citybiz.common.annotation.Log;
import com.citybiz.common.core.controller.BaseController;
import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.enums.BusinessType;
import com.citybiz.common.utils.poi.ExcelUtil;
import com.citybiz.common.core.page.TableDataInfo;

/**
 * 私人认证审核Controller
 * 
 * @author citybiz
 * @date 2023-10-04
 */
@RestController
@RequestMapping("/system/private_approve")
public class BizWxPrivateApproveInfoController extends BaseController
{
    @Autowired
    private IBizWxPrivateApproveInfoService bizWxPrivateApproveInfoService;

    /**
     * 查询私人认证审核列表
     */
    @PreAuthorize("@ss.hasPermi('system:private_approve:list')")
    @GetMapping("/list")
    public TableDataInfo list(BizWxPrivateApproveInfo bizWxPrivateApproveInfo)
    {
        startPage();
        List<BizWxPrivateApproveInfo> list = bizWxPrivateApproveInfoService.selectBizWxPrivateApproveInfoList(bizWxPrivateApproveInfo);
        return getDataTable(list);
    }

    /**
     * 导出私人认证审核列表
     */
    @PreAuthorize("@ss.hasPermi('system:private_approve:export')")
    @Log(title = "私人认证审核", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BizWxPrivateApproveInfo bizWxPrivateApproveInfo)
    {
        List<BizWxPrivateApproveInfo> list = bizWxPrivateApproveInfoService.selectBizWxPrivateApproveInfoList(bizWxPrivateApproveInfo);
        ExcelUtil<BizWxPrivateApproveInfo> util = new ExcelUtil<BizWxPrivateApproveInfo>(BizWxPrivateApproveInfo.class);
        util.exportExcel(response, list, "私人认证审核数据");
    }

    /**
     * 获取私人认证审核详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:private_approve:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(bizWxPrivateApproveInfoService.selectBizWxPrivateApproveInfoById(id));
    }

    /**
     * 审核私人认证信息
     */
    @PreAuthorize("@ss.hasPermi('system:private_approve:edit')")
    @GetMapping(value = "/check/private")
    public AjaxResult checkApproveInfo(Long id,Long status)
    {
        if(status != 2 && status != 1){
            throw new WxBizException("审核信息出错，请确认");
        }
        bizWxPrivateApproveInfoService.checkApproveInfo(id,status);
        return AjaxResult.success("操作成功");
    }
}
