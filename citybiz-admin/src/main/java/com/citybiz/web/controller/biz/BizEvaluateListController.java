package com.citybiz.web.controller.biz;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.citybiz.system.domain.biz.BizEvaluate;
import com.citybiz.system.service.biz.IBizEvaluateListService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.citybiz.common.annotation.Log;
import com.citybiz.common.core.controller.BaseController;
import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.enums.BusinessType;
import com.citybiz.common.utils.poi.ExcelUtil;
import com.citybiz.common.core.page.TableDataInfo;

/**
 * 评级列Controller
 * 
 * @author citybiz
 * @date 2023-10-16
 */
@RestController
@RequestMapping("/system/evaluateList")
public class BizEvaluateListController extends BaseController
{
    @Autowired
    private IBizEvaluateListService bizEvaluateListService;

    /**
     * 查询评级列列表
     */
    @PreAuthorize("@ss.hasPermi('system:evaluateList:list')")
    @GetMapping("/list")
    public TableDataInfo list(BizEvaluate bizEvaluate)
    {
        startPage();
        List<BizEvaluate> list = bizEvaluateListService.selectBizEvaluateListList(bizEvaluate);
        return getDataTable(list);
    }

    /**
     * 导出评级列列表
     */
    @PreAuthorize("@ss.hasPermi('system:evaluateList:export')")
    @Log(title = "评级列", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BizEvaluate bizEvaluate)
    {
        List<BizEvaluate> list = bizEvaluateListService.selectBizEvaluateListList(bizEvaluate);
        ExcelUtil<BizEvaluate> util = new ExcelUtil<BizEvaluate>(BizEvaluate.class);
        util.exportExcel(response, list, "评级列数据");
    }

    /**
     * 获取评级列详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:evaluateList:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(bizEvaluateListService.selectBizEvaluateListById(id));
    }

    /**
     * 新增评级列
     */
    @PreAuthorize("@ss.hasPermi('system:evaluateList:add')")
    @Log(title = "评级列", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BizEvaluate bizEvaluate)
    {
        return toAjax(bizEvaluateListService.insertBizEvaluateList(bizEvaluate));
    }

    /**
     * 修改评级列
     */
    @PreAuthorize("@ss.hasPermi('system:evaluateList:edit')")
    @Log(title = "评级列", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BizEvaluate bizEvaluate)
    {
        return toAjax(bizEvaluateListService.updateBizEvaluateList(bizEvaluate));
    }

    /**
     * 删除评级列
     */
    @PreAuthorize("@ss.hasPermi('system:evaluateList:remove')")
    @Log(title = "评级列", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(bizEvaluateListService.deleteBizEvaluateListByIds(ids));
    }
}
