package com.citybiz.web.controller.biz;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.citybiz.common.exception.WxBizException;
import com.citybiz.common.utils.poi.ExcelUtil;
import com.citybiz.system.domain.biz.BizWxCompanyApproveInfo;
import com.citybiz.system.service.biz.IBizWxCompanyApproveInfoService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.citybiz.common.annotation.Log;
import com.citybiz.common.core.controller.BaseController;
import com.citybiz.common.core.domain.AjaxResult;
import com.citybiz.common.enums.BusinessType;
import com.citybiz.common.core.page.TableDataInfo;

/**
 * 公司认证审核Controller
 * 
 * @author citybiz
 * @date 2023-10-04
 */
@RestController
@RequestMapping("/system/company_approve")
public class BizWxCompanyApproveInfoController extends BaseController
{
    @Autowired
    private IBizWxCompanyApproveInfoService bizWxCompanyApproveInfoService;

    /**
     * 查询公司认证审核列表
     */
    @PreAuthorize("@ss.hasPermi('system:company_approve:list')")
    @GetMapping("/list")
    public TableDataInfo list(BizWxCompanyApproveInfo bizWxCompanyApproveInfo)
    {
        startPage();
        List<BizWxCompanyApproveInfo> list = bizWxCompanyApproveInfoService.selectBizWxCompanyApproveInfoList(bizWxCompanyApproveInfo);
        return getDataTable(list);
    }

    /**
     * 导出公司认证审核列表
     */
    @PreAuthorize("@ss.hasPermi('system:company_approve:export')")
    @Log(title = "公司认证审核", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BizWxCompanyApproveInfo bizWxCompanyApproveInfo)
    {
        List<BizWxCompanyApproveInfo> list = bizWxCompanyApproveInfoService.selectBizWxCompanyApproveInfoList(bizWxCompanyApproveInfo);
        ExcelUtil<BizWxCompanyApproveInfo> util = new ExcelUtil<BizWxCompanyApproveInfo>(BizWxCompanyApproveInfo.class);
        util.exportExcel(response, list, "公司认证审核数据");
    }

    /**
     * 获取公司认证审核详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:company_approve:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(bizWxCompanyApproveInfoService.selectBizWxCompanyApproveInfoById(id));
    }

    /**
     * 新增公司认证审核
     */
//    @PreAuthorize("@ss.hasPermi('system:company_approve:add')")
//    @Log(title = "公司认证审核", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody BizWxCompanyApproveInfo bizWxCompanyApproveInfo)
//    {
//        return toAjax(bizWxCompanyApproveInfoService.insertBizWxCompanyApproveInfo(bizWxCompanyApproveInfo));
//    }

    /**
     * 修改公司认证审核
     */
//    @PreAuthorize("@ss.hasPermi('system:company_approve:edit')")
//    @Log(title = "公司认证审核", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody BizWxCompanyApproveInfo bizWxCompanyApproveInfo)
//    {
//        return toAjax(bizWxCompanyApproveInfoService.updateBizWxCompanyApproveInfo(bizWxCompanyApproveInfo));
//    }

    /**
     * 删除公司认证审核
     */
//    @PreAuthorize("@ss.hasPermi('system:company_approve:remove')")
//    @Log(title = "公司认证审核", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Long[] ids)
//    {
//        return toAjax(bizWxCompanyApproveInfoService.deleteBizWxCompanyApproveInfoByIds(ids));
//    }

    /**
     * 审核公司认证信息
     * @return 审核结果
     */
    @PreAuthorize("@ss.hasPermi('system:company_approve:edit')")
    @GetMapping(value = "/check/company")
    public AjaxResult checkCompanyInfo( Long id, Long status){
        if(status != 2 && status != 1){
            throw new WxBizException("审核信息出错，请确认");
        }
        bizWxCompanyApproveInfoService.checkApproveInfo(id,status);
        return AjaxResult.success("公司认证信息审核成功");
    }

}
