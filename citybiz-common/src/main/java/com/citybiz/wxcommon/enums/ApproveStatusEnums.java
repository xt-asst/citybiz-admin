package com.citybiz.wxcommon.enums;

/**
 * Create by Ykg on 2023/10/5 16:44
 * effect: 认证审核状态
 */
public enum ApproveStatusEnums {

    NOT_EXIST(-1L, "不存在"),

    NOT_CHECK(0L, "审核中"),
    CHECK_PASS(1L, "审核通过"),
    CHECK_NOT_PASS(2L, "审核不通过")
    ;
    private final Long code;

    private final String msg;

    ApproveStatusEnums(Long code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public Long getCode() {
        return code;
    }
}
