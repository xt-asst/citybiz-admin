package com.citybiz.wxcommon.enums;

/**
 * Create by Ykg on 2023/10/3
 * effect: 微信用户角色枚举
 */
public enum WxUserRoleEnums {

    WORKER_ROLE(0, "工作者"),
    EMPLOYER_ROLE(1, "雇主")

    ;

    private final Integer code;

    private final String message;

    WxUserRoleEnums(Integer code, String message){
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
