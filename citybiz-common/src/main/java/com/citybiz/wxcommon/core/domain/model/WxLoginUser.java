package com.citybiz.wxcommon.core.domain.model;

import lombok.Data;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Create by Ykg on 2023/9/29 14:57
 * effect: 微信登录用户
 */
@Data
public class WxLoginUser {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户唯一标识
     */
    private String token;

    /**
     * 登录时间
     */
    private Long loginTime;

    /**
     * 过期时间
     */
    private Long expireTime;

    /**
     * 登录IP地址
     */
    private String ipaddr;

    /**
     * 登录地点
     */
    private String loginLocation;

    /**
     * openId
     */
    private String openId;

    /**
     * 用户真实姓名
     */
    private String username;

    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * 登录角色
     */
    private Integer role;
}
