package com.citybiz.common.exception.base;

/**
 * Create by Ykg on 2023/9/29 00:15
 * effect: 断言异常类
 */
public class AssertException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    private Integer code;

    /**
     * 错误提示
     */
    private String message;

    /**
     * 错误明细，内部调试错误
     *
     * 和 {@link CommonResult#getDetailMessage()} 一致的设计
     */
    private String detailMessage;

    /**
     * 空构造方法，避免反序列化问题
     */
    public AssertException()
    {
    }

    public AssertException(String message)
    {
        this.code = 1;
        this.message = message;
    }

    public AssertException(String message, Integer code)
    {
        this.message = message;
        this.code = code;
    }

    public String getDetailMessage()
    {
        return detailMessage;
    }

    @Override
    public String getMessage()
    {
        return message;
    }

    public Integer getCode()
    {
        return code;
    }

    public AssertException setMessage(String message)
    {
        this.message = message;
        return this;
    }

    public AssertException setDetailMessage(String detailMessage)
    {
        this.detailMessage = detailMessage;
        return this;
    }
}
