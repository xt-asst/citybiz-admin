package com.citybiz.common.exception;

/**
 * 演示模式异常
 * 
 * @author citybiz
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
