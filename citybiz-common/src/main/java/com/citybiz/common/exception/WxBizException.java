package com.citybiz.common.exception;

/**
 * Create by Ykg on 2023/9/29 00:14
 * effect: 微信业务异常
 */
public class WxBizException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    private Integer code;

    /**
     * 错误提示
     */
    private String message;

    /**
     * 错误明细，内部调试错误
     *
     * 和 {@link CommonResult#getDetailMessage()} 一致的设计
     */
    private String detailMessage;

    /**
     * 空构造方法，避免反序列化问题
     */
    public WxBizException()
    {
    }

    public WxBizException(String message)
    {
        this.message = message;
    }

    public WxBizException(String message, Integer code)
    {
        this.message = message;
        this.code = code;
    }

    public String getDetailMessage()
    {
        return detailMessage;
    }

    @Override
    public String getMessage()
    {
        return message;
    }

    public Integer getCode()
    {
        return code;
    }

    public WxBizException setMessage(String message)
    {
        this.message = message;
        return this;
    }

    public WxBizException setDetailMessage(String detailMessage)
    {
        this.detailMessage = detailMessage;
        return this;
    }
}
