package com.citybiz.common.exception;

/**
 * Create by Ykg on 2023/10/23 18:25
 * effect:
 */
public class HandlerException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    private Integer code;

    /**
     * 错误提示
     */
    private String message;

    /**
     * 错误明细，内部调试错误
     *
     * 和 {@link CommonResult#getDetailMessage()} 一致的设计
     */
    private String detailMessage;

    /**
     * 空构造方法，避免反序列化问题
     */
    public HandlerException()
    {
    }

    public HandlerException(String message)
    {
        this.message = message;
    }

    public HandlerException(String message, Integer code)
    {
        this.message = message;
        this.code = code;
    }

    public String getDetailMessage()
    {
        return detailMessage;
    }

    @Override
    public String getMessage()
    {
        return message;
    }

    public Integer getCode()
    {
        return code;
    }

    public HandlerException setMessage(String message)
    {
        this.message = message;
        return this;
    }

    public HandlerException setDetailMessage(String detailMessage)
    {
        this.detailMessage = detailMessage;
        return this;
    }
}