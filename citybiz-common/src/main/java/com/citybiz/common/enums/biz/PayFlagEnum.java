package com.citybiz.common.enums.biz;

import lombok.Getter;

/**
 * Create by Ykg on 2023/10/15 18:38
 * effect: 支付标记
 */
@Getter
public enum PayFlagEnum {

    NOT_PAID(0, "未支付"),

    MANAGED(1, "已托管"),

    PAID(2, "已支付"),

    ;

    private final Integer code;

    private final String msg;

    PayFlagEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
