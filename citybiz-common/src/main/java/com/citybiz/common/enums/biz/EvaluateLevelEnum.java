package com.citybiz.common.enums.biz;

import lombok.Getter;

import java.util.HashSet;
import java.util.Set;

/**
 * Create by Ykg on 2023/10/15 19:14
 * effect: 评价等级
 */
@Getter
public enum EvaluateLevelEnum {

    GOOD_EVALUATE("good_evaluate", "好评"),

    NEUTRAL_EVALUATE("neutral_evaluate", "中评"),

    BAD_EVALUATE("bad_evaluate", "差评"),

    ;

    private final static Set<String> codeSet = new HashSet<>();

    static{
        EvaluateLevelEnum[] values = EvaluateLevelEnum.values();
        for (EvaluateLevelEnum value : values) {
            codeSet.add(value.getCode());
        }
    }

    public static boolean contains(String code){
        return codeSet.contains(code);
    }


    private final String code;

    private final String msg;

    EvaluateLevelEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
