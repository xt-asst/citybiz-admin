package com.citybiz.common.enums.biz;

import lombok.Getter;

/**
 * Create by Ykg on 2023/10/15 16:55
 * effect: 认证信息
 */
@Getter
public enum WxAuthStatusEnum {

    NOT_CERTIFIED(0L, "未认证"),

    CERTIFIED_FAIL(2L, "认证失败"),

    CERTIFIED_SUCCESS(1L, "认证成功");


    private final Long code;

    private final String msg;

    WxAuthStatusEnum(Long code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
