package com.citybiz.common.enums.biz;

import lombok.Getter;

import java.util.HashSet;
import java.util.Set;

/**
 * Create by Ykg on 2023/11/4 23:10
 * effect:
 */
@Getter
public enum BizMessageTypeEnums {

    TASK_MANUSCRIPT("TaskManuscript", "任务投稿信息"),

    ;

    private final String code;

    private final String msg;

    BizMessageTypeEnums(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private final static Set<String> codeSet = new HashSet<>();

    static{
        BizMessageTypeEnums[] values = BizMessageTypeEnums.values();
        for (BizMessageTypeEnums value : values) {
            codeSet.add(value.getCode());
        }
    }

    public static boolean contains(Long code){
        return codeSet.contains(code);
    }

}
