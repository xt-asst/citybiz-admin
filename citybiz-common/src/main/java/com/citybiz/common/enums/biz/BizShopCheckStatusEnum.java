package com.citybiz.common.enums.biz;

import lombok.Getter;

import java.util.HashSet;
import java.util.Set;

/**
 * Create by Ykg on 2023/10/24 21:48
 * effect: 店铺审核枚举
 */
@Getter
public enum BizShopCheckStatusEnum {

    NOT_EXIST(-1L, "不存在"),

    CHECK_FAIL(2L, "审核失败"),

    CHECK_SUCCESS(1L, "审核成功"),

    NOT_CHECK(0L, "未审核");

    private final static Set<Long> codeSet = new HashSet<>();

    static{
        BizShopCheckStatusEnum[] values = BizShopCheckStatusEnum.values();
        for (BizShopCheckStatusEnum value : values) {
            codeSet.add(value.getCode());
        }
    }

    public static boolean contains(Long code){
        return codeSet.contains(code);
    }


    private final Long code;

    private final String msg;

    BizShopCheckStatusEnum(Long code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
