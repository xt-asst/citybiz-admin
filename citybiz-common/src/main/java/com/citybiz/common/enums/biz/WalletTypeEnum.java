package com.citybiz.common.enums.biz;

import lombok.Getter;

/**
 * Create by Ykg on 2023/10/15 19:21
 * effect: 钱包明细类型
 */
@Getter
public enum WalletTypeEnum {

    TOP_UP("top_up", "充值"),

    EXPENDITURE("expenditure", "支出"),

    WITHDRAW("withdraw", "提现"),

    ;

    private final String code;

    private final String msg;

    WalletTypeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
