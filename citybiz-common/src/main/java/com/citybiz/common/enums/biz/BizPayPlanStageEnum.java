package com.citybiz.common.enums.biz;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * Create by Ykg on 2024/2/11 17:14
 * effect: 业务支付阶段状态枚举
 */
@Getter
public enum BizPayPlanStageEnum {

    PRE_PAY_STAGE("pre_pay_stage", "预付阶段"),
    WORKER_STAGE("worker_stage", "工作阶段"),
    AFTER_SALES_STAGE("after_sales_stage", "售后阶段"),

    ;

    static final Map<String, BizPayPlanStageEnum> map = new HashMap<>();

    static{
        BizPayPlanStageEnum[] values = BizPayPlanStageEnum.values();
        for (BizPayPlanStageEnum value : values) {
            map.put(value.getCode(), value);
        }
    }

    public static BizPayPlanStageEnum getEnum(String code){
        return map.get(code);
    }

    private final String code;

    private final String msg;

    BizPayPlanStageEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
