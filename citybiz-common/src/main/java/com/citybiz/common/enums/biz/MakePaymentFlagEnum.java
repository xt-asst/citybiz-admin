package com.citybiz.common.enums.biz;

import lombok.Getter;

/**
 * Create by Ykg on 2023/10/15 18:47
 * effect: 是否打款完毕
 */
@Getter
public enum MakePaymentFlagEnum {

    NO_PAY(0, "未打款"),

    PAID(1, "已打款"),

    ;

    private final Integer code;

    private final String msg;

    MakePaymentFlagEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
