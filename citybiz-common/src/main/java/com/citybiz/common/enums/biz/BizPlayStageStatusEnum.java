package com.citybiz.common.enums.biz;

import lombok.Getter;

/**
 * Create by Ykg on 2024/2/11 17:14
 * effect:
 */
@Getter
public enum BizPlayStageStatusEnum {

    NOT_HOSTED("not_hosted", "未托管"),
    HOSTED("hosted", "已托管"),
    PAID("paid", "已支付"),
    TO_BE_DELIVERED("to_be_delivered", "待交付"),
    TO_BE_ACCEPTED("to_be_accepted", "待验收"),
    ACCEPTANCE_SUCCESS("acceptance_success", "验收成功"),
    ORDER_RIGHTS_PROTECTION("order_rights_protection", "交易维权"),
    END_RIGHTS_PROTECTION("end_rights_protection", "维权结束"),

    ;

    private final String code;

    private final String msg;

    BizPlayStageStatusEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
