package com.citybiz.common.enums.biz;

import lombok.Getter;

/**
 * Create by Ykg on 2023/10/15 16:52
 * effect:
 */
@Getter
public enum WxUserSexEnum {

    BOY("boy", "男性"),

    GIRL("girl", "女性");

    private final String code;

    private final String msg;

    WxUserSexEnum(String code,String msg) {
        this.code = code;
        this.msg = msg;
    }

}
