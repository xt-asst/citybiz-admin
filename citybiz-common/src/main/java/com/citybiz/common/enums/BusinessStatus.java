package com.citybiz.common.enums;

/**
 * 操作状态
 * 
 * @author citybiz
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
