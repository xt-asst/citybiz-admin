package com.citybiz.common.enums.biz;

import lombok.Getter;

/**
 * Create by Ykg on 2023/10/15 17:00
 * effect:
 */
@Getter
public enum TenderTaskStatusEnum {

    COUNSEL_PAY_PLAN("counsel_pay_plan","商议付款方案"),

    TO_BE_CONFIRMED("to_be_confirmed","待确认付款方案"),

    REJECT_PAY_PLAN("reject_pay_plan","拒绝付款方案"),

    WAIT_TO_HOST_PRE_PAYED("wait_to_host_pre_payed","待托管预付款"),

    WAIT_TO_PAY_PRE_PAYED("wait_to_pay_pre_payed","待支付预付款"),

    WAIT_TO_HOST_PROJECT_PAYED("wait_to_host_project_payed","待托管工程款"),

    WAIT_DELIVERY("wait_delivery","待交付"),

    WAIT_DELIVERY_ACCEPTANCE("wait_delivery_acceptance","等待交付验收"),

    WAIT_TO_HOST_PRE_AFTER_SALES_PAYED("wait_to_host_pre_after_sales_payed","待托管售后款"),

    AFTER_SALES("after_sales","售后阶段"),

    ORDER_DISPUTE_HANDLER("order_dispute_handler","交易维权"),

    TASK_END("task_end","交易结束"),

    WAIT_COMMIT_WORK("wait_commit_work","待提交作品"),

    ;


    private final String code;

    private final String msg;

    TenderTaskStatusEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
