package com.citybiz.common.enums.biz;

import lombok.Getter;

/**
 * Create by Ykg on 2023/10/15 18:42
 * effect: 付款方案商议枚举
 */
@Getter
public enum AgreePlanFlagEnum {

    TO_BE_DISCUSSED(0,"待商议"),

    AGREE(1, "同意"),

    DISAGREE(2, "不同意")

    ;

    private final Integer code;

    private final String msg;

    AgreePlanFlagEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
