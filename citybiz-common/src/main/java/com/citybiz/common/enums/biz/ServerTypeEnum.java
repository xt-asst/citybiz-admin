package com.citybiz.common.enums.biz;

import lombok.Getter;

import java.util.HashSet;
import java.util.Set;

/**
 * Create by Ykg on 2023/10/15 19:18
 * effect: 服务类型枚举
 */
@Getter
public enum ServerTypeEnum {

    SERVER("server", "服务"),

    WORK("work", "作品"),

    ;

    private final static Set<String> codeSet = new HashSet<>();

    static{
        ServerTypeEnum[] values = ServerTypeEnum.values();
        for (ServerTypeEnum value : values) {
            codeSet.add(value.getCode());
        }
    }

    private final String code;

    private final String msg;

    ServerTypeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static boolean contains(String code){
        return codeSet.contains(code);
    }

}
