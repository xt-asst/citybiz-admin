package com.citybiz.common.enums.biz;

import lombok.Getter;

import java.util.HashSet;
import java.util.Set;

/**
 * Create by Ykg on 2023/10/15 16:49
 * effect: 任务类型枚举
 */
@Getter
public enum BizTaskSortEnum {

    TENDER("tender", "招标"),

    BOUNTY("bounty", "悬赏");


    private final static Set<String> codeSet = new HashSet<>();

    static{
        BizTaskSortEnum[] values = BizTaskSortEnum.values();
        for (BizTaskSortEnum value : values) {
            codeSet.add(value.getCode());
        }
    }

    public static boolean contains(Long code){
        return codeSet.contains(code);
    }

    private final String code;

    private final String msg;

    BizTaskSortEnum(String code, String msg){
        this.code = code;
        this.msg = msg;
    }

}
