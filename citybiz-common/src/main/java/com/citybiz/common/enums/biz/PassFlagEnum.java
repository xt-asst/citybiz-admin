package com.citybiz.common.enums.biz;

import lombok.Getter;

/**
 * Create by Ykg on 2023/10/15 19:19
 * effect:
 */
@Getter
public enum PassFlagEnum {

    PASS(1, "通过"),

    FAIL(2, "不通过"),

    ;

    private final Integer code;

    private final String msg;

    PassFlagEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
