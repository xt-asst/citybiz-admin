package com.citybiz.common.enums.biz;

import lombok.Getter;

import java.util.HashSet;
import java.util.Set;

/**
 * Create by Ykg on 2023/10/18 23:00
 * effect: 服务订单状态枚举
 */
@Getter
public enum ServerOrderStatusEnum {

    PENDING_PAYMENT("pending_payment", "待付款"),
    PAID("paid", "已付款"),
    IN_PROGRESS("in_progress", "进行中"),
    ORDER_COMPLETED("order_completed", "已完成"),
    OTHER("other", "其他");

    private final static Set<String> codeSet = new HashSet<>();

    static{
        ServerOrderStatusEnum[] values = ServerOrderStatusEnum.values();
        for (ServerOrderStatusEnum value : values) {
            codeSet.add(value.getCode());
        }
    }

    private final String code;

    private final String msg;

    ServerOrderStatusEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static boolean contains(String code){
        return codeSet.contains(code);
    }

}
