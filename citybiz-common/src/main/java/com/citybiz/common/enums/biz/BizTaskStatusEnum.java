package com.citybiz.common.enums.biz;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * Create by Ykg on 2023/10/15 16:58
 * effect:
 */
@Getter
public enum BizTaskStatusEnum {

    START("start", "流程开始"),

    TENDERING("tendering", "投标中"),

    SUBMITTING_WORK("submitting_work", "提交作品中"),

    WORKING("working","工作中"),

    EVALUATING("evaluating", "评价中"),

    TRADE_END("trade_end", "交易结束")

    ;

    private static final Map<String, BizTaskStatusEnum> map = new HashMap<>();

    static {
        BizTaskStatusEnum[] values = BizTaskStatusEnum.values();
        for (BizTaskStatusEnum value : values) {
            map.put(value.getCode(), value);
        }
    }

    private final String code;

    private final String msg;

    BizTaskStatusEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static BizTaskStatusEnum getEnum(String code){
        if(!map.containsKey(code)){
            return BizTaskStatusEnum.START;
        }
        return map.get(code);
    }

}
