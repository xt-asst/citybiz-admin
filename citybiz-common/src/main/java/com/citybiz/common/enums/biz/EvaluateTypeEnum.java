package com.citybiz.common.enums.biz;

import lombok.Getter;

import java.util.HashSet;
import java.util.Set;

/**
 * Create by Ykg on 2023/10/15 19:11
 * effect: 评价类型
 */
@Getter
public enum EvaluateTypeEnum {

    WORK_EVALUATE("work_evaluate","作品评价"),

    TASK_EVALUATE("task_evaluate", "任务评价"),

    SERVER_EVALUATE("server_evaluate", "服务评价"),

    SHOP_EVALUATE("shop_evaluate", "店铺评价"),
    ;

    private final static Set<String> codeSet = new HashSet<>();

    static{
        EvaluateTypeEnum[] values = EvaluateTypeEnum.values();
        for (EvaluateTypeEnum value : values) {
            codeSet.add(value.getCode());
        }
    }

    public static boolean contains(String code){
        return codeSet.contains(code);
    }

    private final String code;

    private final String msg;

    EvaluateTypeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
