package com.citybiz.common.constant;

/**
 * Create by Ykg on 2024/2/10 17:28
 * effect: 任务相关常亮
 */
public class TaskConstant {

    // 任务选择招标文件的id
    public static final String taskSelectManuscriptId = "taskSelectManuscriptId";

    // 任务付款方案拒绝信息备注
    public static final String rejectPayPlanRemark = "rejectPayPlanRemark";

    // 工作者任务评价信息
    public static final String workerEvaluate = "workerEvaluate";

    // 雇主任务评价信息
    public static final String employerEvaluate = "employerEvaluate";

}
