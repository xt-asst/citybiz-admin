package com.citybiz.common.core.service;

import com.citybiz.common.constant.HttpStatus;
import com.citybiz.common.core.page.BizTableDataInfo;
import com.citybiz.common.core.page.TableDataInfo;
import com.citybiz.common.utils.PageUtils;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * Create by Ykg on 2023/12/21 15:35
 * effect:
 */
public class BaseService {

    /**
     * 设置请求分页数据
     */
    protected void startPage()
    {
        PageUtils.startPage();
    }

    /**
     * 响应请求分页数据
     */
    protected <T> BizTableDataInfo<T> getDataTable(List<T> list)
    {
        BizTableDataInfo<T> rspData = new BizTableDataInfo<T>();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setMsg("查询成功");
        rspData.setRows(list);
        rspData.setTotal(new PageInfo<T>(list).getTotal());
        return rspData;
    }

}
