package com.citybiz.common.utils;

import java.util.Random;

/**
 * create by Ykg on 2022/12/04
 * effect: 短信验证码工具
 */
public class VerifyCodeUtils {

    /**
     * 随机生成6位数字
     * @return
     */
    public static String getCode(){
        String str="0123456789";
        String code=new String();
        for(int i=0;i<6;i++)
        {
            char ch=str.charAt(new Random().nextInt(str.length()));
            code += ch;
        }
        return code;
    }
}