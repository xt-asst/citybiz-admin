package com.citybiz.common.utils;

import com.citybiz.common.exception.base.AssertException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Create by Ykg on 2023/9/29 00:14
 * effect: 断言工具
 */
public class AssertUtils {

    public static void isNotEmpty(String value,String message){
        if(StringUtils.isEmpty(value)){
            throw new AssertException(message);
        }
    }

    public static void isNotEmpty(List<Long> value, String message){
        if(CollectionUtils.isEmpty(value)){
            throw new AssertException(message);
        }
    }

    public static void isNotNull(Long value, String message){
        if(value == null){
            throw new AssertException(message);
        }
    }

    public static void isNotNull(Integer value, String message){
        if(value == null){
            throw new AssertException(message);
        }
    }

}
