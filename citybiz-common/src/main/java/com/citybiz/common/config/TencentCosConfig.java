package com.citybiz.common.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * create by Ykg on 2022/11/23
 * effect: 腾讯cos服务配置信息
 */
@Component
@Getter
@Setter
public class TencentCosConfig {

    @Value("${cos.secretId}")
    private String secretId;

    @Value("${cos.secretKey}")
    private String secretKey;

    @Value("${cos.appid}")
    private String appid;

    @Value(("${cos.address}"))
    private String address;

    @Value("${cos.bucketName}")
    private String bucketName;

    @Value("${cos.year}")
    private Long year;

}
