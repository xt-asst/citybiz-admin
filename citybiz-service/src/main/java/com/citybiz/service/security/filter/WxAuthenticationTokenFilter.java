package com.citybiz.service.security.filter;

import com.citybiz.common.core.domain.model.LoginUser;
import com.citybiz.common.utils.SecurityUtils;
import com.citybiz.common.utils.StringUtils;
import com.citybiz.service.web.service.TokenService;
import com.citybiz.service.web.service.WxLoginService;
import com.citybiz.wxcommon.core.domain.model.WxLoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * token过滤器 验证token有效性
 * 
 * @author citybiz
 */
@Component
public class WxAuthenticationTokenFilter extends OncePerRequestFilter
{
    @Autowired
    private WxLoginService wxLoginService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException
    {

        String token = request.getHeader("WxAuthorization");
        WxLoginUser loginUser = wxLoginService.getLoginUser(token);

        if (StringUtils.isNotNull(loginUser) && StringUtils.isNull(SecurityUtils.getAuthentication())) {
            wxLoginService.verifyToken(loginUser);
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(loginUser, null, null);
            authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        }
        chain.doFilter(request, response);
    }
}
