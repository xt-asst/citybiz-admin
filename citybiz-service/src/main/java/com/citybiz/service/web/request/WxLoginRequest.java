package com.citybiz.service.web.request;

import lombok.Data;

import java.util.Base64;

/**
 * Create by Ykg on 2023/9/29 00:02
 * effect:
 */
@Data
public class WxLoginRequest extends BaseRequest {

    private String appid;

    private String code;

    private String phone;

    private String password;

    private String username;

    /**
     * 登录角色
     */
    private Integer role;



}
