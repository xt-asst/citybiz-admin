package com.citybiz.service.utils;

import com.citybiz.system.domain.WxUser;

/**
 * Create by Ykg on 2023/10/3 10:35
 * effect: 微信用户工具类
 */
public class WxUserAuthUtil {

    private static final ThreadLocal<WxUser> userLocal = new ThreadLocal<>();


    public WxUserAuthUtil(){}

    public static WxUser getWxUser(){
        return (WxUser)userLocal.get();
    }



    public static void setWxUser(WxUser wxUser){
        userLocal.set(wxUser);
    }

    public static void rmWxUser(){
        userLocal.remove();
    }

}
